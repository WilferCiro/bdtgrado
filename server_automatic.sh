# This script must be executed on /home/user folder
rm -rf tgrado_copy
git clone https://gitlab.com/WilferCiro/bdtgrado.git tgrado_copy
rm -rf last_copy
cp -r tgrado last_copy
rm -rf tgrado/app
mv tgrado_copy/app tgrado
rm -rf tgrado_copy
cp -rf last_copy/app/static/media tgrado/app/static
cd tgrado
python3 manage.py makemigrations
python3 manage.py migrate

echo "Now procced to reload web app"
