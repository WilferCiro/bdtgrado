# TGRADO
![Logo](tgrado.png)

## TODO:
- [x] URGENTE: solucionar carga de datos en perfil de tgrado (creo que ya).
- [x] Añadir tgrados evaluados a whitelist evaluador
- [x] Añadir perfil del usuario
- [x] Crear gráficas con posibilidad de exportar
- [ ] Enviar correos a los evaluadores asignados (cron, hosting) - terminar editando cron.py
- [x] Añadir casilla de solicitud de datos en el perfil de tgrado
- [x] Crear logo propio :3
- [x] Crear página tipo blog y con ello la configuración del texto del blog
- [x] TGrado perfil: habilitar o deshabilitar el  botón de agregar evaluadores según la cantidad seleccionada
- [x] Criterios de evaluación: permitir eliminar si no hay notas con ese criterio
- [ ] Agregar sección para escribir anteproyecto con opción de compartir al director
- [ ] Loading en carga de archivos
- [ ] Mejorar visualización de errores en peticiones no ajax

## Dudas
- ¿Eliminar Tgrado? (¿no permitir cuando ya exista anteproyecto?)
- ¿Qué pasa si las notas son menor a 3? (solucionado con la opción de "finalizar tgrado")

## Instalación de dependencias
Instalar xampp, https://www.apachefriends.org/xampp-files/7.3.0/xampp-linux-x64-7.3.0-0-installer.run
```
sudo chmod +x xampp-linux-x64-7.3.0-0-installer.run
sudo ./xampp-linux-x64-7.3.0-0-installer.run
```
Dejar las opciones por defecto (para más información https://www.apachefriends.org/es/download.html),
Ejecutar el servidor local
```
sudo /opt/lampp/lampp start
```
Abrir la página "localhost/phpmyadmin" en el navegador, y crear base de datos llamada "bdtgrado"

```
sudo pip3 install django-cleanup django_select2 django-bootstrap-datepicker-plus xhtml2pdf django-ckeditor django-bootstrap4 mysqlclient xlwt django-imagekit django_cron weasyprint python-docx python-dateutil
```

```
python manage.py makemigrations
python manage.py migrate
```
Si es primera vez:
```
python manage.py createsuperuser
```
siempre que desee ejecutar el servidor
```
python manage.py runserver
```

## Sugerencias
- [X] Generar estadísticas por cada director de los trabajos de grado dirigidos, co-dirigidos y evaluados.
- [ ] Incluir información sobe el proyecto de grado: Resumen, Objetivos
- [X] Limitar el correo de los estudiantes para que sea el institucional
- [X] Quitar área de impacto
- [x] Página P: Flujograma de los procedimientos relacionados a trabajos de grado (Lo agregan los administradores)
- [x] Página P: Cronograma de trabajos de grado
- [ ] Al añadir un trabajo de grado nuevo, podría abrir una ventana emergente en vez de abrir otra página - Revisar
- [X] En Banco de Proyectos colocar Grupo (GDSPROC, ...) También puede ser una ventana emergente
- [X] Banco de proyectos que sea trabajo de directores (Según comité)

## Para agregar
- [x] Agregar codirector
- [x] cuando el anteproyecto es aplazado se cuenta con un mes calendario para entregar correcciones de lo contrario se rechaza
- [x] cuando el informe final es aplazado se cuenta con tres meses calendario para entregar correcciones de lo contrario se rechaza
- [x] se debe realizar el trabajo en el cronograma aprobado
- [ ] dos nuevas modalidades de trabajos de grado
