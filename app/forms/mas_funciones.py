from django import forms
from app.models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets
from django_select2.forms import HeavySelect2Widget, HeavySelect2MultipleWidget
from bootstrap_datepicker_plus import MonthPickerInput, TimePickerInput, DatePickerInput

class editBanco(ModelForm):
	
	class Meta:
		model = BancoProyectos
		fields = ["proponente", "modalidad", "grupo", "estado", "titulo", "descripcion"]
		widgets = {
			"proponente" : HeavySelect2Widget(data_url='/json/director'),
			'titulo': Textarea(attrs={'cols': 20, 'rows': 5}),
			'descripcion': Textarea(attrs={'cols': 20, 'rows': 5})
		}
		
	def __init__(self, *args, **kwargs):	
		obj = kwargs.pop('obj','')
		super(editBanco, self).__init__(*args, **kwargs)

class editBancoDirector(ModelForm):
	
	class Meta:
		model = BancoProyectos
		fields = ["modalidad", "grupo", "titulo", "descripcion"]
		widgets = {
			'titulo': Textarea(attrs={'cols': 20, 'rows': 5}),
			'descripcion': Textarea(attrs={'cols': 20, 'rows': 5})
		}
		
	def __init__(self, *args, **kwargs):
		obj = kwargs.pop('obj','')
		super(editBancoDirector, self).__init__(*args, **kwargs)


class editPQRS(ModelForm):
	
	class Meta:
		model = PeticionSugerencia
		fields = ["usuario", "asunto", "cuerpo"]
		
	def __init__(self, *args, **kwargs):
		obj = kwargs.pop('obj','')
		super(editPQRS, self).__init__(*args, **kwargs)

class editPQRSDirector(ModelForm):
	
	class Meta:
		model = PeticionSugerencia
		fields = ["asunto", "cuerpo"]
		
	def __init__(self, *args, **kwargs):
		obj = kwargs.pop('obj','')
		super(editPQRSDirector, self).__init__(*args, **kwargs)


class FormGraphic(forms.Form):
	G_CANTIDAD = "1"
	G_TGRADOFINALIZADO = "2"
	G_ANTEFINALIZADO = "3"
	G_INFFINALIZADO = "4"
	tipo = forms.ChoiceField(
		choices=(
			(G_TGRADOFINALIZADO, "TGrados finalizados y en ejecución"),
			(G_ANTEFINALIZADO, "Conceptos de Anteproyectos finalizados y en ejecución"),
			(G_INFFINALIZADO, "Conceptos de Informes finalizados y en ejecución"),
			(G_CANTIDAD, "Cantidades"),
		),
		required = True
	)
	fecha_desde = forms.CharField(label="Mes desde: ", widget=MonthPickerInput().start_of('event days'), required=True)
	fecha_hasta = forms.CharField(label="Mes hasta: ", widget=MonthPickerInput().end_of('event days'), required=True)

class FormSustentaciones(forms.Form):
	hora_desde = forms.CharField(label="Hora desde: ", widget=TimePickerInput().start_of('event days'), required=True)
	hora_hasta = forms.CharField(label="Hora hasta: ", widget=TimePickerInput().end_of('event days'), required=True)
	fecha = forms.CharField(label="Fecha sustentación: ", widget=DatePickerInput(format="YYYY-MM-DD"), required=True)
	tiempo = forms.CharField(required=True)
	lugar = forms.CharField(label="Lugar de sustentación", required=True)
	tgrados = forms.ModelChoiceField(widget=HeavySelect2MultipleWidget(data_url='/json/tgrados/paraFecha'), queryset = TGrado.objects.none(), label="Tgrados", required = True)
	paralelos = forms.CharField(required=True)
	
