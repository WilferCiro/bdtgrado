from django import forms
from app.models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets
from django_select2.forms import Select2Widget


class editBorrador(ModelForm):

	class Meta:
		model = BorradorCorreo
		fields = ["nombre", "asunto", "cuerpo", "observaciones"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 2}),
		}

	def __init__(self, *args, **kwargs):
		obj = kwargs.pop('obj','')
		super(editBorrador, self).__init__(*args, **kwargs)

class editTemplate(ModelForm):

	class Meta:
		model = MailTemplates
		fields = ["titulo", "estado", "mensaje"]

	def __init__(self, *args, **kwargs):
		obj = kwargs.pop('obj','')
		super(editTemplate, self).__init__(*args, **kwargs)

class sendMain(ModelForm):
	"""correos_internos = forms.ModelMultipleChoiceField(
		label="Correos en plataforma",
		queryset = User.objects.all(),
        widget=HeavySelect2MultipleWidget(
            data_url='/json/correos'
        ), required = False
    )
	correos_externos = forms.CharField(label='Otros correos, separados por coma ( , )', required=False)"""
	correos = forms.MultipleChoiceField()
	class Meta:
		model = BorradorCorreo
		fields = ["correos", "asunto", "cuerpo"]

	def __init__(self, *args, **kwargs):
		super(sendMain, self).__init__(*args, **kwargs)
		self.fields['correos'].widget.attrs['class'] = 'select_email'

class sendMainMensaje(ModelForm):
	correos = forms.MultipleChoiceField()
	class Meta:
		model = Mensajes
		fields = ["correos", "asunto", "mensaje"]

	def __init__(self, *args, **kwargs):
		super(sendMainMensaje, self).__init__(*args, **kwargs)
		self.fields['correos'].widget.attrs['class'] = 'select_email'


class changeBorradorForm(forms.Form):
	borrador = forms.ModelChoiceField(widget=Select2Widget, queryset=BorradorCorreo.objects.all().order_by("-nombre"), label="Seleccione el borrador para ser cargado", required=True)

class editCorreoAdicional(ModelForm):
	class Meta:
		model = CorreoAdicional
		fields = ["nombre", "email", "descripcion"]
