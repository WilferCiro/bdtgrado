from django import forms
from app.models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets


class add_usuario(forms.Form):
	nombres = forms.CharField(label="Nombres ", required=True)
	apellidos = forms.CharField(label="Apellidos ", required=True)
	documento = forms.CharField(label="Documento ", required=True)
