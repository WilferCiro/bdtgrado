from django import forms
from app.models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets


class editDocumento(ModelForm):
	
	class Meta:
		model = DocumentoVario
		fields = ["nombre", "descripcion", "archivo"]
		widgets = {
			'descripcion': Textarea(attrs={'cols': 20, 'rows': 3}),
		}
	
	def __init__(self, *args, **kwargs):	
		obj = kwargs.pop('obj','')
		super(editDocumento, self).__init__(*args, **kwargs)
