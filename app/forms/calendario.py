from django import forms
from app.models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets                                       
from bootstrap_datepicker_plus import DatePickerInput


class editCalendar(ModelForm):
	
	class Meta:
		model = Calendario
		fields = ["fecha_inicio", "fecha_fin", "descripcion"]
		widgets = {
			'descripcion': Textarea(attrs={'cols': 20, 'rows': 4, "required" : True}),			
			"fecha_fin" : DatePickerInput(format="YYYY-MM-DD").start_of('fecha days'),
			"fecha_inicio" : DatePickerInput(format="YYYY-MM-DD").end_of('fecha days')
		}
	
