from django import forms
from app.models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets                                       
from bootstrap_datepicker_plus import DatePickerInput


class editInformes(ModelForm):
	
	class Meta:
		model = ConfiguracionInformes
		fields = "__all__"

class imprimirInformeForm(forms.Form):
	fecha_desde = forms.CharField(label="Fecha desde: ", widget=DatePickerInput(format="YYYY-MM-DD").start_of('event days'), required=False)
	fecha_hasta = forms.CharField(label="Fecha hasta: ", widget=DatePickerInput(format="YYYY-MM-DD").end_of('event days'), required=False)
