from django import forms
from app.models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets
from bootstrap_datepicker_plus import DatePickerInput
from django.contrib.auth.forms import UserCreationForm, UserChangeForm


class editUser(ModelForm):

	class Meta:
		model = User
		fields = ["username", "first_name", "last_name", "genero", "documento", "telefono1", "red_social"]
		widgets = {
			'red_social': Textarea(attrs={'cols': 20, 'rows': 2}),
		}
		labels = {
			"email": "E-Mail",
			"first_name" : "Nombre",
			"last_name" : "Apellido"
		}

class editUserEvaluador(ModelForm):

	class Meta:
		model = User
		fields = ["username", "first_name", "last_name", "email", "genero", "documento", "telefono1", "red_social"]
		widgets = {
			'red_social': Textarea(attrs={'cols': 20, 'rows': 2}),
		}
		labels = {
			"email": "E-Mail",
			"first_name" : "Nombre",
			"last_name" : "Apellido"
		}

class editAvatar(ModelForm):
	class Meta:
		model = User
		fields = ["avatar"]

class editEstadoAcademico(ModelForm):
	class Meta:
		model = EstadoAcademico
		fields = ["estado_academico"]


class changePass(forms.Form):
	pass1 = forms.CharField(widget=forms.PasswordInput())
	pass2 = forms.CharField(widget=forms.PasswordInput())
