from django import forms
from app.models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets


class addCriterioInforme(ModelForm):
	class Meta:
		model = InformeCriterioEvaluacion
		fields = ["aspecto", "observaciones", "peso", "modalidad"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3}),
			'aspecto': Textarea(attrs={'cols': 20, 'rows': 3}),
		}

class editCriterioInforme(ModelForm):
	class Meta:
		model = InformeCriterioEvaluacion
		fields = ["observaciones", "estado"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3}),
		}

class addCriterioSustentacion(ModelForm):
	class Meta:
		model = SustentacionCriterioEvaluacion
		fields = ["aspecto", "observaciones", "peso"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3}),
			'aspecto': Textarea(attrs={'cols': 20, 'rows': 3}),
		}

class editCriterioSustentacion(ModelForm):
	class Meta:
		model = SustentacionCriterioEvaluacion
		fields = ["observaciones", "estado"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3}),
		}

class editMensaje(ModelForm):
	class Meta:
		model = Mensajes
		fields = ["asunto", "mensaje"]

	def __init__(self, *args, **kwargs):
		obj = kwargs.pop('obj','')
		super(editMensaje, self).__init__(*args, **kwargs)

class editPaginaBlog(ModelForm):
	class Meta:
		model = PaginaBlog
		fields = ["titulo", "imagen", "pagina"]


	def __init__(self, *args, **kwargs):
		obj = kwargs.pop('obj','')
		super(editPaginaBlog, self).__init__(*args, **kwargs)
