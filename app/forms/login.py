from django import forms
from app.models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets                                       

class SingUpForm(forms.Form):
	nombre = forms.CharField(label="Nombre * ", required=True, widget=forms.TextInput(attrs={'placeholder': ''}))
	apellido = forms.CharField(label="Apellido * ", required=True, widget=forms.TextInput(attrs={'placeholder': ''}))
	documento = forms.CharField(label="Documento * ", required=True, widget=forms.TextInput(attrs={'placeholder': ''}))
	email = forms.CharField(label="E-mail * ", required=True, widget=forms.TextInput(attrs={'placeholder': 'E mail'}))
	pass1 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': ''}), label="Contraseña * ", required=True)
	pass2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': ''}), label="Repita contraseña * ", required=True)
	sexo = forms.ChoiceField(label="", required=True, choices = User.GENERO_CHOICES)
