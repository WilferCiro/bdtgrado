from django import forms
from app.models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets
from bootstrap_datepicker_plus import DatePickerInput, DateTimePickerInput


class editTGrado(ModelForm):

	#ciudad = forms.ModelMultipleChoiceField(queryset=Ciudad.objects.all(), widget=Select2MultipleWidget)
	estudiantes = forms.ModelMultipleChoiceField(
		queryset = User.objects.none()
	)
	director = forms.ModelMultipleChoiceField(
		queryset = User.objects.none()
	)
	codirector = forms.ModelMultipleChoiceField(
		queryset = User.objects.none()
	)
	asesores = forms.ModelMultipleChoiceField(
		queryset = User.objects.none()
	)
	class Meta:
		model = TGrado
		fields = ["titulo", "modalidad"] # Asesor
		widgets = {
			'titulo': Textarea(attrs={'cols': 20, 'rows': 3, 'required' : True})
		}

	def __init__(self, *args, **kwargs):
		obj = kwargs.pop('obj','')
		super(editTGrado, self).__init__(*args, **kwargs)

		self.fields['estudiantes'].widget.attrs['class'] = 'select_estudiante'
		self.fields['director'].widget.attrs['class'] = 'select_director'
		self.fields['codirector'].widget.attrs['class'] = 'select_codirector'
		self.fields['codirector'].required = False
		self.fields['asesores'].widget.attrs['class'] = 'select_asesores'
		self.fields['asesores'].required = False

class editTGradoDirector(ModelForm):

	#ciudad = forms.ModelMultipleChoiceField(queryset=Ciudad.objects.all(), widget=Select2MultipleWidget)
	estudiantes = forms.ModelMultipleChoiceField(
		queryset = User.objects.none()
	)
	asesores = forms.ModelMultipleChoiceField(
		queryset = User.objects.none()
	)
	class Meta:
		model = TGrado
		fields = ["titulo", "modalidad"] # Asesor
		widgets = {
			'titulo': Textarea(attrs={'cols': 20, 'rows': 3}),
        	'fecha_aprobacion' : DatePickerInput(options={"format":"YYYY-MM-DD"}),
		}

	def __init__(self, *args, **kwargs):
		obj = kwargs.pop('obj','')
		super(editTGradoDirector, self).__init__(*args, **kwargs)

		self.fields['estudiantes'].widget.attrs['class'] = 'select_estudiante'
		self.fields['asesores'].widget.attrs['class'] = 'select_asesores'
		self.fields['asesores'].required = False

class editAnteproyectoAplicacion(ModelForm):
	#archivos = forms.FileField(label="Archivos (Opcional)", widget=forms.ClearableFileInput(attrs={'multiple': True}), required = False)
	class Meta:
		model = Anteproyecto
		fields = ["archivo_anteproyecto", "meses_desarrollo", "observaciones_anteproyecto"]
		widgets = {
			'observaciones_anteproyecto': Textarea(attrs={'cols': 20, 'rows': 3})
		}
		help_texts = {
            'archivo_anteproyecto' : "(PDF) Adjunte el texto de anteproyecto a revisar en formato PDF"
        }

	def __init__(self, *args, **kwargs):
		super(editAnteproyectoAplicacion, self).__init__(*args, **kwargs)
		self.fields['archivo_anteproyecto'].required = True
		self.fields['archivo_anteproyecto'].widget.attrs['accept'] = "application/pdf"
		self.fields['meses_desarrollo'].required = True

class editAnteproyectoPasantia(ModelForm):
	#archivos = forms.FileField(label="Archivos (Opcional)", widget=forms.ClearableFileInput(attrs={'multiple': True}), required = False)
	class Meta:
		model = Anteproyecto
		fields = ["archivo_anteproyecto", "convenio_marco", "carta_presentacion", "observaciones_anteproyecto"]
		widgets = {
			'observaciones_anteproyecto': Textarea(attrs={'cols': 20, 'rows': 3})
		}
		help_texts = {
            'archivo_anteproyecto' : "(PDF) Adjunte el texto de anteproyecto a revisar en formato PDF"
        }

	def __init__(self, *args, **kwargs):
		super(editAnteproyectoPasantia, self).__init__(*args, **kwargs)
		self.fields['archivo_anteproyecto'].required = True
		self.fields['archivo_anteproyecto'].widget.attrs['accept'] = "application/pdf"
		self.fields['convenio_marco'].required = True
		self.fields['carta_presentacion'].required = True

class editAnteproyectoInvestigacion(ModelForm):
	#archivos = forms.FileField(label="Archivos (Opcional)", widget=forms.ClearableFileInput(attrs={'multiple': True}), required = False)
	class Meta:
		model = Anteproyecto
		fields = ["archivo_anteproyecto", "carta_proyecto_vigente", "carta_coordinador", "observaciones_anteproyecto"]
		widgets = {
			'observaciones_anteproyecto': Textarea(attrs={'cols': 20, 'rows': 3})
		}
		help_texts = {
            'archivo_anteproyecto' : "(PDF) Adjunte el texto de anteproyecto a revisar en formato PDF"
        }

	def __init__(self, *args, **kwargs):
		super(editAnteproyectoInvestigacion, self).__init__(*args, **kwargs)
		self.fields['archivo_anteproyecto'].required = True
		self.fields['archivo_anteproyecto'].widget.attrs['accept'] = "application/pdf"
		self.fields['carta_proyecto_vigente'].required = True
		self.fields['carta_coordinador'].required = True

class editAnteproyectoOther(ModelForm):
	#archivos = forms.FileField(label="Archivos (Opcional)", widget=forms.ClearableFileInput(attrs={'multiple': True}), required = False)
	class Meta:
		model = Anteproyecto
		fields = ["archivo_anteproyecto", "observaciones_anteproyecto"]
		widgets = {
			'observaciones_anteproyecto': Textarea(attrs={'cols': 20, 'rows': 3})
		}
		help_texts = {
            'archivo_anteproyecto' : "(PDF) Adjunte el texto de anteproyecto a revisar en formato PDF"
        }

	def __init__(self, *args, **kwargs):
		super(editAnteproyectoOther, self).__init__(*args, **kwargs)
		self.fields['archivo_anteproyecto'].required = True
		self.fields['archivo_anteproyecto'].widget.attrs['accept'] = "application/pdf"


class addEvaluadorAnteproyecto(forms.Form):
	evaluador = forms.ModelMultipleChoiceField(
		queryset = User.objects.none()
	)

	def __init__(self, *args, **kwargs):
		obj = kwargs.pop('obj','')
		super(addEvaluadorAnteproyecto, self).__init__(*args, **kwargs)

		self.fields['evaluador'].widget.attrs['class'] = 'select_evaluador'
		#self.fields['evaluador'].initial = obj

class editConceptoAnteproyecto(ModelForm):
	class Meta:
		model = AnteproyectoConcepto
		fields = ["concepto", "archivo", "observaciones"]

# Informe

class editInforme(ModelForm):
	class Meta:
		model = Informe
		fields = ["archivo_informe", "observaciones"]
		widgets = {
			'observaciones': Textarea(attrs={'cols': 20, 'rows': 3})
		}
		help_texts = {
            'archivo_informe' : "(PDF) Adjunte el texto del informe a revisar en formato PDF"
        }

	def __init__(self, *args, **kwargs):
		super(editInforme, self).__init__(*args, **kwargs)
		#self.fields['archivo_informe'].required = True
		self.fields['archivo_informe'].widget.attrs['accept'] = "application/pdf"

class editFechaSustentacion(ModelForm):
	class Meta:
		model = Informe
		fields = ["fecha_sustentacion", "lugar_sustentacion"]
		widgets = {
			'fecha_sustentacion' : DateTimePickerInput(options={"format":"YYYY-MM-DD HH:mm:ss"}),
		}

class editEvaluadorInforme(forms.Form):
	evaluadores = forms.ModelMultipleChoiceField(
		queryset = User.objects.none()
	)

	def __init__(self, *args, **kwargs):
		obj = kwargs.pop('obj','')
		super(editEvaluadorInforme, self).__init__(*args, **kwargs)
		self.fields['evaluadores'].widget.attrs['class'] = 'select_evaluador'
		self.fields["evaluadores"].help_text = "Si presiona el botón, se asignarán los evaluadores que están en el campo seleccionados solamente."
		self.fields["evaluadores"].required = False


class editConceptoInformeAdmin(ModelForm):
	class Meta:
		model = InformeConcepto
		fields = ["concepto"]

class editConceptoInforme(ModelForm):
	class Meta:
		model = InformeConcepto
		fields = ["concepto", "archivo", "observaciones"]


class FinalizarTGrado(ModelForm):
	estado = forms.ChoiceField(choices = (
		(TGrado.E_PARA_FIN, "Pendiente de finalizar"),
		(TGrado.E_EXITO, "Terminado con éxito"),
		(TGrado.E_APLAZADO_TGRADO, "Aplazado"),
		(TGrado.E_CANCELADO, "Reprobado")
		), label = "Seleccione el estado"
	)

	class Meta:
		model = TGrado
		fields = ["estado"]


class formProrroga(ModelForm):
	class Meta:
		model = Anteproyecto
		fields = ["meses_desarrollo_prorroga"]
