from django.core.mail import send_mail

def send_an_email(asunto = "asunto", cuerpo = "cuerpo", correos = []):
	
	send_mail(
		asunto,
		'',
		'wilcirom@gmail.com',
		correos,
		fail_silently=False,
		html_message = cuerpo
	)
	return True
