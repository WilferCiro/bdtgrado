import os
from django.conf import settings
from django.http import HttpResponse
from django.template import Context
from weasyprint import HTML, CSS
from django.http import HttpResponse
from django.template.loader import render_to_string


def show_pdf(data, template_path, absolute_path="/", download = False):
	html_template = render_to_string(template_path, data)
	pdf_file = HTML(string=html_template, base_url=absolute_path).write_pdf()
	response = HttpResponse(pdf_file, content_type='application/pdf')
	#response['Content-Disposition'] = 'attachment; filename="home_page.pdf"'
	response['Content-Disposition'] = 'inline; filename="home_page.pdf"'
	return response
