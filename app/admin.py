from django.contrib import admin
from app.models import *
from django.contrib.auth.admin import UserAdmin

from django.contrib.auth.forms import UserChangeForm

class MyUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = User

class MyUserAdmin(UserAdmin):
    form = MyUserChangeForm

    fieldsets = (
            (None, {'fields': (
            	'last_name',
            	'first_name',
            	'documento',
            	'username',
            	'password',
            	'email',
            	'es_estudiante',
            	'es_admin',
            	'es_evaluador',
            	'telefono1',
            	'red_social'
            )}),
    )


admin.site.register(User, MyUserAdmin)
# Register your models here.
admin.site.register(TGradoEstudiantes)
admin.site.register(Anteproyecto)
admin.site.register(AnteproyectoConcepto)
admin.site.register(EstadoAcademico)
admin.site.register(TGrado)
admin.site.register(InformeConcepto)
admin.site.register(Mensajes)
admin.site.register(Informe)

admin.site.register(NotaInforme)
admin.site.register(NotaSustentacion)

admin.site.register(InformeCriterioEvaluacion)

admin.site.register(CambioContrasena)
