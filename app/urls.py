from django.urls import path
from app.views import various
from app.views import tgrado, ajax, users, documentos, email, informes, calendario, configuracion, login, mas_funciones, blog, docx


urlpatterns = [
	path('logout/', various.logoutView, name='Logout'),
	path('login/', various.loginView, name='Login'),
	path('index/', various.IndexPage.as_view(), name='IndexPage'),

	path('tgrado/', tgrado.TGradoView.as_view(), name='TGradoView'),
	path('tgrado/view/<pk>', tgrado.TGradoViewIndividual.as_view(), name='TGradoViewIndividual'),

	path("tgrado/notas/<tgrado>/<informe>/<concepto>", tgrado.CalificacionView.as_view(), name="CalificacionView"),

	path("proyectos/banco", mas_funciones.BancoProyectos.as_view(), name="BancoProyectos"),
	path("programacion/sustentaciones", mas_funciones.ProgramacionSustentaciones.as_view(), name="ProgramacionSustentaciones"),

	path('estudiantes/', users.EstudiantesView.as_view(), name='EstudiantesView'),
	path('evaluadores/', users.EvaluadoresView.as_view(), name='EvaluadoresView'),

	path("word/evaluacion_formato", docx.EvaluacionFormato.as_view(), name="EvaluacionFormato"),

	path('documentos/', documentos.DocumentosView.as_view(), name='DocumentosView'),

	path('email/borradores', email.EmailBorradoresView.as_view(), name='EmailBorradoresView'),
	path('email/redactar', email.EmailRedactarView.as_view(), name='EmailRedactarView'),
	path('email/otros', email.EmailOtrosView.as_view(), name='EmailOtrosView'),
	path('email/plantillas', email.EmailTemplatesView.as_view(), name='EmailTemplatesView'),

	path('informes/configurar', informes.ConfigurarInformesView.as_view(), name='ConfigurarInformesView'),
	path('informes/print', informes.PrintInformeView.as_view(), name='PrintInformeView'),

	path('calendario/', calendario.CalendarioView.as_view(), name='CalendarioView'),

	path('configuracion/criterios/informe', configuracion.CriteriosInformeView.as_view(), name='CriteriosInformeView'),
	path('configuracion/criterios/sustentacion', configuracion.CriterioSustentacionView.as_view(), name='CriterioSustentacionView'),

	path('configuracion/email', configuracion.EmailConfigView.as_view(), name='EmailConfigView'),
	path("configuracion/paginas/blog", configuracion.PaginasBlog.as_view(), name='PaginasBlog'),

	path("estadisticas/", mas_funciones.EstadisticasView.as_view(), name = "EstadisticasView"),
	path("pqrs/", mas_funciones.PQRS.as_view(), name="PQRS"),

	path('json/estudiante', ajax.EstudianteAjax.as_view(), name='EstudianteJson'),
	path('json/director', ajax.DirectorAjax.as_view(), name='DirectorAjax'),
	path('json/correos', ajax.CorreosAjax.as_view(), name='CorreosJson'),
	path('json/notification', ajax.NotificacionesAjax.as_view(), name='NotificacionesAjax'),
	path('json/tgrados/paraFecha', ajax.TGradosParaFecha.as_view(), name='TGradosParaFecha'),

	path("json/addData", ajax.AddDataModal.as_view(), name="AddDataModal"),

	path("buscar/", various.SearchPage.as_view(), name="BuscarPagina"),

	path("user/profile", users.MyProfileView.as_view(), name="MyProfileView"),
	path("estudiantes/view/<pk>", users.MyProfileView.as_view(), name="UserProfileView"),
	path("evaluadores/view/<pk>", users.MyProfileView.as_view(), name="EvaluadorProfileView"),

	path("singup/", login.SingUp.as_view(), name="SingUp"),
	path("recover/pass", login.RecoverPass.as_view(), name="RecoverPass"),
	path("recover/pass/code", login.RecoverPassCode.as_view(), name="RecoverPassCode"),

	path("change/idioma/", various.ChangeIdioma, name="ChangeIdioma"),

	# Blog paths
	path('help/', blog.HelpView.as_view(), name='HelpView'),
	path('blog/page/<pk>', blog.PaginaBlogView.as_view(), name='PaginaBlog'),
	path('blog/calendario', blog.CalendarioView.as_view(), name='CalendarioView'),
	path('blog/sustentaciones', blog.SustentacionesView.as_view(), name='SustentacionesView'),
	path('blog/banco', blog.BancoView.as_view(), name='BancoView'),
	path('blog/procedimientos', blog.ProcedimientosView.as_view(), name='ProcedimientosView'),
	path('', blog.IndexBlog.as_view(), name = "IndexBlog")
]
