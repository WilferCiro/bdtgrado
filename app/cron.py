from django_cron import CronJobBase, Schedule
import time

from app.models import *

class cron(CronJobBase):
	RUN_EVERY_MINS = 1

	schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
	code = 'my_app.my_cron_job'
	
	def do(self):
		f = open("text.txt","w+")
		user = User.objects.last()
		ticks = time.time()
		f.write(str(user.get_full_name()))
		f.close()
		print("run")

