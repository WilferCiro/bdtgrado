from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models import Q
from ckeditor.fields import RichTextField
import os
from django.core.validators import FileExtensionValidator
from imagekit.models import ProcessedImageField
from imagekit.processors import ResizeToFill
from django.core.validators import MaxValueValidator, MinValueValidator
from datetime import datetime

# Elementos de usuario
# criterios de evaluacion
# Linkein
MODALIDAD_APLICACION = "1"
MODALIDAD_PASANTIA = "2"
MODALIDAD_INVESTIGACION = "3"
MODALIDAD_NEGOCIO = "4"
MODALIDAD_SEMINARIO = "5"
MODALIDAD_MAESTRIA = "6"
MODALIDADES_CHOOSE = (
	(MODALIDAD_APLICACION, "Aplicación"),
	(MODALIDAD_PASANTIA, "Pasantía"),
	(MODALIDAD_INVESTIGACION, "Investigación"),
	(MODALIDAD_NEGOCIO, "Plan de negocio"),
	(MODALIDAD_SEMINARIO, "Seminario"),
	(MODALIDAD_MAESTRIA, "Cursos de maestría")
)

ESTADO_ACTIVO = "1"
ESTADO_INACTIVO = "2"
ESTADO_BLOQUEADO = "3"

ESTADO_ELIMINADO = "4"

def get_modalidad(modalidad):
	for data in MODALIDADES_CHOOSE:
		if data[0] == modalidad:
			return data[1]
	return "None"

# Administración
class User(AbstractUser):
	documento = models.CharField(max_length = 20, null=True)
	fecha_creacion = models.DateTimeField(auto_now_add = True, null=True, blank=True)

	G_MASCULINO = "Masculino"
	G_FEMENINO = "Femenino"
	GENERO_CHOICES = (
		(G_MASCULINO, 'Masculino'),
		(G_FEMENINO, 'Femenino')
	)
	genero = models.CharField(
		max_length = 10,
		choices = GENERO_CHOICES,
		null = True,
		blank = True
	)

	telefono1 = models.CharField(max_length=12, null = True, blank = True)
	red_social = models.TextField(null=True, blank=True)
	bloqueado = models.BooleanField("¿Bloqueado?", default=False)
	es_estudiante = models.BooleanField("¿Es Estudiante?", default=False)
	es_admin = models.BooleanField("¿Es Administrador?", default=False)
	es_evaluador = models.BooleanField("¿Es Evaluador?", default=False)
	#avatar = models.ImageField(upload_to='avat/%s', null=True, blank = True)
	avatar = ProcessedImageField(upload_to='avat/%s',
       processors=[ResizeToFill(200, 200)],
       format='JPEG',
	   options={'quality': 100},
	   null = True,
	   blank = True)

	def __str__(self):
		return self.get_full_name()

	def getTGradosFilter(self):
		fil = Q(pk = 0)
		if self.es_admin:
			fil = Q()
		elif self.es_evaluador:
			for tge in TGrado.objects.filter(director = self):
				fil = fil | Q(pk = tge.id)

			for tge in InformeConcepto.objects.filter(Q(evaluador = self) & (Q(informe__tgrado__estado = TGrado.E_EJECUCION_INFORME) | Q(informe__tgrado__estado = TGrado.E_CANCELADO) | Q(informe__tgrado__estado = TGrado.E_EXITO) | Q(informe__tgrado__estado = TGrado.E_APLAZADO_TGRADO))):
				fil = fil | Q(pk = tge.informe.tgrado.id)

		else:
			for tge in TGradoEstudiantes.objects.filter(estudiante = self):
				fil = fil | Q(pk = tge.tgrado.id)
		return fil

	def getEstudiantes(self):
		fil = Q()
		if self.es_evaluador:
			for tge in TGrado.objects.filter(director = self):
				estud = TGradoEstudiantes.objects.filter(Q(tgrado = tge))
				for e in estud:
					fil = fil | Q(pk = e.estudiante.pk)
		return fil

	def can_operate_tgrado(self, tgrado):
		if self.es_admin:
			return True
		elif self.es_evaluador:
			if tgrado.director == self:
				return True
		return False

class EstadoAcademico(models.Model):
	estado_academico = models.FileField(upload_to='e_a/%s', null = True, blank = True)
	estudiante = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
	fecha_edicion = models.DateTimeField(auto_now = True, null=True, blank=True)

	def __str__(self):
		return str(self.estudiante)


class TGrado(models.Model):
	titulo = models.TextField(null=True, blank=True)
	modalidad = models.CharField(
		max_length = 1,
		choices = MODALIDADES_CHOOSE,
		default = MODALIDAD_APLICACION,
	)
	director = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="director")
	codirector = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, related_name="codirector")
	fecha_creacion = models.DateTimeField(auto_now_add = True, null=True, blank=True)
	fecha_modificiacion = models.DateTimeField(auto_now = True, null=True, blank=True)

	acta_propiedad_intelectual = models.FileField(upload_to='a_p_i/%s', null = True, blank = True)

	E_EJECUCION = "1"
	E_ANTEPROYECTO = "2"
	E_INFORME = "3"
	E_PARA_FIN = "4"
	E_EJECUCION_ANTEPROYECTO = "5"
	E_EJECUCION_INFORME = "6"
	E_EXITO = "7"
	E_CANCELADO = "8"
	E_APLAZADO_TGRADO = "9"
	ESTADO_CHOICES = (
		(E_EJECUCION, "En ejecución"),
		(E_ANTEPROYECTO, "Con Anteproyecto"),
		(E_INFORME, "Con Informe"),
		(E_PARA_FIN, "Pendiente de finalizar"),
		(E_EXITO, "Terminado con éxito"),
		(E_CANCELADO, "Reprobado"),
		(E_APLAZADO_TGRADO, "Aplazado"),
		(E_EJECUCION_ANTEPROYECTO, "Ejecución anteproyecto"),
		(E_EJECUCION_INFORME, "Ejecución informe")
	)
	estado = models.CharField(
		max_length = 1,
		choices = ESTADO_CHOICES,
		default = E_EJECUCION,
	)

	def __str__(self):
		return self.titulo

	def returnEstudiantesFormato(self):
		estudiantes = []
		for r in TGradoEstudiantes.objects.filter(tgrado = self):
			estudiantes.append("<a href='/estudiantes/view/"+str(r.estudiante.pk)+"'>"+r.estudiante.get_full_name()+"</a>")
		return ", ".join(estudiantes)

	def returnEstudiantes(self):
		estudiantes = []
		for r in TGradoEstudiantes.objects.filter(tgrado = self):
			estudiantes.append(str(r.estudiante.get_full_name()))
		return ", ".join(estudiantes)

	def returnAsesores(self):
		asesores = []
		for r in TGradoAsesores.objects.filter(tgrado = self):
			asesores.append(str(r.asesor.get_full_name()))
		return ", ".join(asesores)

	def returnDirector(self):
		director = "No hay director"
		if self.director != None:
			director = self.director.get_full_name()
		return director

	def returnDirectorFormato(self):
		director = "No hay director"
		if self.director != None:
			director = "<a href='/evaluadores/view/"+str(self.director.pk)+"'>"+self.director.get_full_name()+"</a>"
		return director

	def returnCoDirector(self):
		director = "No hay co-director"
		if self.codirector != None:
			director = self.codirector.get_full_name()
		return director

	def returnCoDirectorFormato(self):
		director = "No hay co-director"
		if self.codirector != None:
			director = "<a href='/evaluadores/view/"+str(self.codirector.pk)+"'>"+self.codirector.get_full_name()+"</a>"
		return director


	def has_anteproyecto(self, anteproyecto):
		try:
			anteproyecto = Anteproyecto.objects.get(Q(tgrado = self) & Q(pk = anteproyecto.pk))
			return True
		except:
			return False

	def get_estado(self):
		for data in self.ESTADO_CHOICES:
			if data[0] == self.estado:
				return data[1]
		return "None"

	def get_modalidad(self):
		return get_modalidad(self.modalidad)

class TGradoAsesores(models.Model):
	asesor = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
	tgrado = models.ForeignKey(TGrado, on_delete=models.CASCADE, null=True)

	def __str__(self):
		return "Asesor: " + str(self.asesor) + ", TGrado: " + str(self.tgrado)


class TGradoEstudiantes(models.Model):
	estudiante = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
	tgrado = models.ForeignKey(TGrado, on_delete=models.CASCADE, null=True)
	def __str__(self):
		return "Estudiante: "

class HistoricoTGrado(models.Model):
	tgrado = models.ForeignKey(TGrado, on_delete=models.CASCADE, null=True)
	observaciones = models.TextField(null=True, blank=True)
	fecha = models.DateTimeField(auto_now_add = False, null=True, blank=True)
	usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)

	def __str__(self):
		return "Histórico de Tgrado " + self.tgrado

##### Informe
class Informe(models.Model):
	tgrado = models.ForeignKey(TGrado, on_delete=models.CASCADE, null=True)
	fecha_ingreso = models.DateTimeField(auto_now_add = True, null=True, blank=True)
	fecha_edicion = models.DateTimeField(auto_now = True, null=True, blank=True)
	fecha_sustentacion = models.DateTimeField(null=True, blank=True)
	lugar_sustentacion = models.CharField(max_length = 100, null=True, blank=True)
	observaciones = models.TextField(null=True, blank=True)
	archivo_informe = models.FileField(upload_to='inf/%s', validators=[FileExtensionValidator(allowed_extensions=['pdf'])], null=False)
	def __str__(self):
		return "Informe de " + str(self.tgrado)

class InformeConcepto(models.Model):
	informe = models.ForeignKey(Informe, on_delete=models.CASCADE, null=True)
	evaluador = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
	fecha_asignacion = models.DateTimeField(auto_now_add = True, null=True, blank=True)
	observaciones = RichTextField(null=True, blank=True)

	fecha_modificacion = models.DateTimeField( null=True, blank=True)
	archivo = models.FileField(upload_to='in_co/%s', null=True, blank = True)
	plazo_aplazado = models.IntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(6)])

	CONCEPTO_LISTO_SUSTENTAR = "1"
	CONCEPTO_APLAZADO = "2"
	CONCEPTO_EVALUACION = "3"
	CONCEPTO_REPROBADO = "4"

	CONCEPTOS_CHOICES = (
		(CONCEPTO_LISTO_SUSTENTAR, "Listo para sustentar"),
		(CONCEPTO_APLAZADO, "Aplazado"),
		(CONCEPTO_EVALUACION, "En evaluación"),
		(CONCEPTO_REPROBADO, "Reprobado")
	)
	concepto = models.CharField(
		max_length = 1,
		choices = CONCEPTOS_CHOICES,
		default = CONCEPTO_EVALUACION
	)

	def get_concepto(self):
		for data in self.CONCEPTOS_CHOICES:
			if data[0] == self.concepto:
				return data[1]
		return "None"


	def __str__(self):
		return "Concepto de " + str(self.informe)

	def save(self, *args, **kwargs):
		if not kwargs.pop('skip_lastupdatetime', False):
			self.fecha_modificacion = datetime.now()
		super(InformeConcepto, self).save(*args, **kwargs)


#################### Anteproyecto
class Anteproyecto(models.Model):
	tgrado = models.ForeignKey(TGrado, on_delete=models.CASCADE, null=True)
	fecha_ingreso = models.DateTimeField(auto_now_add = True, null=True, blank=True)
	fecha_edicion = models.DateTimeField(auto_now = True, null=True, blank=True)
	observaciones_anteproyecto = models.TextField(null=True, blank=True)
	meses_desarrollo = models.IntegerField(default=6, help_text="Meses establecidos en el cronograma, (6 - 24)", validators=[MinValueValidator(6), MaxValueValidator(24)])
	meses_desarrollo_prorroga = models.IntegerField(default=0, help_text="valor de 0 - 12", validators=[MinValueValidator(0), MaxValueValidator(12)])
	# Archivo PDF con el anteproyecto de grado
	archivo_anteproyecto = models.FileField(upload_to='a_a/%s', validators=[FileExtensionValidator(allowed_extensions=['pdf'])], null = False)

	# Investigación
	carta_proyecto_vigente = models.FileField(upload_to='c_p_v/%s', null = True, blank = True)
	carta_coordinador = models.FileField(upload_to='c_c/%s', null = True, blank = True)

	# Pasantía
	convenio_marco = models.FileField(upload_to='c_m/%s', null = True, blank = True)
	carta_presentacion = models.FileField(upload_to='c_p/%s', null = True, blank = True)

	def __str__(self):
		return "Anteproyecto de " + str(self.tgrado)[0:20]+"..."


class AnteproyectoConcepto(models.Model):
	anteproyecto = models.ForeignKey(Anteproyecto, on_delete=models.CASCADE, null=True)
	evaluador = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
	fecha_asignacion = models.DateTimeField(auto_now_add = True, null=True, blank=True)
	observaciones = RichTextField(null=True, blank=True)
	fecha_modificacion = models.DateTimeField(null=True, blank=True)
	archivo = models.FileField(upload_to='con_ap/%s', null = True, blank = True)
	plazo_aplazado = models.IntegerField(default=1, validators=[MinValueValidator(1), MaxValueValidator(6)])

	CONCEPTO_APROBADO = "0"
	CONCEPTO_APLAZADO = "1"
	CONCEPTO_EVALUACION = "2"

	CONCEPTOS_CHOICES = (
		(CONCEPTO_APROBADO, "Aprobado"),
		(CONCEPTO_APLAZADO, "Aplazado"),
		(CONCEPTO_EVALUACION, "En evaluación")
	)
	concepto = models.CharField(
		max_length = 1,
		choices = CONCEPTOS_CHOICES,
		default = CONCEPTO_EVALUACION
	)

	def get_concepto(self):
		for data in self.CONCEPTOS_CHOICES:
			if data[0] == self.concepto:
				return data[1]
		return "None"


	def __str__(self):
		return "Concepto de " + str(self.anteproyecto)

	def save(self, *args, **kwargs):
		if not kwargs.pop('skip_lastupdatetime', False):
			self.fecha_modificacion = datetime.now()
		super(AnteproyectoConcepto, self).save(*args, **kwargs)

########### Notas configuración

class SustentacionCriterioEvaluacion(models.Model):
	aspecto = models.TextField(null=True, blank=True)
	peso = models.FloatField(default=0.0, help_text="valor de 0 - 100")
	observaciones = models.TextField(null=True, blank=True)
	ESTADO_CHOICES = (
		(ESTADO_ACTIVO, "Activo"),
		(ESTADO_INACTIVO, "Inactivo")
	)
	estado = models.CharField(
		max_length = 1,
		choices = ESTADO_CHOICES,
		default = ESTADO_ACTIVO,
	)

	def __str__(self):
		return self.aspecto

	def get_estado(self):
		for data in self.ESTADO_CHOICES:
			if data[0] == self.estado:
				return data[1]
		return "None"



class InformeCriterioEvaluacion(models.Model):
	modalidad = models.CharField(
		max_length = 1,
		choices = MODALIDADES_CHOOSE,
		default = MODALIDAD_APLICACION,
	)
	aspecto = models.TextField(null=True, blank=True)
	peso = models.FloatField(default=0.0, help_text="valor de 0 - 100")
	observaciones = models.TextField(null=True, blank=True)

	ESTADO_CHOICES = (
		(ESTADO_ACTIVO, "Activo"),
		(ESTADO_INACTIVO, "Inactivo")
	)
	estado = models.CharField(
		max_length = 1,
		choices = ESTADO_CHOICES,
		default = ESTADO_ACTIVO,
	)

	def __str__(self):
		return str(self.aspecto)

	def get_estado(self):
		for data in self.ESTADO_CHOICES:
			if data[0] == self.estado:
				return data[1]
		return "None"

	def get_modalidad(self):
		return get_modalidad(self.modalidad)

class NotaInforme(models.Model):
	concepto_informe = models.ForeignKey(InformeConcepto, on_delete=models.CASCADE, null=True)
	criterio_evaluacion = models.ForeignKey(InformeCriterioEvaluacion, on_delete=models.CASCADE, null=True)
	valor = models.FloatField(default=0.0)
	fecha_modificacion = models.DateTimeField(auto_now = True, null=True, blank=True)
	observaciones = models.TextField(null=True, blank=True)

	def __str__(self):
		return "Nota de " + str(self.concepto_informe) + " (" + str(self.valor) + ")"


class NotaSustentacion(models.Model):
	concepto_informe = models.ForeignKey(InformeConcepto, on_delete=models.CASCADE, null=True)
	criterio_evaluacion = models.ForeignKey(SustentacionCriterioEvaluacion, on_delete=models.CASCADE, null=True)
	valor = models.FloatField(default=0.0)
	estudiante	= models.ForeignKey(User, on_delete=models.CASCADE, null=True)
	fecha_modificacion = models.DateTimeField(auto_now = True, null=True, blank=True)
	observaciones = models.TextField(null=True, blank=True)

	def __str__(self):
		return "Nota de " + str(self.concepto_informe) + " (" + str(self.valor) + ")"


##################### Calendario configuración

class Calendario(models.Model):
	fecha_inicio = models.DateField(null=True)
	fecha_fin = models.DateField(null=True)
	descripcion = models.TextField(null=True, blank=True)

	def __str__(self):
		return self.descripcion

class BorradorCorreo(models.Model):
	nombre = models.CharField(max_length = 100)
	asunto = models.CharField(max_length = 200)
	cuerpo = RichTextField(null=True, blank=True)
	fecha_ingreso = models.DateTimeField(auto_now_add = True, null=True, blank=True)
	observaciones = models.TextField(null=True, blank=True)
	def __str__(self):
		return self.nombre

class DocumentoVario(models.Model):
	archivo = models.FileField(upload_to='ar_gen/%s')
	nombre = models.CharField(max_length = 100)
	descripcion = models.TextField(null=True, blank=True)
	fecha_ingreso = models.DateTimeField(auto_now_add = True, null=True, blank=True)

	def __str__(self):
		return self.nombre

class ConfiguracionInformes(models.Model):
	cabecera = RichTextField(null=True, blank=True)
	procedimiento = RichTextField("Procedimiento de trabajos de grado", null=True, blank=True)
	escudo_izquierda = models.ImageField(upload_to='conf/es_iz/%s', null=True, blank=True)
	escudo_derecha = models.ImageField(upload_to='conf/es_de/%s', null=True, blank=True)
	formato_evaluacion = models.FileField(upload_to='f_e/%s', validators=[FileExtensionValidator(allowed_extensions=['docx'])], null=True, blank=True)

	def __str__(self):
		return "Configuración informe"

class CorreoAdicional(models.Model):
	nombre = models.CharField(max_length = 100)
	descripcion = models.TextField(null=True, blank=True)
	email = models.CharField(max_length = 200)

	def __str__(self):
		return self.nombre + " - " + self.email


class Mensajes(models.Model):
	asunto = models.CharField(max_length = 100)
	mensaje = RichTextField(null=True, blank=True)

	ASIGNACION_ANTEPROYECTO = "1"
	ASIGNACION_INFORME = "2"
	SOLICITUD_DATOS = "3"
	RECORDATORIO_CONCEPTO = "4"
	RECORDATORIO_SUSTENTACION = "5"

	TIPO_CHOICES = (
		(ASIGNACION_ANTEPROYECTO, "Asignación de anteproyecto"),
		(ASIGNACION_INFORME, "Asignación de informe"),
		(SOLICITUD_DATOS, "Solicitud de datos"),
		(RECORDATORIO_CONCEPTO, "Recordatorio concepto"),
		(RECORDATORIO_SUSTENTACION, "Recordatorio sustentación")
	)
	tipo = models.CharField(
		max_length = 1,
		choices = TIPO_CHOICES,
		null = False
	)

	def get_tipo(self):
		for data in self.TIPO_CHOICES:
			if data[0] == self.tipo:
				return data[1]
		return "None"

	def __str__(self):
		return str(self.asunto) + " - " + str(self.tipo)


class MailTemplates(models.Model):
	titulo = models.CharField(max_length = 100)
	mensaje = RichTextField(null=True, blank=True)
	TIPO_CHOICES = (
		(ESTADO_ACTIVO, "Activo"),
		(ESTADO_INACTIVO, "Inactivo")
	)
	estado = models.CharField(
		max_length = 1,
		choices = TIPO_CHOICES,
		default = ESTADO_ACTIVO
	)
	fecha_ingreso = models.DateTimeField(auto_now_add = True, null=True, blank=True)

	def __str__(self):
		return str(self.titulo)


class BancoProyectos(models.Model):
	proponente = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
	titulo = models.TextField(null=True, blank=True)
	modalidad = models.CharField(
		max_length = 1,
		choices = MODALIDADES_CHOOSE,
		default = MODALIDAD_APLICACION,
	)
	descripcion = models.TextField(null=True, blank=True)
	grupo = models.CharField(max_length = 100)
	fecha_modificacion = models.DateTimeField(auto_now = True, null=True, blank=True)
	TIPO_CHOICES = (
		(ESTADO_ACTIVO, "Activo"),
		(ESTADO_INACTIVO, "Inactivo")
	)
	estado = models.CharField(
		max_length = 1,
		choices = TIPO_CHOICES,
		default = ESTADO_ACTIVO
	)

	def get_modalidad(self):
		return get_modalidad(self.modalidad)

	def __str__(self):
		return str(self.titulo)


class PaginaBlog(models.Model):
	titulo = models.CharField(max_length = 100)
	imagen = models.ImageField(upload_to='pag/%s', null=True, blank=True)
	pagina = RichTextField(null=True, blank=True)
	fecha_edicion = models.DateTimeField(auto_now = True, null=True, blank=True)

	def __str__(self):
		return str(self.titulo)

class CambioContrasena(models.Model):
	usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
	codigo = models.CharField(max_length = 8)
	fecha_solicitud = models.DateTimeField(auto_now = True, null=True, blank=True)

	def __str__(self):
		return str(self.codigo)

class PeticionSugerencia(models.Model):
	usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
	asunto = models.CharField(max_length = 100, null=True, blank=True)
	cuerpo = RichTextField(null=True, blank=True)
	fecha_edicion = models.DateTimeField(auto_now = True, null=True, blank=True)

	def __str__(self):
		return str(self.asunto)
