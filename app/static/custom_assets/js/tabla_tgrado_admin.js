class bdtgrado_view extends PageHandler {
	constructor() {
		super();
		var self = this;
		$('.select_estudiante').select2({
			tags: true,
			ajax: {
				url: '/json/estudiante',
				dataType: 'json',
				quietMillis: 250
			},
			createTag: function (tag) {
				if (tag.term === ""){
					return null;
				}
				return {
					id: tag.term,
					text: tag.term + " - Seleccioname para agregar inexistente",
					isNew : true
				};
			}

		}).on("select2:select", function(e) {
			if(e.params.data.isNew){
				var r = true;
				$("#nuevoUsuarioTgrado").modal("show");
				self.showNormal()
				var text = e.params.data.text;
				var sp = text.split("-");
				$("#id_nombres").val(sp[0]);
				$("#id_apellidos").val("");
				$("#tipo_add").val("add_estudiante");
				$("#tipo_title").text("Estudiante");
				$(this).find('[value="'+e.params.data.id+'"]').replaceWith('');
			}

		});


		$('.select_director').select2({
			tags: true,
			maximumSelectionLength: 1,
			ajax: {
				url: '/json/director',
				dataType: 'json'
			},
			createTag: function (tag) {

				if (tag.term === ""){
					return null;
				}
				return {
					id: tag.term,
					text: tag.term + " - Seleccioname para agregar inexistente",
					isNew : true
				};
			}
		}).on("select2:select", function(e) {
			if(e.params.data.isNew){
				var r = true;
				$("#nuevoUsuarioTgrado").modal("show");
				self.showNormal();
				var text = e.params.data.text;
				var sp = text.split("-");
				$("#id_nombres").val(sp[0]);
				$("#id_apellidos").val("");
				$("#tipo_add").val("add_director");
				$("#tipo_title").text("Director");
				$(this).find('[value="'+e.params.data.id+'"]').replaceWith('');
			}
		});

		$('.select_codirector').select2({
			tags: true,
			maximumSelectionLength: 1,
			ajax: {
				url: '/json/director',
				dataType: 'json'
			},
			createTag: function (tag) {

				if (tag.term === ""){
					return null;
				}
				return {
					id: tag.term,
					text: tag.term + " - Seleccioname para agregar inexistente",
					isNew : true
				};
			}
		}).on("select2:select", function(e) {
			if(e.params.data.isNew){
				var r = true;
				$("#nuevoUsuarioTgrado").modal("show");
				self.showNormal();
				var text = e.params.data.text;
				var sp = text.split("-");
				$("#id_nombres").val(sp[0]);
				$("#id_apellidos").val("");
				$("#tipo_add").val("add_codirector");
				$("#tipo_title").text("Co director");
				$(this).find('[value="'+e.params.data.id+'"]').replaceWith('');
			}
		});

		$('.select_asesores').select2({
			tags: true,
			maximumSelectionLength: 3,
			ajax: {
				url: '/json/director',
				dataType: 'json'
			},
			createTag: function (tag) {

				if (tag.term === ""){
					return null;
				}
				return {
					id: tag.term,
					text: tag.term + " - Seleccioname para agregar inexistente",
					isNew : true
				};
			}
		}).on("select2:select", function(e) {
			if(e.params.data.isNew){
				var r = true;
				$("#nuevoUsuarioTgrado").modal("show");
				self.showNormal();
				var text = e.params.data.text;
				var sp = text.split("-");
				$("#id_nombres").val(sp[0]);
				$("#id_apellidos").val("");
				$("#tipo_add").val("add_asesor");
				$("#tipo_title").text("Asesor");
				$(this).find('[value="'+e.params.data.id+'"]').replaceWith('');
			}
		});
	}

	aditional_proccess_json (json_data, array_objects){
		var newState = new Option(json_data.obj_id[1], json_data.obj_id[0], true, true);
		if (json_data.obj_id[2] == "add_estudiante"){
			$(".select_estudiante").append(newState).trigger('change');
		}
		else if (json_data.obj_id[2] == "add_director"){
			$(".select_director").append(newState).trigger('change');
		}
		else if (json_data.obj_id[2] == "add_asesor"){
			$(".select_asesores").append(newState).trigger('change');
		}
		else if (json_data.obj_id[2] == "add_codirector"){
			$(".select_codirector").append(newState).trigger('change');
		}
	}
}

const bdtgrado = new bdtgrado_view();
