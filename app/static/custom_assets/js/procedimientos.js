$(document).ready(function(){
    // Step show event
    $("#smartwizard").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
       //alert("You are on step "+stepNumber+" now");
       if(stepPosition === 'first'){
           $("#prev-btn").addClass('disabled');
       }
       else if(stepPosition === 'final'){
           $("#next-btn").addClass('disabled');
       }
       else{
           $("#prev-btn").removeClass('disabled');
           $("#next-btn").removeClass('disabled');
       }
    });

    // Smart Wizard 1
    $('#smartwizard').smartWizard({
		selected: 0,
		theme: 'arrows',
		transitionEffect:'fade',
		showStepURLhash: false,
		toolbarSettings: {toolbarPosition: 'bottom'}
    });
	
	$("#smartwizard2").on("showStep", function(e, anchorObject, stepNumber, stepDirection, stepPosition) {
       //alert("You are on step "+stepNumber+" now");
       if(stepPosition === 'first'){
           $("#prev-btn").addClass('disabled');
       }
       else if(stepPosition === 'final'){
           $("#next-btn").addClass('disabled');
       }
       else{
           $("#prev-btn").removeClass('disabled');
           $("#next-btn").removeClass('disabled');
       }
    });

    // Smart Wizard 1
    $('#smartwizard2').smartWizard({
		selected: 0,
		theme: 'arrows',
		transitionEffect:'fade',
		showStepURLhash: false,
		toolbarSettings: {toolbarPosition: 'bottom'}
    });


});


