
class notas_class extends PageHandler {
	constructor() {
		super();
		$(".input-nota").keyup(this.change_item);
		$(".input-nota").blur(this.check_nota);
		$(".input-nota").focus(function(){
			$(this).select();
		})
		$("input").tooltip();
		this.calculate_notas();
	}

	check_nota(e) {
		e.preventDefault();
		var last_nota = parseFloat($(this).attr("last_nota"));
		var valor_nota = parseFloat($(this).val());
		if (last_nota != valor_nota) {
			self.save_nota(this, false);
		}
	}

	change_item(e) {
		e.preventDefault();
		var valor_nota = $(this).val().replace(",", ".");
		if (valor_nota != $(this).val()){
			$(this).val(valor_nota);
		}
		if(e.which==13) {
			self.save_nota(this, true);
		}
	}

	save_nota(item, jump){
		var nota = $(item).attr("nota");

		var campo_id = $(item).attr("campo_id");
		var tipo = $(item).attr("tipo");
		var estudiante = $(item).attr("estudiante");
		var id = parseInt(campo_id.split("_")[1]);
		var porcentaje = parseFloat($("#porcentaje_"+id).text());
		var valor_nota = parseFloat($(item).val());
		if (valor_nota > 5.0) {
			if (valor_nota > 10 && valor_nota < 100){
				$(item).val(valor_nota / 10);
			}
			valor_nota = parseFloat($(item).val());
			if (valor_nota > 5.0) {
				$(item).val(5.0);
			}
		}
		else if (valor_nota < 0.0) {
			$(item).val(0.0);
		}
		if ($(item).val() == "" || isNaN(valor_nota)){
			$(item).val(0.0);
		}
		$(item).val(parseFloat($(item).val()).toFixed(1));
		valor_nota = parseFloat($(item).val());

		if (!isNaN(valor_nota)) {
			$("#acum_"+id).text(((porcentaje * valor_nota) / 100).toFixed(2));
			var tgrado = $("#tgrado").val();
			var data_send = {
				"valor_nota" : valor_nota,
				"nota_id" : nota,
				"ajaxRequest" : "Yes",
				"ajax_action" : "editNota",
				"estudiante" : estudiante,
				"tgrado" : tgrado,
				"tipo" : tipo
			}
			var url = $("#form_action").val();
			self.sendData(self, data_send, [id, valor_nota], url);
		}
		else{
			alert("Este campo no soporta letras");
		}


		$(item).attr("last_nota", valor_nota);
		if (jump){
			$("[campo_id='campo_" + (id + 1) + "']").focus();
		}


		self.calculate_notas();


	/*
		else if (e.which == 38){
			$("[campo_id='campo_" + (id) + "']").blur();
			$("[campo_id='campo_" + (id - 1) + "']").focus();
		}
	*/
	}

	calculate_notas() {
		var inputs = $(".sustentacion_nota_input");
		var nota = 0.0;
		inputs.each(function(){
			nota += parseFloat($(this).text());
		});
		$(".nota_sustentacion").text(nota.toFixed(1));

		$(".sustentacion_final").each(function(){
			var estudiante = $(this).attr("estudiante");
			var nota_dos = 0.0;
			$(".nota_sustentacion_input_" + estudiante).each(function(){
				nota_dos += parseFloat($(this).text());
			});
			$(this).text((nota + nota_dos).toFixed(1));
		});
	}

	proccess_error (objects) {
		var campo_id = objects;
		//$("[campo_id='nota_" + String(campo_id) + "']").attr("title", "Hubo un error al guardar la nota").tooltip("update");
	}

	proccess_json (json, data) {
		var campo_id = data[0];
		var valor = data[1];
		if (json.concept == "success") {
			$("#fecha_" + (campo_id)).attr("data-original-title", json.fecha).tooltip();
			//$("[campo_id='campo_" + (campo_id + 1) + "']").focus();
			$("[campo_id='campo_" + (campo_id) + "']").addClass("success").removeClass("error");
		}
		else {
			$("[campo_id='campo_" + (campo_id) + "']").addClass("error").removeClass("success").val(valor);
		}
	}
}

const not = new notas_class();
