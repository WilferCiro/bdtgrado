
class programacion extends PageHandler {
	constructor() {
		super();
		$("#guardados").hide();
		$("#error_guardados").hide();
		$("#definirFechas").click(function(){
			var send = {};
			var datos = [];
			$(".dateSelected").each(function(i, obj) {
				var value = $(obj).val();
				var tgrado_id = $(obj).attr("tgrado_id");
				var lugar = $(".lugar_" + tgrado_id).val();
				datos.push({"value" : value, "tgrado_id" : tgrado_id, "lugar" : lugar});
			});
			send = {
				"datos[]" : JSON.stringify(datos),
				"ajaxRequest" : "Yes",
				"ajax_action" : "editData",
			};
			$("#guardados").show("quick").html("Procesando la petición...");
			self.sendData(self, send, [], "/programacion/sustentaciones");
			return false;
		});
	}
	
	aditional_proccess_json (json_data, array_objects){
		if (json_data.error == false){
			$("#guardados").show("quick").html("Datos procesados con éxito");
		}
		else{
			$("#error_guardados").show('quick').html("Hubo un error al realizar la operación<br />" + json_data.errores);
		}
	}

}

const progra = new programacion();



