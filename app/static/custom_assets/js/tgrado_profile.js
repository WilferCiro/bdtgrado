class tgrado_profile extends PageHandler {
	constructor() {
		super();
		$("#form_evaluadores").hide();
		$("#tabla_anteproyecto_historicos").hide();
		$("#add_evaluadores").click(function(event){
			$("#form_evaluadores").toggle("200");
		});
		$("#btn_historico_anteproyecto").click(function(event){
			$("#tabla_anteproyecto_historicos").toggle("200");
		});
		$(".editarConcepto").click(this.editarConcepto);

		$(".edit_concepto_informe_btn").click(this.edit_concepto_informe_btn);

		var tgrado_pk = 2;

		$('.select_evaluador').select2({
			tags: true,
			maximumSelectionLength: parseInt($("#maxSelection").val()),
			ajax: {
				url: '/json/director?tgrado_pk=' + tgrado_pk,
				dataType: 'json'
			},
			createTag: function (tag) {

				if (tag.term === ""){
					return null;
				}
				return {
					id: tag.term,
					text: tag.term + " - Seleccioname para agregar evaluador",
					isNew : true
				};
			}
		}).on("select2:select", function(e) {
			if(e.params.data.isNew){
				var r = true;
				$("#nuevoUsuarioTgrado").modal("show");
				var text = e.params.data.text;
				var sp = text.split("-");
				$("#id_nombres").val(sp[0]);
				$("#id_apellidos").val("");
				$("#tipo_add").val("add_evaluador");
				$("#tipo_title").text("Evaluador");
				$(this).find('[value="'+e.params.data.id+'"]').replaceWith('');
			}
		}).on('change', function(e) {
			var data = $(".select_evaluador option:selected").text();
			if (data == ""){
				$("#btn_evaluador_anteproyecto").attr("disabled", "true");
			}
			else{
				$("#btn_evaluador_anteproyecto").removeAttr("disabled");
			}
		});

	}

	edit_concepto_informe_btn (event) {
		var concepto_id = $(this).attr("concepto");
		$("#concepto_id_editar").val(concepto_id);
		var value_concepto = $(this).attr("value_concepto");
		$("#id_concepto").val(value_concepto);
		$("#editConceptoInforme").modal("show");
	}

	editarConcepto (event){
		var concepto_id = $(this).attr("concepto_id");
		$("#anteproyecto_concepto_id").val(concepto_id);
		$("#editConceptoAnteproyecto").modal("show");
		var data = {"getObjectData" : "get", object_id : concepto_id};
		var url = $("#form_action").val();
		self.sendData(self, data, [], url)
	}

	aditional_proccess_json (json_data, array_objects){
		if (json_data.get_json){
			$.each(json_data,function(index, value){
				$("#id_" + index).val(value);
				$("select#id_"+index).val(value).change();
				$("textarea#id_" + index).text(value);
			});
			$("#modalEdit").modal("show");
		}
		else{
			var newState = new Option(json_data.obj_id[1], json_data.obj_id[0], true, true);
			if (json_data.obj_id[2] == "add_evaluador"){
				$(".select_evaluador").append(newState).trigger('change');
			}
		}
	}
}

const tgrado_p = new tgrado_profile();
