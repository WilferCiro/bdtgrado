class email_view extends PageHandler {
	constructor() {
		super();
		$('.select_email').select2({
			tags: true,
			ajax: {
				url: '/json/correos',
				dataType: 'json',
				quietMillis: 250
			},
			createTag: function (tag) {
				if (tag.term === ""){
					return null;
				}
				return {
					id: tag.term,
					text: tag.term + " - Seleccioname para agregar correo",
					isNew : true
				};
			}
			
		}).on("select2:select", function(e) {
			if(e.params.data.isNew){
				$(this).find('[value="'+e.params.data.id+'"]').replaceWith('<option selected value="'+e.params.data.id+'">'+e.params.data.text.split("-")[0]+'</option>');
			}

		});
	}
	
	aditional_proccess_json (json_data, array_objects){		
		/*var newState = new Option(json_data.obj_id[1], json_data.obj_id[0], true, true);
		if (json_data.obj_id[2] == "add_estudiante"){
			$(".select_estudiante").append(newState).trigger('change');
		}*/
	}
}

const email = new email_view();
