# Generated by Django 2.1.3 on 2019-03-13 16:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0003_paginablog'),
    ]

    operations = [
        migrations.AddField(
            model_name='paginablog',
            name='fecha_edicion',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
    ]
