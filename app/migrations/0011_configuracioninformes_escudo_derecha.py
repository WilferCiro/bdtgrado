# Generated by Django 2.2.6 on 2019-11-03 13:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0010_auto_20191103_1346'),
    ]

    operations = [
        migrations.AddField(
            model_name='configuracioninformes',
            name='escudo_derecha',
            field=models.ImageField(default=None, upload_to='conf/es_de/%s'),
            preserve_default=False,
        ),
    ]
