from django.shortcuts import render
from django.template import RequestContext


def handler404(request, *args, **argv):
	data_return = {
		"error_code" : "404",
		"message" : "al parecer esta página no existe",
		"error_title" : "404 No encontrado :("
	}
	response = render(request, 'error/error_page.html', data_return)
	return response


def handler500(request, *args, **argv):
	data_return = {
		"error_code" : "500",
		"message" : "al parecer tenemos un error, los administradores ya estan trabajando en ello :3 ",
		"error_title" : "500 Error interno"
	}
	response = render(request, 'error/error_page.html', data_return)
	return response

def handler400(request, *args, **argv):
	data_return = {
		"error_code" : "400",
		"message" : "al parecer tenemos un error, los administradores ya estan trabajando en ello :3 ",
		"error_title" : "400 peticion erronea"
	}
	response = render(request, 'error/error_page.html', data_return)
	return response

def handler403(request, *args, **argv):
	data_return = {
		"error_code" : "403",
		"message" : "al parecer no tienes permiso de estar aquí >:( ",
		"error_title" : "403 prohibido"
	}
	response = render(request, 'error/error_page.html', data_return)
	return response
