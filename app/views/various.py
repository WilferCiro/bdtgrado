from app.views.main import Main, MainTable
from app.models import *
from django.shortcuts import render
from django.views import View
import json

from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render

from django.utils import translation
from django.utils.translation import ugettext_lazy as _
from django.core.paginator import Paginator


class IndexPage(Main, View):
	template = "index.html"
	page_title = _("Inicio")
	def get_data(self, request, args):
		import datetime
		x = datetime.datetime.now()
		fecha_desde = x.strftime('%Y-%m-%d')
		fechas_cal = Calendario.objects.filter(Q(fecha_inicio__gte = fecha_desde))

		data_return = {}
		if self.user.es_admin:
			anteproyectos_falta = []
			informes_falta = []
			fecha_sustentacion_falta = []
			terminar_falta = []

			for tgrado in TGrado.objects.filter(estado = TGrado.E_ANTEPROYECTO):
				anteproyectos_falta.append({
					"pk" : tgrado.pk,
					"tgrado_titulo" : tgrado.titulo
				})
			for tgrado in TGrado.objects.filter(estado = TGrado.E_INFORME):
				informes_falta.append({
					"pk" : tgrado.pk,
					"tgrado_titulo" : tgrado.titulo
				})
			tgrados = []
			for concepto in InformeConcepto.objects.filter(Q(concepto = InformeConcepto.CONCEPTO_LISTO_SUSTENTAR) & Q(informe__fecha_sustentacion = None)):
				tgrado = concepto.informe.tgrado
				if tgrado.estado == TGrado.E_EJECUCION_INFORME and not tgrado in tgrados:
					tgrados.append(tgrado)
					fecha_sustentacion_falta.append({
					"pk" : tgrado.pk,
					"tgrado_titulo" : tgrado.titulo
				})

			for tgrado in TGrado.objects.filter(estado = TGrado.E_PARA_FIN):
				terminar_falta.append({
					"pk" : tgrado.pk,
					"tgrado_titulo" : tgrado.titulo
				})


			"""for tgrado in TGrado.objects.filter(estado = TGrado.E_EJECUCION):
				anteproyecto = Anteproyecto.objects.filter(tgrado = tgrado).last()
				if anteproyecto != None:
					concepto = AnteproyectoConcepto.objects.filter(anteproyecto = anteproyecto).last()
					if concepto == None:
						anteproyectos_falta.append({
							"pk" : tgrado.pk,
							"tgrado_titulo" : tgrado.titulo
						})
				informe = Informe.objects.filter(tgrado = tgrado).last()
				if informe != None:
					concepto = InformeConcepto.objects.filter(informe = informe)
					if not concepto.exists():
						informes_falta.append({
							"pk" : tgrado.pk,
							"tgrado_titulo" : tgrado.titulo
						})
					else:
						existe = False
						for con in concepto:
							nota = NotaInforme.objects.filter(concepto_informe = con)
							for no in nota:
								if no.valor > 0:
									existe = True
									break
							if not existe:
								nota = NotaSustentacion.objects.filter(concepto_informe = con)
								for no in nota:
									if no.valor > 0:
										existe = True
										break
						if existe:
							terminar_falta.append({
								"pk" : tgrado.pk,
								"tgrado_titulo" : tgrado.titulo
							})"""
			data_return["anteproyectos_falta"] = anteproyectos_falta
			data_return["informes_falta"] = informes_falta
			data_return["fecha_sustentacion_falta"] = fecha_sustentacion_falta
			data_return["terminar_falta"] = terminar_falta
			if len(anteproyectos_falta) + len(informes_falta) + len(terminar_falta) + len(fecha_sustentacion_falta) > 0:
				data_return["admin_acciones"] = True

		if self.user.es_evaluador:
			lista_con_an = []
			lista_con_in = []
			lista_con_no = []
			for concepto in AnteproyectoConcepto.objects.filter(Q(evaluador = self.user) & Q(concepto = AnteproyectoConcepto.CONCEPTO_EVALUACION)):
				lista_con_an.append({
					"pk" : concepto.anteproyecto.tgrado.pk,
					"tgrado_titulo" : concepto.anteproyecto.tgrado.titulo
				})

			for concepto in InformeConcepto.objects.filter(Q(evaluador = self.user) & Q(concepto = InformeConcepto.CONCEPTO_EVALUACION)):
				lista_con_in.append({
					"pk" : concepto.informe.tgrado.pk,
					"tgrado_titulo" : concepto.informe.tgrado.titulo
				})


			for concepto in InformeConcepto.objects.filter(Q(evaluador = self.user) & Q(concepto = InformeConcepto.CONCEPTO_LISTO_SUSTENTAR)):
				if concepto.informe.tgrado.estado == TGrado.E_EJECUCION_INFORME or concepto.informe.tgrado.estado == TGrado.E_PARA_FIN:
					existe = False
					nota = NotaInforme.objects.filter(concepto_informe = concepto)
					"""for no in nota:
						if no.valor > 0:
							existe = True
							break
					if not existe:
						nota = NotaSustentacion.objects.filter(concepto_informe = concepto)
						for no in nota:
							if no.valor > 0:
								existe = True
								break"""
					if not existe:
						lista_con_no.append({
							"pk" : concepto.informe.tgrado.pk,
							"tgrado_titulo" : concepto.informe.tgrado.titulo
						})
			data_return["lista_con_an"] = lista_con_an
			data_return["lista_con_in"] = lista_con_in
			data_return["lista_con_no"] = lista_con_no
			if len(lista_con_an) + len(lista_con_in) + len(lista_con_no) > 0:
				data_return["evaluador_acciones"] = True

		if not self.user.es_estudiante:
			nro_tgrados = TGrado.objects.all().count()
			nro_tgrados_finalizados = TGrado.objects.filter(estado = TGrado.E_EXITO).count()
			nro_estudiantes = User.objects.filter(es_estudiante = True).count()
			nro_evaluadores = User.objects.filter(es_evaluador = True).count()
			data_return["nro_tgrados"] = nro_tgrados
			data_return["nro_tgrados_finalizados"] = nro_tgrados_finalizados
			data_return["nro_estudiantes"] = nro_estudiantes
			data_return["nro_evaluadores"] = nro_evaluadores
		else:
			tgrados = []
			for tge in TGradoEstudiantes.objects.filter(estudiante = self.user):
				tgrado = tge.tgrado
				tgrados.append({
					"titulo" : tgrado.titulo,
					"modalidad" : tgrado.get_modalidad(),
					"pk" : tgrado.pk,
				})
			if len(tgrados) > 0:
				data_return["tgrados"] = tgrados
			else:
				banco_list = BancoProyectos.objects.all().order_by("estado", "fecha_modificacion")
				page = request.GET.get('page', 1)

				paginator = Paginator(banco_list, 15)
				try:
					banco = paginator.page(page)
				except PageNotAnInteger:
					banco = paginator.page(1)
				except EmptyPage:
					banco = paginator.page(paginator.num_pages)

				lista = []
				for ban in banco:
					es_activo = False
					if ban.estado == ESTADO_ACTIVO:
						es_activo = True
					if ban.proponente == None:
						proponente = ""
						proponente_correo = ""
					else:
						proponente = ban.proponente.get_full_name()
						proponente_correo = ban.proponente.email
					lista.append({
						"titulo" : ban.titulo,
						"descripcion" : ban.descripcion,
						"proponente" : proponente,
						"correo_proponente" : proponente_correo,
						"grupo" : ban.grupo,
						"modalidad" : ban.get_modalidad(),
						"es_activo" : es_activo,
						"fecha_modificacion" : ban.fecha_modificacion
					})
				data_return["lista_banco"] = lista
				data_return["banco"] = banco

		data_return["fechas_cal"] = fechas_cal


		return data_return


def ChangeIdioma(request):
	idioma = request.GET.get("idioma", "es")
	idiomas_validos = ["en", "es"]
	if idioma in idiomas_validos:
		translation.activate(idioma)
		request.session[translation.LANGUAGE_SESSION_KEY] = idioma
	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def logoutView(request):
	"""
		Logout from application, and returns to blog
	"""
	from django.contrib.auth import logout
	logout(request)
	return HttpResponseRedirect('/index/?success')

def loginView (request):
	"""
		Log in the page
	"""
	from django.contrib.auth import authenticate, login
	username = request.POST.get("usuario", None)
	password = request.POST.get("contrasena", None)
	recordarme = request.POST.get("recordarme", False)
	try:
		user = authenticate(username=username, password=password)
		if user.es_admin or user.es_estudiante or user.es_evaluador:
			if request.POST.get('remember_me'):
				# Expira en 2 semanas
				request.session.set_expiry(1209600)
				request.session.modified = True
			login(request, user)
			return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
		else:
			return HttpResponseRedirect('/index/?error')
	except Exception as e:
		print(e)
		return HttpResponseRedirect('/index/?error')


class SearchPage(Main, View):
	"""
		view of search page
	"""
	template = 'funciones/search.html'

	def get_data(self, request, kwargs):
		busqueda = request.GET.get("q", "")
		full_items = []
		for men in self.menu:
			if busqueda.lower() in men["label"].lower() or busqueda.lower() in men["content"].lower():
				if men["url"] != "":
					full_items.append(men)
			for item in men["submenu"]:
				if busqueda.lower() in item["label"].lower() or busqueda.lower() in item["content"].lower():
					full_items.append(item)

		estudiantes = User.objects.filter(Q(es_estudiante = True) & Q(last_name__icontains = busqueda) | Q(first_name__icontains = busqueda))
		for est in estudiantes:
			data = dict(label = "Estudiante: " + est.get_full_name(), url = "/estudiantes/view/"+str(est.pk), content = "Datos del estudiante")
			full_items.append(data)

		evaluadores = User.objects.filter(Q(es_evaluador = True) & Q(last_name__icontains = busqueda) | Q(first_name__icontains = busqueda))
		for eva in evaluadores:
			data = dict(label = "Evaluador: " + eva.get_full_name(), url = "/evaluador/view/"+str(eva.pk), content = "Datos del evaluador")
			full_items.append(data)

		tgrado = TGrado.objects.filter(Q(titulo__icontains = busqueda))
		for tg in tgrado:
			data = dict(label = "Trabajo de grado: " + tg.titulo, url = "/tgrado/view/"+str(tg.pk), content = "Datos del trabajo de grado")
			full_items.append(data)

		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Búsqueda de \""+busqueda+"\"", "")
		return dict(query = busqueda, full_items = full_items)
