from app.views.main import Main, MainTable
from django.views import View
from django.db.models import Q

from app.models import *
from app.forms.calendario import *
from django.http import JsonResponse
from django.core import serializers


class CalendarioView(Main, View):
	template = "calendario/calendario.html"
	page_title = "Calendario"
	form_action = "/calendario/"
	
	def get_data(self, request, kwargs):
		
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Calendario", "")
		
		self._add_css("calendario/fullcalendar.css")
		self._add_css("calendario/fullcalendar.print.css")
		self._add_js("calendario/jquery-ui.custom.min.js")
		self._add_js("calendario/fullcalendar.js")
		if self.user.es_admin:
			self._add_js("calendario_handler.js")
			self._add_aditional_template("varios/dialogo_agregar.html")
			self._add_aditional_template("varios/dialogo_editar.html")
			self._add_aditional_template("varios/dialogo_eliminar.html")			
		else:
			self._add_js("calendario_handler_noAdmin.js")		
		
		data_return = dict()
		
		data_return["form_add"] = editCalendar()
		data_return["form_edit"] = editCalendar()
		data_return["form_action"] = "/calendario/"
		data_return["delete_register"] = True
		
		return data_return
	
	def _deleteData(self, request):
		"""
			Deletes a table register
		"""
		if self.user.es_admin:
			object_id = request.POST.get("object_id", None)
			if object_id != None:
				#if self._checkPermission(object_id, user):
				sed = Calendario.objects.get(pk = object_id)
				sed.delete()
				return True
				self._add_error("No se pudo eliminar el objeto")
			else:
				self._add_error("Usted no tiene permisos para eliminar este objeto")
		return False
	
	
	def _addData(self, request):
		"""
			Add a table register
		"""
		if self.user.es_admin:
			form_add = editCalendar(data = request.POST)
			if form_add.is_valid():
				form_add.save()
				return [True, form_add.cleaned_data["descripcion"]]
			self._add_error(form_add.errors)
		return False
	
	def _editData(self, request):
		"""
			edit a table register
		"""
		if self.user.es_admin:
			object_id = request.POST.get("object_id", None)
			if object_id != None:
				sed = Calendario.objects.get(pk = object_id)
				tipoEdit = request.POST.get("tipoEdit", None)
				if tipoEdit == "only_date":
					fecha_inicio = request.POST.get("fecha_inicio", None)
					fecha_fin = request.POST.get("fecha_fin", None)
					sed.fecha_inicio = fecha_inicio
					sed.fecha_fin = fecha_fin
					sed.save()
					return True
				else:
					form_edit = editCalendar(data = request.POST, instance=sed)
					if form_edit.is_valid():
						form_edit.save()	
						return True
					
					self._add_error(form_edit.errors)
		return False
	
	def _requestData(self, request):
		date_start = request.POST.get("start", 0)
		date_end = request.POST.get("end", 0)
		dates = []
		from datetime import datetime
		fecha_desde = datetime.utcfromtimestamp(int(date_start)).strftime('%Y-%m-%d')
		fecha_hasta = datetime.utcfromtimestamp(int(date_end)).strftime('%Y-%m-%d')
		
		dates = []
		for d in Calendario.objects.filter(Q(fecha_inicio__range = (fecha_desde, fecha_hasta)) | Q(fecha_fin__range = (fecha_desde, fecha_hasta))):
			dates.append({				
				"allDay": True,
				"title": d.descripcion,
				"start": d.fecha_inicio,
				"end": d.fecha_fin,
				"id": d.pk
			})
		
		return JsonResponse(dates, safe=False) 
		
		
	def _ajaxRequest(self, request):
		"""
			Proccess the ajax data
		"""
		action = request.POST.get("ajax_action", None)
		error = False
		obj_id = 0
		
		if not self.user.is_anonymous and self.user.es_admin:
			if action == "editar_modal":
				data = self._editData(request)
			if action == "agregar_modal":
				data = self._addData(request)
			if action == "eliminar_modal":
				data = self._deleteData(request)
				
		if action == "request_data":
			return self._requestData(request)
		
		if type(data) is list or type(data) is tuple:
			error = not(data[0])
			obj_id = data[1]
		else:
			error = not(data)
		print(self.errors)	
		data = {
			"errores" : ", ".join(self.errors),
			"error": error,
			"obj_id" : obj_id
		}
		return JsonResponse(data)
	
	
	
