from app.views.main import MainTable, Main
from django.views import View
from django.db.models import Q

from app.models import *
from app.forms.documentos import *

class DocumentosView(MainTable, View):
	"""
		Vista de los documentos
	"""
	page_title = "Documentos"
	register_name = "documento"	
	form_action = "/documentos/"	
	model_object = DocumentoVario
	table_columns =	dict(nombre = "Nombre", descripcion = "Descripción", archivo = "Archivo", fecha_ingreso__date_format = "Fecha Ingreso")
	form_edit = editDocumento
	form_add = editDocumento
	delete_register = True
	
	edit_aparte = True
		
	def _preProccess(self, request):
		if self.user.es_admin:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
	
	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Documentos", "")
	
	def _getFilerTable(self, value):
		fil = Q()					
		if value != "":
			fil = fil & (Q(nombre__icontains = value) | Q(descripcion__icontains = value) | Q(fecha_ingreso__icontains = value) | Q(usuario__first_name__icontains = value) | Q(usuario__last_name__icontains = value))
		return fil


