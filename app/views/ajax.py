from app.models import *
from django.http import JsonResponse
from django.views import View
from django.db.models import Q

from app.forms.ajax import *


class EstudianteAjax(View):
	def get(self, request, *args, **kargs):
		data = dict()
		if request.user.is_authenticated:
			term = request.GET.get("term", "")
			estudiantes = User.objects.filter((Q(first_name__icontains = term) | Q(last_name__icontains = term)) & Q(es_estudiante = True))[0:10]
			in_data = []
			for ci in estudiantes:
				in_data.append({"text" : ci.first_name + " " + ci.last_name, "id" : ci.pk, "selected": "true"})
			data = {
				'results': in_data,
			}
		return JsonResponse(data)

class DirectorAjax(View):
	def get(self, request, *args, **kargs):
		data = dict()
		if request.user.is_authenticated:
			term = request.GET.get("term", "")
			tgrado_pk = request.GET.get("tgrado_pk", None)
			evaluadores = User.objects.filter((Q(first_name__icontains = term) | Q(last_name__icontains = term)) & Q(es_evaluador = True))[0:10]
			in_data = []
			for ci in evaluadores:
				in_data.append({"text" : ci.first_name + " " + ci.last_name, "id" : ci.pk})
			data = {
				'results': in_data
			}
		return JsonResponse(data)

class CorreosAjax(View):
	def get(self, request, *args, **kargs):
		data = dict()
		if request.user.is_authenticated:
			term = request.GET.get("term", "")
			evaluadores = User.objects.filter((Q(first_name__icontains = term) | Q(last_name__icontains = term) | Q(email__icontains = term)) & ~Q(email = ""))[0:10]
			in_data = []
			for ci in evaluadores:
				in_data.append({"text" : ci.first_name + " " + ci.last_name + " - " + ci.email, "id" : ci.pk})

			#aditional = CorreoAdicional.objects.filter(Q(nombre__icontains = term) & ~Q(email = ""))
			#for ad in aditional:
			#	in_data.append({"text" : ad.nombre + " - " + ad.email, "id" : ad.email})

			data = {
				'results': in_data
			}
		return JsonResponse(data)

class NotificacionesAjax(View):
	def post(self, request, *args, **kargs):
		data = dict()
		if request.user.is_authenticated:
			import datetime
			x = datetime.datetime.now()
			fecha_desde = x.strftime('%Y-%m-%d')
			fecha_hasta = x.strftime('%Y-%m-%d')
			eventos = []
			for c in Calendario.objects.filter(Q(fecha_inicio__range = (fecha_desde, fecha_hasta)) | Q(fecha_fin__range = (fecha_desde, fecha_hasta))):
				eventos.append({
					"evento" : c.descripcion,
					"fecha_inicio" : c.fecha_inicio,
					"fecha_fin" : c.fecha_fin,
					"href" : "/calendario"
				})

			data = {
				"notificacion" : "yes",
				"results" : eventos
			}

		return JsonResponse(data)


class TGradosParaFecha(View):
	def get(self, request, *args, **kargs):
		data = dict()
		if request.user.is_authenticated:
			in_data = []
			tgrados = []
			for concepto in InformeConcepto.objects.filter(Q(concepto = InformeConcepto.CONCEPTO_LISTO_SUSTENTAR) & Q(informe__fecha_sustentacion = None)):
				tgrado = concepto.informe.tgrado
				if tgrado.estado == TGrado.E_EJECUCION_INFORME and not tgrado in tgrados:
					tgrados.append(tgrado)
					in_data.append({
						"text" : tgrado.titulo,
						"id" : tgrado.pk
					})
			data = {
				'results': in_data
			}
		return JsonResponse(data)

class AddDataModal(View):

	def post(self, request, *args, **kwargs):
		data_ret = False
		obj_id = None
		tipo_add = request.POST.get("tipo_add", None)
		data_ret = dict()
		descripcion = ""
		if request.user.is_authenticated:
			if tipo_add == "add_estudiante" or tipo_add == "add_director" or tipo_add == "add_asesor"  or tipo_add == "add_evaluador" or tipo_add == "add_codirector":
				form_add = add_usuario(data = request.POST)
				if form_add.is_valid():
					nombres = form_add.cleaned_data["nombres"]
					apellidos = form_add.cleaned_data["apellidos"]
					documento = form_add.cleaned_data["documento"]
					try:
						user = User.objects.create_user(username=documento, password=documento)
						user.last_name = apellidos
						user.first_name = nombres
						user.documento = documento
						if tipo_add == "add_director" or tipo_add == "add_asesor" or tipo_add == "add_evaluador" or tipo_add == "add_codirector":
							user.es_evaluador = True
						else:
							user.es_estudiante = True
						user.save()

						data_ret = [True, [user.pk, user.get_full_name(), tipo_add]]
					except Exception as e:
						print(e)
						descripcion = "Ya existe un usuario con este documento"
						data_ret = False

			if type(data_ret) is list or type(data_ret) is tuple:
				error = not(data_ret[0])
				obj_id = data_ret[1]
			else:
				error = not(data_ret)
			data_ret = {
				'error': error,
				'descripcion' : descripcion,
				"obj_id" : obj_id
			}
		return JsonResponse(data_ret)
