from app.views.main import Main, MainTable
from django.views import View
from django.db.models import Q

from app.models import *
from app.forms.mas_funciones import *
from django.http import JsonResponse
import json
from django.utils.html import strip_tags

class BancoProyectos(MainTable, View):
	page_title = "Banco de tgrados"
	register_name = "registro"	
	form_action = "/proyectos/banco"
	model_object = BancoProyectos
	table_columns =	dict(titulo = "Título", proponente = "Proponente", modalidad__format = "Modalidad", grupo = "Grupo", fecha_modificacion__date_format = "Fecha modificación")
	return_edit_columns = ["titulo", "proponente", "modalidad", "grupo", "descripcion", "estado"]
	form_edit = editBanco
	form_add = editBanco
	delete_register = True
	
	edit_aparte = False
	mostrar_control_activos = True
		
	def _getFormatRow(self, column, obj):
		if column == "modalidad":
			return obj.get_modalidad()
		return ""	
	
	def can_edit_row(self, obj):
		if self.user == obj.proponente or self.user.es_admin:
			return True
		return False
	
	def can_delete_row(self, obj):
		if self.user == obj.proponente or self.user.es_admin:
			return True
		return False
	
	def _preProccess(self, request):
		if not self.user.es_estudiante:
			if self.user.es_evaluador and not self.user.es_admin:
				self.form_edit = editBancoDirector
				self.form_add = editBancoDirector
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
	
	def _addData(self, request):
		if self.can_add:
			form_edit = self.form_edit(data = request.POST)
			if form_edit.is_valid():
				try:
					new_data = form_edit.save(commit = False)
					new_data.proponente = self.user
					new_data.save()
					form_edit.save_m2m()
					return True
				except Exception as e:
					print(e)
					self._add_error("Ya existe un campo con estos datos")
					return False
			self._add_error(form_edit.errors)
		else:
			self._add_error("Usted no tiene permisos para añadir este objeto")
		return False
	
	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Banco de proyectos", "")
	
	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(titulo__icontains = value) | Q(proponente__last_name__icontains = value) | Q(proponente__first_name__icontains = value) | Q(modalidad__icontains = value))
		return fil


class PQRS(MainTable, View):
	page_title = "PQRS"
	register_name = "PQRS"
	form_action = "/pqrs/"
	model_object = PeticionSugerencia
	table_columns = dict(asunto = "Asunto", usuario = "Usuario", cuerpo__format = "Cuerpo", fecha_edicion__date_format = "Fecha modificación")
	return_edit_columns = ["asunto", "usuario", "cuerpo"]
	form_edit = editPQRS
	form_add = editPQRS
	delete_register = True
	
	edit_aparte = True
		
	def _getFormatRow(self, column, obj):
		if column == "cuerpo":
			return strip_tags(obj.cuerpo[0:250]) + "..."
		return ""
	
	def can_edit_row(self, obj):
		if self.user == obj.usuario or self.user.es_admin:
			return True
		return False
	
	def can_delete_row(self, obj):
		if self.user == obj.usuario or self.user.es_admin:
			return True
		return False
	
	def _preProccess(self, request):
		if not self.user.es_estudiante:
			self.can_edit = True
			self.can_add = True
			if self.user.es_evaluador and not self.user.es_admin:
				self.form_edit = editPQRSDirector
				self.form_add = editPQRSDirector
			elif self.user.es_admin:
				self.can_add = False
				self.can_edit = False
			self.can_delete = True
	
	def _addData(self, request):
		if self.can_add:
			form_edit = self.form_edit(data = request.POST)
			if form_edit.is_valid():
				try:
					new_data = form_edit.save(commit = False)
					new_data.usuario = self.user
					new_data.save()
					form_edit.save_m2m()
					return True
				except Exception as e:
					print(e)
					self._add_error("Ya existe un campo con estos datos")
					return False
			self._add_error(form_edit.errors)
		else:
			self._add_error("Usted no tiene permisos para añadir este objeto")
		return False
	
	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("PQRS", "")
	
	def _getFilerTable(self, value):
		fil = Q()
		if self.user.es_evaluador and not self.user.es_admin:
			fil = Q(usuario = self.user)
		if value != "":
			fil = fil & (Q(asunto__icontains = value) | Q(usuario__last_name__icontains = value) | Q(usuario__first_name__icontains = value) | Q(cuerpo__icontains = value))
		return fil



class EstadisticasView(Main, View):
	template = 'funciones/graficas.html'

	def get_data(self, request, kwargs):
		
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Estadísticas", "")
		
		from datetime import datetime
		from dateutil.relativedelta import relativedelta
		
		tipo = request.GET.get("tipo", None)
		fecha_desde = request.GET.get("fecha_desde", None)
		fecha_hasta = request.GET.get("fecha_hasta", None)
		
		form_graphics = FormGraphic(initial = {"fecha_desde" : fecha_desde, "fecha_hasta" : fecha_hasta, "tipo" : tipo})
		
		data_grafica = {}
		
		categorias = []
		series = {}
		
		# Tipo: B -> Barras, P -> Pie
		tipo_grafica = "B"
		if tipo == FormGraphic.G_CANTIDAD:
			series = {"anteproyectos" : [], "informes" : [], "tgrados" : []}
			if fecha_desde != None:
				cur_date = start = datetime.strptime(fecha_desde, '%Y-%m-%d').date()
				end = datetime.strptime(fecha_hasta, '%Y-%m-%d').date()
				end_date = cur_date
				while cur_date < end:
					if cur_date != end_date:
						categorias.append(str(end_date) + " - " + str(cur_date))
						nro_tgrado = TGrado.objects.filter(fecha_creacion__range = [end_date, cur_date]).count()
						nro = Anteproyecto.objects.filter(fecha_ingreso__range = [end_date, cur_date]).count()
						nro_info = Informe.objects.filter(fecha_ingreso__range = [end_date, cur_date]).count()
						series["anteproyectos"].append(nro)
						series["informes"].append(nro_info)
						series["tgrados"].append(nro_tgrado)
					end_date = cur_date
					cur_date += relativedelta(months = 1)
				
			data_grafica["titulo"] = "Cantidades"
			data_grafica["texto_y"] = "Cantidad"
			data_grafica["categorias"] = categorias
			
		elif tipo == FormGraphic.G_ANTEFINALIZADO:
			if fecha_desde != None and fecha_hasta != None:
				cur_date = start = datetime.strptime(fecha_desde, '%Y-%m-%d').date()
				end_date = datetime.strptime(fecha_hasta, '%Y-%m-%d').date()
				series = []
				conceptos_aprobados = AnteproyectoConcepto.objects.filter(Q(fecha_modificacion__range = [cur_date, end_date]) & Q(concepto = AnteproyectoConcepto.CONCEPTO_APROBADO)).count()
				conceptos_aplazado = AnteproyectoConcepto.objects.filter(Q(fecha_modificacion__range = [cur_date, end_date]) & Q(concepto = AnteproyectoConcepto.CONCEPTO_APLAZADO)).count()
				conceptos_evaluacion = AnteproyectoConcepto.objects.filter(Q(fecha_modificacion__range = [cur_date, end_date]) & Q(concepto = AnteproyectoConcepto.CONCEPTO_EVALUACION)).count()
				series.append({"tipo" : "Aprobados", "nro" : conceptos_aprobados})
				series.append({"tipo" : "Aplazados", "nro" : conceptos_aplazado})
				series.append({"tipo" : "En ejecución", "nro" : conceptos_evaluacion})
				data_grafica["titulo"] = "Conceptos de Anteproyectos finalizados y en ejecución [" + fecha_desde + " - " + fecha_hasta + "]"
				tipo_grafica = "P"
		
		elif tipo == FormGraphic.G_INFFINALIZADO:
			if fecha_desde != None and fecha_hasta != None:
				cur_date = start = datetime.strptime(fecha_desde, '%Y-%m-%d').date()
				end_date = datetime.strptime(fecha_hasta, '%Y-%m-%d').date()
				series = []
				conceptos_aprobados = InformeConcepto.objects.filter(Q(fecha_modificacion__range = [cur_date, end_date]) & Q(concepto = InformeConcepto.CONCEPTO_LISTO_SUSTENTAR)).count()
				conceptos_aplazado = InformeConcepto.objects.filter(Q(fecha_modificacion__range = [cur_date, end_date]) & Q(concepto = InformeConcepto.CONCEPTO_APLAZADO)).count()
				conceptos_evaluacion = InformeConcepto.objects.filter(Q(fecha_modificacion__range = [cur_date, end_date]) & Q(concepto = InformeConcepto.CONCEPTO_EVALUACION)).count()
				conceptos_reprobados = InformeConcepto.objects.filter(Q(fecha_modificacion__range = [cur_date, end_date]) & Q(concepto = InformeConcepto.CONCEPTO_REPROBADO)).count()
				series.append({"tipo" : "Aprobados", "nro" : conceptos_aprobados})
				series.append({"tipo" : "Aplazados", "nro" : conceptos_aplazado})
				series.append({"tipo" : "En ejecución", "nro" : conceptos_evaluacion})
				series.append({"tipo" : "Reprobados", "nro" : conceptos_reprobados})
				data_grafica["titulo"] = "Conceptos de Informes finalizados y en ejecución [" + fecha_desde + " - " + fecha_hasta + "]"
				tipo_grafica = "P"
			
		elif tipo == FormGraphic.G_TGRADOFINALIZADO:
			if fecha_desde != None and fecha_hasta != None:
				cur_date = start = datetime.strptime(fecha_desde, '%Y-%m-%d').date()
				end_date = datetime.strptime(fecha_hasta, '%Y-%m-%d').date()
				series = []
				
				
				conceptos_exito = TGrado.objects.filter(Q(fecha_modificiacion__range = [cur_date, end_date]) & Q(estado = TGrado.E_EXITO)).count()
				conceptos_cancelado = TGrado.objects.filter(Q(fecha_modificiacion__range = [cur_date, end_date]) & Q(estado = TGrado.E_CANCELADO)).count()
				conceptos_aplazado = TGrado.objects.filter(Q(fecha_modificiacion__range = [cur_date, end_date]) & Q(estado = TGrado.E_APLAZADO_TGRADO)).count()
				en_ejecucion = TGrado.objects.filter(Q(fecha_modificiacion__range = [cur_date, end_date])).count()
				en_ejecucion = en_ejecucion - conceptos_exito - conceptos_cancelado - conceptos_aplazado
				series.append({"tipo" : "finalizados con éxito", "nro" : conceptos_exito})
				series.append({"tipo" : "Cancelados", "nro" : conceptos_cancelado})
				series.append({"tipo" : "Aplazados", "nro" : conceptos_aplazado})
				series.append({"tipo" : "En ejecución", "nro" : en_ejecucion})
				data_grafica["titulo"] = "Tgrados finalizados y en ejecución [" + fecha_desde + " - " + fecha_hasta + "]"
				tipo_grafica = "P"
		
		data_return = {}
		
		data_return["form_graphics"] = form_graphics
		if series != {}:
			data_grafica["series"] = series
			data_return["data_grafica"] = data_grafica
			data_return["tipo_grafica"] = tipo_grafica
		return data_return


class ProgramacionSustentaciones(Main, View):
	template = 'funciones/programacion_sustentaciones.html'

	def get_data(self, request, kwargs):
		
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Sustentaciones", "")
		
		from datetime import datetime, timedelta
		from dateutil.relativedelta import relativedelta
		
		tgrados = request.GET.getlist("tgrados", None)
		hora_desde = request.GET.get("hora_desde", None)
		hora_hasta = request.GET.get("hora_hasta", None)
		tiempo = request.GET.get("tiempo", None)
		paralelos = request.GET.get("paralelos", None)
		fecha = request.GET.get("fecha", None)
		lugar = request.GET.get("lugar", "Sin lugar")
		
		if tiempo == None:
			tiempo = 40
		if paralelos == None:
			paralelos = 1
		
		form_sustentacion = FormSustentaciones(initial = {"lugar" : lugar, "fecha" : fecha,"hora_desde" : hora_desde, "hora_hasta" : hora_hasta, "tgrados" : tgrados, "tiempo" : tiempo, "paralelos" : paralelos})
		
		data_return = {}
		data_return["form_sustentacion"] = form_sustentacion
		
		if tgrados != None and hora_desde != None and hora_hasta != None:
			self._add_js("programacion.js");
			hoy = datetime.now()
			horaD = datetime(hoy.year, hoy.month, hoy.day, hour=int(hora_desde[0:2]), minute=int(hora_desde[3:6]))
			horaH = datetime(hoy.year, hoy.month, hoy.day, hour=int(hora_hasta[0:2]), minute=int(hora_hasta[3:6]))
			delta_t = timedelta(minutes = int(tiempo))
			hora_act = horaD
			
			#tgrados = TGrado.objects.all()
			all_tgrados = []
			for tgrado in tgrados:
				try:
					all_tgrados.append(TGrado.objects.get(Q(pk = tgrado)))
				except Exception as e:
					print(e)
					pass
			
			sustentaciones = []
			
			while hora_act < horaH:
				last_personas = []
				for i in range(0, int(paralelos)):
					for tgrado in all_tgrados:
						director = tgrado.director						
						informe = Informe.objects.filter(tgrado = tgrado).last()						
						conceptos = InformeConcepto.objects.filter(informe = informe)
						error = False
						evaluadores = []
						for con in conceptos:
							evaluadores.append(con.evaluador)
							if con.evaluador in last_personas:
								error = True
								break
						if not director in last_personas and not error:
							last_personas.append(tgrado.director)
							last_personas.extend(evaluadores)
							all_tgrados.remove(tgrado)
							sustentaciones.append({
								"tgrado" : tgrado,
								"hora" : str(fecha) + " " + str(hora_act.strftime("%X"))
							})
							break
				hora_act += delta_t
			if len(sustentaciones) > 0:
				data_return["sustentaciones"] = sustentaciones
				data_return["len_no"] = len(tgrados) - len(sustentaciones)
				data_return["lugar"] = lugar
		else:
			data_return["faltan_datos"] = True
		return data_return

	
	
	def _editData(self, request):
		"""
			Add a table register
		"""
		from datetime import datetime
		if self.user.es_admin:
			datos = request.POST.getlist("datos[]", None)
			if datos is not None:
				datos = json.loads(datos[0])
				for da in datos:
					tgrado = TGrado.objects.get(pk = int(da["tgrado_id"]))
					informe = Informe.objects.filter(tgrado = tgrado).last()
					fecha = datetime.strptime(da["value"], '%Y-%m-%d %H:%M:%S').timestamp()
					informe.fecha_sustentacion = da["value"]
					informe.lugar_sustentacion = da["lugar"]
					informe.save()
				return True
		else:
			self._add_error("usted no es administrador.")
		return False
	
		
	def _ajaxRequest(self, request):
		"""
			Proccess the ajax data
		"""
		action = request.POST.get("ajax_action", None)
		error = False
		
		data = None
		if not self.user.is_anonymous and self.user.es_admin:
			if action == "editData":
				data = self._editData(request)
		
		error = not(data)
		data = {
			"errores" : ", ".join(self.errors),
			"error": error
		}
		return JsonResponse(data)
	
	
	


