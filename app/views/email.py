from app.views.main import Main, MainTable
from django.views import View
from django.db.models import Q

from django.core.mail import send_mail

from app.models import *
from app.forms.email import *

class EmailBorradoresView(MainTable, View):
	"""
		Vista de los borradores
	"""
	page_title = "Email borradores"
	register_name = "borrador"
	form_action = "/email/borradores"
	model_object = BorradorCorreo
	table_columns =	dict(nombre = "Nombre", asunto = "Asunto", fecha_ingreso__date_format = "Fecha Ingreso", observaciones = "Observaciones")
	form_edit = editBorrador
	form_add = editBorrador
	delete_register = True

	edit_aparte = True

	aditional_buttons = "<a title='Enviar correo a...' class='btn btn-xs btn-info' href='/email/redactar?borrador=__PK'><i class='fa fa-send'></i></a>"

	def _preProccess(self, request):
		if self.user.es_admin:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_view_page = False

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Correos borradores", "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(nombre__icontains = value) | Q(asunto__icontains = value) | Q(fecha_ingreso__icontains=value) | Q(observaciones__icontains = value))
		return fil


class EmailTemplatesView(MainTable, View):
	"""
		Vista de los borradores
	"""
	page_title = "Email plantillas"
	register_name = "plantilla"
	form_action = "/email/plantillas"
	model_object = MailTemplates
	table_columns =	dict(titulo = "Título", fecha_ingreso__date_format = "Fecha Ingreso", estado = "Estado")
	form_edit = editTemplate
	form_add = editTemplate
	delete_register = True

	edit_aparte = True

	mostrar_control_activos = True

	def _preProccess(self, request):
		if self.user.es_admin:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_view_page = False

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Plantillas de correo", "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(titulo__icontains = value) | Q(mensaje__icontains = value) | Q(fecha_ingreso__icontains=value))
		return fil

class EmailRedactarView(Main, View):
	template = "form.html"
	page_title = "Enviar email"
	form_action = "/email/redactar"

	def get_data(self, request, kwargs):
		pk = request.GET.get("borrador", None)
		mensaje = request.GET.get("mensaje", None)
		correos = request.GET.get("correos", None)
		request_data = request.GET.get("request_data", None)

		borrador = None
		if pk != None:
			try:
				borrador = BorradorCorreo.objects.get(pk = pk)
			except Exception as ex:
				print(ex)
				pass
		mensaje = None
		if request_data != None:
			try:
				mensaje = Mensajes.objects.get(tipo = Mensajes.SOLICITUD_DATOS)
			except Exception as e:
				print(e)
				mensaje = None
				pass
		if correos != None:
			correos = correos.split(",")

		self._add_aditional_template("email/select_borrador.html")
		self._add_js("email.js")

		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Enviar correo", "")

		form_borrador = changeBorradorForm()
		if borrador != None:
			form_envio = sendMain(instance = borrador)
		elif mensaje != None:
			form_envio = sendMainMensaje(instance = mensaje)
		else:
			form_envio = sendMain()

		ayuda = "Cuando presiones el botón, puede que se demore un poco, según la cantidad de destinatarios a los que envías, además, este texto se envía como cuerpo de la última plantilla agregada activa de la plataforma.<br /><br />" + \
			"Se enviará un correo automático con los datos de los usuario seleccionados si en ellos escribes las palabras claves siguientes:<br /><ul>" +\
			"<li><b>{{username}}</b> se envía el nombre de usuario que permite el inicio de sesión</li>" +\
			"<li><b>{{nombre}}</b> se envía el nombre del usuario</li>" +\
			"<li><b>{{apellido}}</b> se envía el apellido del usuario</li>" +\
			"</ul>"
		data_return = dict()
		forms = [dict(href="first", completa="True", tab="Enviar mensaje", form = form_envio)]
		data_return["forms"] = forms
		data_return["form_action"] = self.form_action
		data_return["form_borrador"] = form_borrador
		data_return["ayuda"] = ayuda
		data_return["tipo_form"] = "sendData"
		data_return["target"] = ""
		data_return["correos"] = correos
		return data_return

	def _preProccess(self, request):
		if not self.user.es_admin:
			self.can_view_page = False

	def _sendData(self, request):
		if request.user.es_admin:
			replaces = {
				"username" : "username",
				"nombre" : "first_name",
				"apellido" : "last_name"
			}
			asunto = request.POST.get("asunto", "")
			if asunto == "":
				self._add_error("El asunto no puede estar vacío")
				return False
			mensaje = request.POST.get("cuerpo", "")
			if mensaje == "":
				mensaje = request.POST.get("mensaje", "")
			correos_post = request.POST.getlist("correos", None)
			correos = []
			individual = False

			for key in replaces:
				if "{{" + key + "}}" in mensaje:
					individual = True

			if individual:
				for new_e in correos_post:
					if new_e != "":
						user = User.objects.filter(Q(pk = int(new_e)))
						if len(user) > 0:
							mensaje_send = mensaje
							user = user[0]
							if user.email not in correos:
								for key in replaces:
									mensaje_send = mensaje_send.replace("{{" + str(key) + "}}", getattr(user, replaces[key]))

								last_template = MailTemplates.objects.filter(Q(estado = ESTADO_ACTIVO)).last()
								if last_template != None:
									if "{{cuerpo}}" in last_template.mensaje:
										mensaje_send = last_template.mensaje.replace("{{cuerpo}}", mensaje_send)
								send_mail(
									asunto,
									'',
									'infotgradouniquindio@gmail.com',
									[user.email],
									fail_silently=False,
									html_message = mensaje_send
								)
			else:
				for new_e in correos_post:
					if new_e != "":
						user = User.objects.filter(Q(pk = int(new_e)))
						if len(user) > 0:
							user = user[0]
							if user.email not in correos:
								correos.append(user.email)

				last_template = MailTemplates.objects.filter(Q(estado = ESTADO_ACTIVO)).last()
				if last_template != None:
					if "{{cuerpo}}" in last_template.mensaje:
						mensaje = last_template.mensaje.replace("{{cuerpo}}", mensaje)
				send_mail(
					asunto,
					'',
					'infotgradouniquindio@gmail.com',
					correos,
					fail_silently=False,
					html_message = mensaje
				)
			return True
		return False


class EmailOtrosView(MainTable, View):
	"""
		Vista de los correos adicionales
	"""
	page_title = "Correos adicionales"
	register_name = "correo adicional"
	form_action = "/email/otros"
	model_object = CorreoAdicional
	table_columns =	dict(nombre = "Nombre", email = "Correo", descripcion = "Descripción")
	return_edit_columns = ["nombre", "email", "descripcion"]
	form_edit = editCorreoAdicional
	form_add = editCorreoAdicional
	delete_register = True

	def _preProccess(self, request):
		if self.user.es_admin:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_view_page = False

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Correos adicionales", "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(nombre__icontains = value) | Q(email__icontains = value) | Q(descripcion__icontains=value))
		return fil
