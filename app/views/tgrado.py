# Creado por Wilfer Daniel Ciro Maya - wilcirom@gmail.com
# Desde 2018 - 2019
# Creado para el comité de trabajos de grado del programa de Ingeniería electrónica de la universidad del Quindío, Armenia - Quindío, Colombia

# Django 2.1.3
# Bootstrap 4
# Mysql
# Para tips de optimización visite https://docs.djangoproject.com/en/2.1/topics/db/optimization/
# Para tips de seguridad visite https://librosweb.es/libro/django_1_0/capitulo_19.html

from app.views.main import Main, MainTable
from django.views import View
from django.db.models import Q

from datetime import datetime
from app.models import *
from app.forms.tgrado import *
from app.forms.ajax import *
from django.http import JsonResponse
from django.utils.formats import localize

class TGradoView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	page_title = "Trabajos de grado"
	register_name = "trabajo de grado"
	form_action = "/tgrado/"
	model_object = TGrado
	table_columns =	dict(titulo__format = "Título", estudiantes__format = "Estudiantes", director__format = "Director", modalidad__format = "Modalidad", fecha_creacion__date_format = "Fecha de Creación")
	return_edit_columns = ["titulo", "estado"]
	form_edit = editTGrado
	form_add = editTGrado
	delete_register = False

	can_view = True
	view_aparte = True
	edit_aparte = True

	def _getFormatRow(self, column, obj):
		if column == "estudiantes":
			return obj.returnEstudiantesFormato()
		elif column == "modalidad":
			return obj.get_modalidad()
		elif column == "titulo":
			return "<a href='/tgrado/view/"+str(obj.pk)+"'>" + str(obj.titulo) + "</a>"
		elif column == "director":
			return "<a href='/evaluadores/view/"+str(obj.pk)+"'>" + str(obj.director) + "</a>"
		return ""

	def order_rows(self, row):
		if row == "estudiantes":
			return "pk"
		return row

	def _editData(self, request):
		"""
			Edit the object data depending of the form
		"""
		object_id = request.POST.get("object_id", None)
		if object_id != None and self.can_edit:
			if self._checkPermission(object_id, request.user):
				new_tgrado = self.model_object.objects.get(pk = object_id)

				titulo = request.POST.get("titulo", new_tgrado.titulo)
				modalidad = request.POST.get("modalidad", new_tgrado.modalidad)

				new_tgrado.titulo = titulo
				new_tgrado.modalidad = modalidad

				current = TGradoEstudiantes.objects.filter(tgrado = new_tgrado)
				current.delete()
				estudiantes = request.POST.getlist("estudiantes", None)
				if estudiantes != None:
					for est in estudiantes:
						try:
							est_sel = User.objects.get(pk = est)
							new_e = TGradoEstudiantes(estudiante = est_sel, tgrado = new_tgrado)
							new_e.save()
						except:
							pass

				director = request.POST.get("director", None)
				try:
					user_dir = User.objects.get(pk = director)
					new_tgrado.director = user_dir
					new_tgrado.save()
				except:
					pass

				codirector = request.POST.get("codirector", None)
				try:
					user_dir = User.objects.get(pk = codirector)
					new_tgrado.codirector = user_dir
					new_tgrado.save()
				except:
					pass

				current = TGradoAsesores.objects.filter(tgrado = new_tgrado)
				current.delete()
				asesores = request.POST.getlist("asesores", None)
				if asesores != None:
					for ases in asesores:
						try:
							ases_sel = User.objects.get(pk = ases)
							if ases_sel != new_tgrado.director:
								new_e = TGradoAsesores(asesor = ases_sel, tgrado = new_tgrado)
								new_e.save()
								print(new_e)
						except Exception as e:
							print(e)
							pass
				return True
		else:
			self._add_error("Usted no tiene permisos para editar este objeto")
		return False

	def _deleteData(self, request):
		"""
			Deletes a table register
		"""
		object_id = request.POST.get("object_id", None)
		if object_id != None and self.can_delete:
			#if self._checkPermission(object_id, user):
			tgrado = TGrado.objects.get(pk = object_id)
			for est_tgrado in TGradoEstudiantes.objects.filter(tgrado = tgrado):
				est_tgrado.delete()
			for ases in TGradoAsesores.objects.filter(tgrado = tgrado):
				ases.delete()
			tgrado.delete()
			return True
		return False


	def _addData(self, request):
		"""
			Add a table register
		"""
		if self.can_add:
			tipo = request.POST.get("tipo_add", None)

			try:
				titulo = request.POST.get("titulo", "")
				modalidad = request.POST.get("modalidad", MODALIDAD_APLICACION)

				new_tgrado = TGrado(titulo = titulo, modalidad = modalidad)
				new_tgrado.save()

				estudiantes = request.POST.getlist("estudiantes", None)
				if estudiantes != None:
					for est in estudiantes:
						print(est)
						try:
							est_sel = User.objects.get(pk = est)
							new_e = TGradoEstudiantes(estudiante = est_sel, tgrado = new_tgrado)
							new_e.save()
						except:
							pass

				try:
					if self.user.es_admin:
						director = request.POST.get("director", None)
						user_dir = User.objects.get(pk = director)
						new_tgrado.director = user_dir
					elif self.user.es_evaluador:
						new_tgrado.director = self.user
					new_tgrado.save()
				except:
					pass

				try:
					if self.user.es_admin:
						codirector = request.POST.get("codirector", None)
						user_dir = User.objects.get(pk = codirector)
						new_tgrado.codirector = user_dir
					elif self.user.es_evaluador:
						new_tgrado.codirector = self.user
					new_tgrado.save()
				except:
					pass

				asesores = request.POST.getlist("asesores", None)
				if asesores != None:
					for ases in asesores:
						try:
							ases_sel = User.objects.get(pk = ases)
							if ases_sel != new_tgrado.director:
								new_e = TGradoAsesores(asesor = ases_sel, tgrado = new_tgrado)
								new_e.save()
						except:
							pass

				return True
			except Exception as e:
				self._add_error(str(e))
		else:
			self._add_error("Usted no tiene permisos para añadir este objeto")
		return False


	def _preProccess(self, request):
		if not self.user.es_estudiante:
			self.can_edit = True
			self.can_add = True
			if self.user.es_admin:
				self.form_edit = editTGrado
				self.form_add = editTGrado
				self.can_delete = True
				self.delete_register = True
			else:
				self.form_edit = editTGradoDirector
				self.form_add = editTGradoDirector

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Trabajos de grado", "")

		editarGET = request.GET.get("EditRegister", None)
		addGET = request.GET.get("AddRegister", None)

		data_return = {}
		if not self.user.es_estudiante:
			self._add_js("select2.min.js")
			self._add_css("select2.min.css")
			form_usuario = None
			if self.user.es_admin:
				if editarGET != None or addGET != None:
					self._add_js("tabla_tgrado_admin.js")
					self._add_aditional_template("varios/nuevo_usuario.html")
				form_usuario = add_usuario()
			elif self.user.es_evaluador:
				if editarGET != None or addGET != None:
					self._add_js("tabla_tgrado_evaluador.js")
			self._add_aditional_template("varios/autocomplete_fields.html")

			initial_values_student = []
			initial_director = None
			initial_asesores = []
			editarGET = request.GET.get("EditRegister", None)
			if editarGET != None:
				object_id = request.GET.get("object_id", None)
				try:
					tgrado = TGrado.objects.get(pk = object_id)
					for cu in TGradoEstudiantes.objects.filter(tgrado = tgrado):
						initial_values_student.append({"full_name" : cu.estudiante.get_full_name(), "pk" : cu.estudiante.pk})
					for cu in TGradoAsesores.objects.filter(tgrado = tgrado):
						initial_asesores.append({"full_name" : cu.asesor.get_full_name(), "pk" : cu.asesor.pk})

					initial_director = {"full_name" : tgrado.director.get_full_name(), "pk" : tgrado.director.pk}
				except:
					pass
			data_return["form_usuario"] = form_usuario
			data_return["initial_values_student"] = initial_values_student
			data_return["initial_director"] = initial_director
			data_return["initial_asesores"] = initial_asesores
			data_return["prev_dir"] = request.META.get('HTTP_REFERER')

		return data_return


	def _getFilerTable(self, value):
		fil = self.user.getTGradosFilter()
		if value != "":
			fil_ad = Q()
			if value.lower() in "pasantía":
				fil_ad |= Q(modalidad = MODALIDAD_PASANTIA)
			if value.lower() in "aplicación":
				fil_ad |= Q(modalidad = MODALIDAD_APLICACION)
			if value.lower() in "investigación":
				fil_ad |= Q(modalidad = MODALIDAD_INVESTIGACION)
			fil = fil & (Q(titulo__icontains = value) | Q(modalidad__icontains = value) | Q(director__last_name__icontains=value) | Q(director__first_name__icontains = value) | Q(fecha_creacion__icontains = value) | fil_ad)
		return fil


	def can_edit_row(self, obj):
		anteproyecto = Anteproyecto.objects.filter(Q(tgrado = obj)).last()
		if anteproyecto != None:
			concepto = AnteproyectoConcepto.objects.filter(anteproyecto = anteproyecto).last()
			if concepto != None and concepto.concepto == AnteproyectoConcepto.CONCEPTO_APLAZADO:
				return True
		else:
			return True
		return False


class TGradoViewIndividual(Main, View):
	template = "tgrado/profile.html"
	page_title = "Perfil de Tgrado"

	def get_data(self, request, kwargs):
		pk = int(kwargs['pk'])
		try:
			tgrado = TGrado.objects.get(pk = pk)
		except Exception as ex:
			self.can_view_page = False
			return {}

		data_return = {}
		data_return["tgrado"] = tgrado
		data_return["meses_restantes"] = None
		self._add_css("tgrado.css")
		self._add_css("dropdown_extension.css")
		if self.user.es_admin:
			self._add_js("select2.min.js")
			self._add_css("select2.min.css")
			self._add_js("tgrado_profile.js")
			self._add_aditional_template("varios/nuevo_usuario.html")

		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Tgrados", "/tgrado")
		self._add_breadcrumb(tgrado.titulo[0:15] + "...", "")

		E_INICIO = "1"
		E_ANTEPROYECTO = "2"
		E_EVALUADOR_ANTEPROYECTO = "3"
		E_APROBADO_ANTEPROYECTO = "4"
		E_APLAZADO_ANTEPROYECTO = "5"
		E_INFORME = "6"
		E_EVALUADORES_INFORME = "7"
		E_LISTO_SUSTENTAR_INFORME = "8"
		E_APLAZADO_INFORME = "9"
		E_REPROBADO_INFORME = "10"
		E_NOTAS = "11"
		E_EXITO = "12"
		E_CANCELADO = "13"
		E_APROBADO_TGRADO = "14"
		E_REPROBADO_TGRADO = "15"
		E_APLAZADO_TGRADO = "16"

		estado_tgrado = E_INICIO

		# Datos públicos
		archivos_anteproyecto = []
		informe = None
		todos_conceptos_informe = []

		correos_tgrado = [tgrado.director.email]

		data_return["asesores"] = tgrado.returnAsesores()
		estudiantes_data = []

		for est_tgrado in TGradoEstudiantes.objects.filter(tgrado = tgrado):
			estudiante = est_tgrado.estudiante
			estudiantes_data.append({
				"nombre" : estudiante.get_full_name(),
				"pk" : estudiante.pk
			})
			try:
				est_aca = EstadoAcademico.objects.get(estudiante = estudiante)
				archivos_anteproyecto.append({"nombre" : "Estado académico [" + str(estudiante) + "]", "archivo" : est_aca.estado_academico})
			except Exception as ex:
				pass

		data_return["estudiantes_data"] = estudiantes_data

		todos_anteproyectos = Anteproyecto.objects.filter(tgrado = tgrado)

		if todos_anteproyectos.count() > 1:
			data_return["anteriores_anteproyectos"] = True

		anteproyecto = todos_anteproyectos.last()
		concepto_anteproyecto = None
		if anteproyecto != None:
			estado_tgrado = E_ANTEPROYECTO
			anteproyecto_return = {
				"fecha_ingreso" : anteproyecto.fecha_ingreso,
				"fecha_edicion" : anteproyecto.fecha_edicion,
				"meses_desarrollo" : anteproyecto.meses_desarrollo,
				"meses_desarrollo_prorroga" : anteproyecto.meses_desarrollo_prorroga,
				"observaciones_anteproyecto" : anteproyecto.observaciones_anteproyecto,
				"pk" : anteproyecto.pk
			}

			data_return["anteproyecto"] = anteproyecto_return
			concepto_anteproyecto = AnteproyectoConcepto.objects.filter(anteproyecto = anteproyecto).last()
			aprobado_anteproyecto = False
			if concepto_anteproyecto != None:
				estado_tgrado = E_EVALUADOR_ANTEPROYECTO
				if concepto_anteproyecto.concepto == AnteproyectoConcepto.CONCEPTO_EVALUACION:
					data_return["concepto_evaluacion_anteproyecto"] = True
				elif concepto_anteproyecto.concepto == AnteproyectoConcepto.CONCEPTO_APROBADO:
					estado_tgrado = E_APROBADO_ANTEPROYECTO
					data_return["concepto_aprobado_anteproyecto"] = True
					data_return["meses_restantes"] = (anteproyecto.meses_desarrollo + anteproyecto.meses_desarrollo_prorroga) - (datetime.now().month - concepto_anteproyecto.fecha_modificacion.month)
					if self.user.es_admin:
						data_return["form_prorroga"] = formProrroga()

				elif concepto_anteproyecto.concepto == AnteproyectoConcepto.CONCEPTO_APLAZADO:
					estado_tgrado = E_APLAZADO_ANTEPROYECTO
					data_return["concepto_aplazado_anteproyecto"] = True
					data_return["meses_anteproyecto_aplazado"] = concepto_anteproyecto.plazo_aplazado - (datetime.now().month - concepto_anteproyecto.fecha_modificacion.month)
					if self.user.es_admin:
						data_return["form_prorroga_aplazado_anteproyecto"] = True

				concepto_anteproyecto_return = {
					"evaluador" : concepto_anteproyecto.evaluador,
					"evaluador_id" : concepto_anteproyecto.evaluador.pk,
					"concepto" : concepto_anteproyecto.get_concepto(),
					"fecha_asignacion" : concepto_anteproyecto.fecha_asignacion,
					"fecha_modificacion" : concepto_anteproyecto.fecha_modificacion,
					"archivo" : concepto_anteproyecto.archivo,
					"observaciones" : concepto_anteproyecto.observaciones,
					"pk" : concepto_anteproyecto.pk
				}
				data_return["concepto_anteproyecto"] = concepto_anteproyecto_return

			if estado_tgrado == E_APROBADO_ANTEPROYECTO:
				data_return["show_informe"] = True
				todos_informes = Informe.objects.filter(tgrado = tgrado)

				if todos_informes.count() > 1:
					data_return["anteriores_informe"] = True

				informe = todos_informes.last()
				if informe != None:
					informe_return = {
						"fecha_ingreso" : informe.fecha_ingreso,
						"fecha_edicion" : informe.fecha_edicion,
						"observaciones_informe" : informe.observaciones,
						"fecha_sustentacion" : informe.fecha_sustentacion,
						"lugar_sustentacion" : informe.lugar_sustentacion,
						"pk" : informe.pk
					}
					data_return["informe"] = informe_return
					data_return["meses_restantes"] = None
					estado_tgrado = E_INFORME
					conceptos_informe = InformeConcepto.objects.filter(informe = informe).order_by("-fecha_modificacion")
					if conceptos_informe.exists():
						estado_tgrado = E_EVALUADORES_INFORME

						todos_conceptos_informe = []
						conceptos_ids = []
						notas_estudiantes = []
						cantidad_notas = 0
						for con in conceptos_informe:
							can_view_nota = False
							todos_conceptos_informe.append({
								"evaluador" : con.evaluador,
								"evaluador_id" : con.evaluador.pk,
								"concepto" : con.get_concepto(),
								"observaciones" : con.observaciones,
								"fecha_modificacion" : con.fecha_modificacion,
								"archivo" : con.archivo,
								"pk" : con.pk,
								"concepto_id" : con.concepto
							})
							conceptos_ids.append(con.concepto)

							if self.user.es_admin or (self.user.es_evaluador and self.user == con.evaluador) or self.user.es_estudiante or self.user == tgrado.director:
								can_view_nota = True

							if con.concepto == InformeConcepto.CONCEPTO_LISTO_SUSTENTAR:
								if self.user.es_admin:
									data_return["no_fecha_sustentacion"] = True
								nota_informe = 0
								notas_interna = []
								for no in NotaInforme.objects.filter(Q(concepto_informe = con)):
									nota_informe += (no.valor * no.criterio_evaluacion.peso) / 100

								agregada = False
								for est_tgrado in TGradoEstudiantes.objects.filter(Q(tgrado = tgrado)):
									correos_tgrado.append(est_tgrado.estudiante.email)
									nota_estudiante = 0
									for no in NotaSustentacion.objects.filter(Q(concepto_informe = con) & Q(estudiante = est_tgrado.estudiante)):
										nota_estudiante += (no.valor * no.criterio_evaluacion.peso) / 100
									notas_interna.append({
										"estudiante" : est_tgrado.estudiante.get_full_name(),
										"nota" : round(nota_estudiante + nota_informe, 2)
									})
									if nota_estudiante > 0:
										agregada = True
								if agregada:
									cantidad_notas += 1
								notas_estudiantes.append({"can_view_nota": can_view_nota, "pk" : con.pk, "evaluador": con.evaluador.get_full_name(), "notas" : notas_interna})


						concepto_informe = {}
						if InformeConcepto.CONCEPTO_EVALUACION in conceptos_ids:
							data_return["en_evaluacion_informe"] = True
							concepto_informe["concepto"] = InformeConcepto.CONCEPTO_EVALUACION
							concepto_informe["cantidad"] = conceptos_ids.count(InformeConcepto.CONCEPTO_EVALUACION)
						elif InformeConcepto.CONCEPTO_REPROBADO in conceptos_ids:
							estado_tgrado = E_REPROBADO_INFORME
							concepto_informe["concepto"] = InformeConcepto.CONCEPTO_REPROBADO
							concepto_informe["cantidad"] = conceptos_ids.count(InformeConcepto.CONCEPTO_REPROBADO)
						elif InformeConcepto.CONCEPTO_APLAZADO in conceptos_ids:
							estado_tgrado = E_APLAZADO_INFORME
							concepto_informe["concepto"] = InformeConcepto.CONCEPTO_APLAZADO
							concepto_informe["cantidad"] = conceptos_ids.count(InformeConcepto.CONCEPTO_APLAZADO)
							data_return["aplazado_informe"] = True
							estado_tgrado = E_APLAZADO_INFORME
							data_return["last_concepto_informe"] = conceptos_informe = InformeConcepto.objects.filter(Q(informe = informe) & Q(concepto = InformeConcepto.CONCEPTO_APLAZADO)).order_by("-fecha_modificacion")
							data_return["last_concepto_informe"] = data_return["last_concepto_informe"][0]
							data_return["meses_informe_aplazado"] = data_return["last_concepto_informe"].plazo_aplazado - (datetime.now().month - data_return["last_concepto_informe"].fecha_modificacion.month)
							if self.user.es_admin:
								data_return["form_prorroga_aplazado_informe"] = True

						elif InformeConcepto.CONCEPTO_LISTO_SUSTENTAR in conceptos_ids:
							estado_tgrado = E_LISTO_SUSTENTAR_INFORME
							concepto_informe["concepto"] = InformeConcepto.CONCEPTO_LISTO_SUSTENTAR
							concepto_informe["cantidad"] = conceptos_ids.count(InformeConcepto.CONCEPTO_LISTO_SUSTENTAR)

							data_return["notas_estudiantes"] = notas_estudiantes
							data_return["cantidad_notas"] = cantidad_notas

						if concepto_informe != {}:
							for data in InformeConcepto.CONCEPTOS_CHOICES:
								if data[0] == concepto_informe["concepto"]:
									concepto_informe["concepto_texto"] = data[1].replace("para", "")
									break
							data_return["concepto_informe"] = concepto_informe

						if todos_conceptos_informe != []:
							data_return["todos_conceptos_informe"] = todos_conceptos_informe

						if cantidad_notas > 0:
							estado_tgrado = E_NOTAS

						if tgrado.estado == TGrado.E_EXITO:
							estado_tgrado = E_APROBADO_TGRADO
							data_return["aprobado_tgrado"] = True
						elif tgrado.estado == TGrado.E_CANCELADO:
							estado_tgrado = E_REPROBADO_TGRADO
							data_return["reprobado_tgrado"] = True
						elif tgrado.estado == TGrado.E_APLAZADO_TGRADO:
							data_return["aplazado_informe"] = True
							estado_tgrado = E_APLAZADO_TGRADO
		nuevo_estado_tgrado = tgrado.estado
		#### Permisos de usuarios
		if self.user.es_estudiante:
			register = TGradoEstudiantes.objects.filter(Q(tgrado = tgrado) & Q(estudiante = self.user))
			if register.count() == 0:
				self.can_view_page = False
				return {}
		else:
			es_evaluador_anteproyecto = False
			es_evaluador_informe = False

			if not self.user.es_admin:
				if not self.user == tgrado.director:
					if concepto_anteproyecto != None and concepto_anteproyecto.evaluador == self.user and informe == None:
						es_evaluador_anteproyecto = True
					elif todos_conceptos_informe != []:
						for con in todos_conceptos_informe:
							if con["evaluador_id"] == self.user.pk:
								es_evaluador_informe = True
								break
						if not es_evaluador_informe:
							self.can_view_page = False
							return {}
					else:
						self.can_view_page = False
						return {}


			if estado_tgrado == E_INICIO:
				if not es_evaluador_informe and not es_evaluador_anteproyecto:
					data_return["add_anteproyecto"] = True
					data_return["edit_tgrado"] = True

			elif estado_tgrado == E_ANTEPROYECTO:
				if not es_evaluador_informe and not es_evaluador_anteproyecto:
					data_return["edit_anteproyecto"] = True
				if self.user.es_admin:
					data_return["edit_evaluador_anteproyecto"] = True
				nuevo_estado_tgrado = TGrado.E_ANTEPROYECTO

			elif estado_tgrado == E_EVALUADOR_ANTEPROYECTO:
				if self.user.es_admin:
					data_return["edit_evaluador_anteproyecto"] = True
				if es_evaluador_anteproyecto or self.user.es_admin:
					data_return["edit_concepto_anteproyecto"] = True
				nuevo_estado_tgrado = TGrado.E_EJECUCION_ANTEPROYECTO

			elif estado_tgrado == E_APLAZADO_ANTEPROYECTO:
				if not es_evaluador_informe and not es_evaluador_anteproyecto:
					data_return["add_anteproyecto"] = True
					data_return["edit_tgrado"] = True
				if self.user.es_admin:
					data_return["edit_concepto_anteproyecto"] = True
				nuevo_estado_tgrado = TGrado.E_EJECUCION_ANTEPROYECTO

			elif estado_tgrado == E_APROBADO_ANTEPROYECTO:
				if not es_evaluador_informe and not es_evaluador_anteproyecto:
					data_return["add_informe"] = True
				if self.user.es_admin:
					data_return["edit_concepto_anteproyecto"] = True
				nuevo_estado_tgrado = TGrado.E_EJECUCION_ANTEPROYECTO

			elif estado_tgrado == E_INFORME:
				if not es_evaluador_informe and not es_evaluador_anteproyecto:
					data_return["edit_informe"] = True
				if self.user.es_admin:
					data_return["edit_evaluadores_informe"] = True
				nuevo_estado_tgrado = TGrado.E_INFORME

			elif estado_tgrado == E_EVALUADORES_INFORME:
				if self.user.es_admin:
					data_return["edit_evaluadores_informe"] = True
					data_return["edit_conceptos_informe"] = True
				elif es_evaluador_informe or self.user.es_admin:
					data_return["edit_conceptos_evaluador"] = True
				nuevo_estado_tgrado = TGrado.E_EJECUCION_INFORME

			elif estado_tgrado == E_LISTO_SUSTENTAR_INFORME:
				if self.user.es_evaluador and es_evaluador_informe:
					data_return["edit_conceptos_evaluador"] = True
				if self.user.es_admin:
					data_return["edit_conceptos_informe"] = True
					data_return["edit_evaluadores_informe"] = True
				nuevo_estado_tgrado = TGrado.E_EJECUCION_INFORME

			elif estado_tgrado == E_APLAZADO_INFORME:
				if self.user.es_evaluador and es_evaluador_informe:
					data_return["edit_conceptos_evaluador"] = True
				if not es_evaluador_informe and not es_evaluador_anteproyecto:
					data_return["add_informe"] = True
				if self.user.es_admin:
					data_return["edit_evaluadores_informe"] = True
					data_return["edit_conceptos_informe"] = True

			elif estado_tgrado == E_APLAZADO_TGRADO:
				if not es_evaluador_informe and not es_evaluador_anteproyecto:
					data_return["add_informe"] = True
				if self.user.es_admin:
					data_return["edit_evaluadores_informe"] = True
					data_return["finalizar_tgrado"] = True

			elif estado_tgrado == E_REPROBADO_INFORME:
				data_return["reprobado_tgrado"] = True
				if self.user.es_evaluador and es_evaluador_informe:
					data_return["edit_conceptos_evaluador"] = True
				if self.user.es_admin:
					data_return["finalizar_tgrado"] = True
					data_return["edit_conceptos_informe"] = True
				nuevo_estado_tgrado = TGrado.E_PARA_FIN

			elif estado_tgrado == E_NOTAS:
				if self.user.es_admin:
					data_return["finalizar_tgrado"] = True
					data_return["fecha_sustentacion"] = False
				nuevo_estado_tgrado = TGrado.E_PARA_FIN

			elif estado_tgrado == E_APROBADO_TGRADO:
				if self.user.es_admin:
					data_return["finalizar_tgrado"] = True

			elif estado_tgrado == E_REPROBADO_TGRADO:
				if self.user.es_admin:
					data_return["finalizar_tgrado"] = True

		if tgrado.estado != nuevo_estado_tgrado:
			tgrado.estado = nuevo_estado_tgrado
			tgrado.save()

		if "add_anteproyecto" in data_return:
			if tgrado.modalidad == MODALIDAD_APLICACION:
				data_return["form_add_anteproyecto"] = editAnteproyectoAplicacion()
			elif tgrado.modalidad == MODALIDAD_PASANTIA:
				data_return["form_add_anteproyecto"] = editAnteproyectoPasantia()
			elif tgrado.modalidad == MODALIDAD_INVESTIGACION:
				data_return["form_add_anteproyecto"] = editAnteproyectoInvestigacion()
			else:
				data_return["form_add_anteproyecto"] = editAnteproyectoOther()

		if "edit_anteproyecto" in data_return:
			if tgrado.modalidad == MODALIDAD_APLICACION:
				data_return["form_edit_anteproyecto"] = editAnteproyectoAplicacion(instance = anteproyecto)
			elif tgrado.modalidad == MODALIDAD_PASANTIA:
				data_return["form_edit_anteproyecto"] = editAnteproyectoPasantia(instance = anteproyecto)
			elif tgrado.modalidad == MODALIDAD_INVESTIGACION:
				data_return["form_edit_anteproyecto"] = editAnteproyectoInvestigacion(instance = anteproyecto)
			else:
				data_return["form_add_anteproyecto"] = editAnteproyectoOther(instance = anteproyecto)

		if "edit_evaluador_anteproyecto" in data_return:
			data_return["form_edit_evaluador_anteproyecto"] = addEvaluadorAnteproyecto()
			data_return["form_usuario"] = add_usuario()

		if "edit_concepto_anteproyecto" in data_return:
			data_return["form_edit_concepto_anteproyecto"] = editConceptoAnteproyecto(instance = concepto_anteproyecto)

		if "add_informe" in data_return:
			data_return["form_add_informe"] = editInforme()

		if "edit_informe" in data_return:
			data_return["form_edit_informe"] = editInforme(instance = informe)

		if "edit_evaluadores_informe" in data_return:
			data_return["form_edit_evaluador_informe"] = editEvaluadorInforme(obj = tgrado)
			data_return["form_usuario"] = add_usuario()

		if "edit_conceptos_informe" in data_return:
			data_return["form_edit_concepto_informe"] = editConceptoInformeAdmin()

		if "edit_conceptos_evaluador" in data_return:
			concepto_evaluador = InformeConcepto.objects.get(Q(evaluador = self.user) & Q(informe = informe))
			#if concepto_evaluador.concepto == InformeConcepto.CONCEPTO_EVALUACION:
			data_return["form_edit_concepto_informe"] = editConceptoInforme(instance = concepto_evaluador)
			data_return["concepto_id_editar"] = concepto_evaluador.pk
			#else:
			#	data_return["edit_conceptos_evaluador"] = False

		if "finalizar_tgrado" in data_return:
			data_return["form_finalizar_tgrado"] = FinalizarTGrado(instance = tgrado)

		if "no_fecha_sustentacion" in data_return and not "aprobado_tgrado" in data_return and not "reprobado_tgrado" in data_return and not "aplazado_informe" in data_return:
			data_return["form_fecha_sustentacion"] = editFechaSustentacion(instance = informe)
		else:
			data_return["no_fecha_sustentacion"] = False

		###### Obteniendo los Archivos
		if anteproyecto != None:
			archivos_anteproyecto.append({"nombre" : "Archivo de anteproyecto", "archivo" : anteproyecto.archivo_anteproyecto})
			if tgrado.modalidad == MODALIDAD_PASANTIA:
				if anteproyecto.convenio_marco != None:
					archivos_anteproyecto.append({"nombre" : "Carta de convenio marco", "archivo" : anteproyecto.convenio_marco})
				if anteproyecto.carta_presentacion != None:
					archivos_anteproyecto.append({"nombre" : "Carta de presentación", "archivo" : anteproyecto.carta_presentacion})

			if tgrado.modalidad == MODALIDAD_INVESTIGACION:
				if anteproyecto.carta_proyecto_vigente != None:
					archivos_anteproyecto.append({"nombre" : "Carta de vigencia del proyecto", "archivo" : anteproyecto.carta_proyecto_vigente})
				if anteproyecto.carta_coordinador != None:
					archivos_anteproyecto.append({"nombre" : "Carta del coordinador", "archivo" : anteproyecto.carta_coordinador})

		if archivos_anteproyecto == []:
			archivos_anteproyecto = None
		data_return["archivos_anteproyecto"] = archivos_anteproyecto

		archivos_informe = []
		if informe != None:
			archivos_informe.append({"nombre" : "Archivo de informe", "archivo" : informe.archivo_informe})
		if archivos_informe == []:
			archivos_informe = None
		data_return["archivos_informe"] = archivos_informe

		data_return["correos_tgrado"] = ",".join(correos_tgrado)

		return data_return

	def _addData(self, request):
		tipo_add = request.POST.get("tipoAdd", None)
		if tipo_add == "anteproyecto" and not self.user.es_estudiante:
			tgrado_id = request.POST.get("tgrado", None)
			tgrado = TGrado.objects.get(pk = tgrado_id)
			if self.user.can_operate_tgrado(tgrado):
				if tgrado.modalidad == MODALIDAD_APLICACION:
					form = editAnteproyectoAplicacion(data = request.POST, files = request.FILES)
				elif tgrado.modalidad == MODALIDAD_PASANTIA:
					form = editAnteproyectoPasantia(data = request.POST, files = request.FILES)
				elif tgrado.modalidad == MODALIDAD_INVESTIGACION:
					form = editAnteproyectoInvestigacion(data = request.POST, files = request.FILES)
				else:
					form = editAnteproyectoOther(data = request.POST, files = request.FILES)
				if form.is_valid():
					try:
						new_anteproyecto = form.save(commit = False)
						new_anteproyecto.tgrado = tgrado
						new_anteproyecto.save()
						form.save_m2m()
						return True
					except Exception as e:
						pass
			self._add_error(form.errors)

		elif tipo_add == "evaluador_anteproyecto" and self.user.es_admin:
			evaluador = request.POST.getlist("evaluador", None)
			anteproyecto_id = request.POST.get("anteproyecto", None)
			tgrado_id = request.POST.get("tgrado", None)
			try:
				tgrado = TGrado.objects.get(pk = tgrado_id)
				anteproyecto = Anteproyecto.objects.get(Q(pk = anteproyecto_id) & Q(tgrado = tgrado))
				for ev in evaluador:
					evaluador_choose = User.objects.get(Q(pk = ev) & Q(es_evaluador = True))
					if evaluador_choose != tgrado.director:
						# TODO: comprobar que el evaluador no sea asesor
						concepto = AnteproyectoConcepto.objects.filter(anteproyecto = anteproyecto).last()
						if concepto != None:
							concepto.evaluador = evaluador_choose
							concepto.save()
						else:
							concepto = AnteproyectoConcepto(evaluador = evaluador_choose, anteproyecto = anteproyecto)
							concepto.save()
						break
				return True
			except Exception as e:
				print(e)
				pass

		elif tipo_add == "informe" and not self.user.es_estudiante:
			tgrado_id = request.POST.get("tgrado", None)
			tgrado = TGrado.objects.get(pk = tgrado_id)
			if self.user.can_operate_tgrado(tgrado):
				form = editInforme(data = request.POST, files = request.FILES)
				if form.is_valid():
					try:
						new_informe = form.save(commit = False)
						new_informe.tgrado = tgrado
						new_informe.save()
						form.save_m2m()
						return True
					except Exception as e:
						pass
			self._add_error(form.errors)

		elif tipo_add == "evaluadores_informe" and self.user.es_admin:
			evaluadores = request.POST.getlist("evaluadores")
			informe_id = request.POST.get("informe", None)
			tgrado_id = request.POST.get("tgrado", None)
			errores = ""
			try:
				tgrado = TGrado.objects.get(pk = tgrado_id)
				informe = Informe.objects.get(Q(pk = informe_id) & Q(tgrado = tgrado))

				all_eval = []
				for ac_ev in InformeConcepto.objects.filter(Q(informe = informe)):
					all_eval.append(ac_ev.evaluador)

				for ev in evaluadores:
					evaluador_choose = User.objects.get(Q(pk = ev) & Q(es_evaluador = True))
					if evaluador_choose in all_eval:
						all_eval.remove(evaluador_choose)
					else:
						if evaluador_choose != tgrado.director:
							# TODO: comprobar que el evaluador no sea asesor
							concept = InformeConcepto(evaluador = evaluador_choose, informe = informe)
							concept.save()
				for ac_ev in all_eval:
					in_co = InformeConcepto.objects.get(Q(informe = informe) & Q(evaluador = ac_ev))
					if in_co.concepto == InformeConcepto.CONCEPTO_EVALUACION:
						in_co.delete()
					else:
						errores += "El evaluador: " + str(ac_ev) + " tiene concepto por ello no puede ser eliminado"
				return True
			except Exception as e:
				errores = str(e)
				print(errores)
		return False

	def _editData(self, request):
		tipo_edit = request.POST.get("tipoEdit", None)
		object_id = request.POST.get("object_id", None)

		if tipo_edit == "anteproyecto" and object_id != None and not self.user.es_estudiante:
			try:
				tgrado_id = request.POST.get("tgrado", None)
				tgrado = TGrado.objects.get(pk = tgrado_id)
				anteproyecto = Anteproyecto.objects.get(Q(pk=object_id) & Q(tgrado = tgrado))

				if self.user.can_operate_tgrado(tgrado):
					if tgrado.modalidad == MODALIDAD_APLICACION:
						form = editAnteproyectoAplicacion(data = request.POST, files = request.FILES, instance = anteproyecto)
					elif tgrado.modalidad == MODALIDAD_PASANTIA:
						form = editAnteproyectoPasantia(data = request.POST, files = request.FILES, instance = anteproyecto)
					elif tgrado.modalidad == MODALIDAD_INVESTIGACION:
						form = editAnteproyectoInvestigacion(data = request.POST, files = request.FILES, instance = anteproyecto)
					else:
						form = editAnteproyectoOther(data = request.POST, files = request.FILES, instance = anteproyecto)

					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
			except Exception as e:
				pass

		elif tipo_edit == "concepto_anteproyecto" and object_id != None and not self.user.es_estudiante:
			tgrado_id = request.POST.get("tgrado", None)
			anteproyecto_id = request.POST.get("anteproyecto", None)
			if object_id != None and tgrado_id != None and anteproyecto_id != None:
				try:
					tgrado = TGrado.objects.get(pk = tgrado_id)
					anteproyecto = Anteproyecto.objects.get(Q(pk=anteproyecto_id) & Q(tgrado = tgrado))
					concepto = AnteproyectoConcepto.objects.get(Q(pk=object_id) & Q(anteproyecto = anteproyecto))
					if concepto.evaluador == self.user or self.user.es_admin:
						form = editConceptoAnteproyecto(data = request.POST, files = request.FILES, instance = concepto)
						if form.is_valid():
							form.save()
							return True
						self._add_error(form.errors)
				except Exception as ex:
					print(ex)
					pass

		elif tipo_edit == "prorroga" and object_id != None and self.user.es_admin:
			tgrado_id = request.POST.get("tgrado", None)
			anteproyecto_id = object_id
			if tgrado_id != None:
				try:
					tgrado = TGrado.objects.get(pk = tgrado_id)
					anteproyecto = Anteproyecto.objects.get(Q(pk=anteproyecto_id) & Q(tgrado = tgrado))
					form = formProrroga(data = request.POST, files = request.FILES, instance = anteproyecto)
					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
				except Exception as ex:
					print(ex)
					pass

		elif tipo_edit == "prorroga_aplazado" and object_id != None and self.user.es_admin:
			tgrado_id = request.POST.get("tgrado", None)
			anteproyecto_id = request.POST.get("anteproyecto_id", None)
			print(tgrado_id, anteproyecto_id, object_id)
			if tgrado_id != None and self.user.es_admin:
				try:
					tgrado = TGrado.objects.get(pk = tgrado_id)
					anteproyecto = Anteproyecto.objects.get(Q(pk=anteproyecto_id) & Q(tgrado = tgrado))
					concepto = AnteproyectoConcepto.objects.get(Q(pk=object_id) & Q(anteproyecto = anteproyecto))
					concepto.plazo_aplazado = concepto.plazo_aplazado + 1
					concepto.save(skip_lastupdatetime = True)
					return True
				except Exception as ex:
					print(ex)
					pass

		elif tipo_edit == "prorroga_aplazado_informe" and object_id != None and self.user.es_admin:
			tgrado_id = request.POST.get("tgrado", None)
			informe_id = request.POST.get("informe_id", None)
			if tgrado_id != None and self.user.es_admin:
				try:
					tgrado = TGrado.objects.get(pk = tgrado_id)
					informe = Informe.objects.get(Q(pk=informe_id) & Q(tgrado = tgrado))
					concepto = InformeConcepto.objects.get(Q(pk=object_id) & Q(informe = informe))
					concepto.plazo_aplazado = concepto.plazo_aplazado + 1
					concepto.save(skip_lastupdatetime = True)
					return True
				except Exception as ex:
					print(ex)
					pass

		elif tipo_edit == "informe" or tipo_edit == "fecha_sustentacion" and object_id != None and not self.user.es_estudiante:
			try:
				tgrado_id = request.POST.get("tgrado", None)
				tgrado = TGrado.objects.get(pk = tgrado_id)
				informe = Informe.objects.get(Q(pk=object_id) & Q(tgrado = tgrado))
				if self.user.can_operate_tgrado(tgrado):
					if tipo_edit == "informe":
						form = editInforme(data = request.POST, files = request.FILES, instance = informe)
					elif tipo_edit == "fecha_sustentacion":
						form = editFechaSustentacion(data = request.POST, files = request.FILES, instance = informe)
					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
			except Exception as e:
				pass

		elif tipo_edit == "concepto_informe" and object_id != None and not self.user.es_estudiante:
			tgrado_id = request.POST.get("tgrado", None)
			informe_id = request.POST.get("informe", None)
			if object_id != None and tgrado_id != None and informe_id != None:
				try:
					tgrado = TGrado.objects.get(pk = tgrado_id)
					informe = Informe.objects.get(Q(pk=informe_id) & Q(tgrado = tgrado))
					concepto = InformeConcepto.objects.get(Q(pk=object_id) & Q(informe = informe))
					if concepto.evaluador == self.user or self.user.es_admin:
						if self.user.es_admin:
							form = editConceptoInformeAdmin(data = request.POST, files = request.FILES, instance = concepto)
						else:
							form = editConceptoInforme(data = request.POST, files = request.FILES, instance = concepto)
						if form.is_valid():
							form.save()
							tgrado.estado = TGrado.E_EJECUCION
							tgrado.save()
							return True
						self._add_error(form.errors)
				except Exception as ex:
					print(ex)
					pass
		elif tipo_edit == "finalizar_tgrado":
			try:
				if self.user.es_admin:
					tgrado = TGrado.objects.get(pk = object_id)
					form = FinalizarTGrado(data = request.POST, instance = tgrado)
					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
			except Exception as e:
				print(e)
		return False



class CalificacionView(Main, View):
	template = "tgrado/notas.html"
	page_title = "Notas del tgrado"

	def get_data(self, request, kwargs):
		tgrado_id = int(kwargs['tgrado'])
		informe_id = int(kwargs['informe'])
		concepto_id = int(kwargs['concepto'])
		try:
			tgrado = TGrado.objects.get(pk = tgrado_id)
			informe = Informe.objects.get(Q(tgrado = tgrado) & Q(pk = informe_id))
			concepto = InformeConcepto.objects.get(Q(informe = informe) & Q(pk = concepto_id))
		except Exception as ex:
			self.can_view_page = False
			return {}

		edit_notas = False
		data_return = dict()

		if not self.user.es_admin:
			if self.user.es_evaluador:
				if not concepto.evaluador == self.user and not tgrado.director == self.user:
					self.can_view_page = False
					return {}
				if concepto.evaluador == self.user:
					edit_notas = True

			elif self.user.es_estudiante:
				if not TGradoEstudiantes.objects.filter(Q(estudiante = self.user) & Q(tgrado = tgrado)).exists():
					self.can_view_page = False
					return {}
		else:
			edit_notas = True

		datos_tgrado = {
			"titulo_tgrado" : tgrado.titulo,
			"evaluador" : concepto.evaluador,
			"director" : tgrado.director,
			"pk" : tgrado.pk
		}

		if edit_notas:
			self._add_js("notas.js")
			self._add_css("notas.css")
		notas_informe = []
		nro = 1
		notas_informe_sql = NotaInforme.objects.filter(Q(concepto_informe = concepto))
		final_informe = 0
		if notas_informe_sql.exists():
			for nota in notas_informe_sql:
				datos = {
					"nro" : nro,
					"criterio" : nota.criterio_evaluacion.aspecto,
					"peso" : nota.criterio_evaluacion.peso,
					"nota" : nota.valor,
					"porcentaje" : round((nota.valor * nota.criterio_evaluacion.peso) / 100, 2),
					"fecha_edicion" : nota.fecha_modificacion,
					"pk" : nota.pk,
				}
				notas_informe.append(datos)
				final_informe += round((nota.valor * nota.criterio_evaluacion.peso) / 100, 2)
				nro += 1
		else:
			for crit_in in InformeCriterioEvaluacion.objects.filter(Q(estado = ESTADO_ACTIVO)):
				nota = NotaInforme(concepto_informe = concepto, criterio_evaluacion = crit_in)
				nota.save()
				datos = {
					"nro" : nro,
					"criterio" : crit_in.aspecto,
					"peso" : crit_in.peso,
					"nota" : "0.0",
					"porcentaje" : "0.0",
					"fecha_edicion" : nota.fecha_modificacion,
					"pk" : nota.pk,
				}
				notas_informe.append(datos)
				nro += 1

		data_return["final_informe"] = round(final_informe,2)

		final = final_informe

		notas_sustentacion = []
		for tgrado_est in TGradoEstudiantes.objects.filter(tgrado = tgrado):
			notas_interna = []
			notas_sustentacion_sql = NotaSustentacion.objects.filter(Q(concepto_informe = concepto) & Q(estudiante = tgrado_est.estudiante))
			if notas_sustentacion_sql.exists():
				for nota in notas_sustentacion_sql:
					datos = {
						"nro" : nro,
						"criterio" : nota.criterio_evaluacion.aspecto,
						"peso" : nota.criterio_evaluacion.peso,
						"nota" : nota.valor,
						"porcentaje" : round((nota.valor * nota.criterio_evaluacion.peso) / 100, 2),
						"fecha_edicion" : nota.fecha_modificacion,
						"pk" : nota.pk,
					}
					notas_interna.append(datos)
					nro += 1
			else:
				for crit_in in SustentacionCriterioEvaluacion.objects.filter(Q(estado = ESTADO_ACTIVO)):
					nota = NotaSustentacion(concepto_informe = concepto, criterio_evaluacion = crit_in, estudiante = tgrado_est.estudiante)
					nota.save()
					datos = {
						"nro" : nro,
						"criterio" : crit_in.aspecto,
						"peso" : crit_in.peso,
						"nota" : "0.0",
						"porcentaje" : "0.0",
						"fecha_edicion" : nota.fecha_modificacion,
						"pk" : nota.pk,
					}
					notas_interna.append(datos)
					nro += 1
			notas_sustentacion.append({
				"estudiante" : tgrado_est.estudiante.get_full_name(),
				"estudiante_pk" : tgrado_est.estudiante.pk,
				"notas" : notas_interna,
				"final" : round(float(final) + float(sum([float(comp["porcentaje"]) for comp in notas_interna])),2)
			})

		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb(tgrado.titulo[0:15] + "...", "/tgrado/view/"+str(tgrado_id))
		self._add_breadcrumb("Notas", "")

		data_return["edit_notas"] = edit_notas
		data_return["notas_informe"] = notas_informe
		data_return["datos_tgrado"] = datos_tgrado
		data_return["form_action"] = request.path
		data_return["notas_sustentacion"] = notas_sustentacion
		data_return["tgrado_id"] = tgrado.pk
		data_return["concepto_id"] = concepto_id
		return data_return

	def _editNota(self, request):
		valor_nota = request.POST.get("valor_nota", 0.0)
		nota_id = request.POST.get("nota_id", None)
		tipo = request.POST.get("tipo", None)
		estudiante_id = request.POST.get("estudiante", None)
		tgrado_id = request.POST.get("tgrado", None)
		try:
			if tipo == "informe":
				nota = NotaInforme.objects.get(pk = nota_id)
			elif tipo == "sustentacion":
				nota = NotaSustentacion.objects.get(Q(pk = nota_id) & Q(estudiante__pk = estudiante_id))

			if self.user.es_estudiante or (not self.user.es_admin and not nota.concepto_informe.evaluador == self.user):
				return {"concept" : "error", "error" : "No puede editar", "tipo" : tipo}

			if float(valor_nota) > 0:
				try:
					tgrado = TGrado.objects.get(pk = tgrado_id)
					tgrado.estado = TGrado.E_PARA_FIN
					tgrado.save()
				except Exception as ex:
					pass
			nota.valor = valor_nota
			nota.save()

			return {"concept" : "success", "fecha" : localize(nota.fecha_modificacion), "tipo" : tipo}
		except Exception as ex:
			print(ex)
			return {"concept" : "error", "error" : "Revise los datos que ingresó", "tipo" : tipo}


	def _ajaxRequest(self, request):
		action = request.POST.get("ajax_action", None)
		json_data = {"concept" : "error", "error" : "No hay datos"}
		if action == "editNota":
			json_data = self._editNota(request)

		return JsonResponse(json_data)

	def _editData(self, request):
		tipo_edit = request.POST.get("tipoEdit", None)
		if tipo_edit == "restaurar_notas":
			tgrado_id = request.POST.get("tgrado_id", None)
			tgrado = TGrado.objects.get(pk = tgrado_id)
			concepto_id = request.POST.get("concepto_id", None)
			concepto = InformeConcepto.objects.get(pk = concepto_id)
			if concepto.informe.tgrado == tgrado:
				for nota in NotaInforme.objects.filter(concepto_informe = concepto):
					nota.delete()
				for nota in NotaSustentacion.objects.filter(concepto_informe = concepto):
					nota.delete()
				return True

			print("Hit");

		return False
