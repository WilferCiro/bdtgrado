from app.views.main import Main, MainTable
from django.views import View
from django.db.models import Q

from app.models import *
from app.forms.configuracion import *
from django.utils.html import strip_tags



class CriteriosInformeView(MainTable, View):
	"""
		Vista de los correos adicionales
	"""
	page_title = "Conf. Evaluación informe"
	register_name = "criterio de evaluación de informe"
	form_action = "/configuracion/criterios/informe"
	model_object = InformeCriterioEvaluacion
	table_columns =	dict(aspecto = "Aspecto", peso = "Peso", observaciones = "Observaciones", estado__format = "Estado", modalidad__format = "Modalidad")
	return_edit_columns = ["estado", "observaciones", "modalidad"]
	form_edit = editCriterioInforme
	form_add = addCriterioInforme

	mostrar_control_activos = True
	delete_register = True

	def _getFormatRow(self, column, obj):
		if column == "estado":
			return obj.get_estado()
		elif column == "modalidad":
			return obj.get_modalidad()
		return ""

	def _preProccess(self, request):
		if self.user.es_admin:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_view_page = False

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Criterios de evaluación de informes", "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(aspecto__icontains = value) | Q(peso__icontains = value) | Q(observaciones__icontains=value) | Q(estado__icontains = value) | Q(modalidad__icontains = value))
		return fil

	def can_delete_row(self, obj):
		if NotaInforme.objects.filter(Q(criterio_evaluacion = obj)).exists():
			return False
		return True

class CriterioSustentacionView(MainTable, View):
	"""
		Vista de los correos adicionales
	"""
	page_title = "Conf. Evaluación sustentación"
	register_name = "criterio de evaluación de sustentación"
	form_action = "/configuracion/criterios/sustentacion"
	model_object = SustentacionCriterioEvaluacion
	table_columns =	dict(aspecto = "Aspecto", peso = "Peso", observaciones = "Observaciones", estado__format = "Estado")
	return_edit_columns = ["estado", "observaciones"]
	form_edit = editCriterioSustentacion
	form_add = addCriterioSustentacion

	mostrar_control_activos = True
	delete_register = True

	def _getFormatRow(self, column, obj):
		if column == "estado":
			return obj.get_estado()
		return ""

	def _preProccess(self, request):
		if self.user.es_admin:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_view_page = False

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Criterios de evaluación de sustentación", "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(aspecto__icontains = value) | Q(peso__icontains = value) | Q(observaciones__icontains=value) | Q(estado__icontains = value))
		return fil

	def can_delete_row(self, obj):
		if NotaSustentacion.objects.filter(Q(criterio_evaluacion = obj)).exists():
			return False
		return True


class EmailConfigView(MainTable, View):
	"""
		Vista de la configuración de correos a enviar por debajo
	"""
	page_title = "Conf. Mensajes"
	register_name = "Mensaje"
	form_action = "/configuracion/email"
	model_object = Mensajes
	table_columns =	dict(asunto = "Asunto", tipo__format = "Tipo")
	return_edit_columns = ["asunto", "mensaje", "tipo"]
	form_edit = editMensaje
	edit_aparte = True

	def _getFormatRow(self, column, obj):
		if column == "tipo":
			return obj.get_tipo()
		return ""

	def _preProccess(self, request):
		if self.user.es_admin:
			self.can_edit = True
			self.can_delete = False
			self.can_add = False
		else:
			self.can_view_page = False

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Configuración mensajes", "")

		for item in Mensajes.TIPO_CHOICES:
			if not Mensajes.objects.filter(tipo = item[0]).exists():
				men = Mensajes(tipo = item[0])
				men.save()

	def _get_title_form(self, obj):
		return obj.get_tipo()

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(tipo__icontains = value) | Q(asunto__icontains = value))
		return fil


class PaginasBlog(MainTable, View):
	page_title = "Páginas del blog"
	register_name = "página"
	form_action = "/configuracion/paginas/blog"
	model_object = PaginaBlog
	table_columns =	dict(titulo = "Título", imagen = "Imagen", pagina__format = "Página")
	return_edit_columns = ["titulo", "imagen", "pagina"]
	form_edit = editPaginaBlog
	form_add = editPaginaBlog
	delete_register = True

	edit_aparte = True
	mostrar_control_activos = False

	def _getFormatRow(self, column, obj):
		if column == "pagina":
			return strip_tags(obj.pagina[0:250]) + "..."
		return ""

	def _preProccess(self, request):
		if not self.user.es_estudiante:
			if self.user.es_evaluador and not self.user.es_admin:
				self.form_edit = editBancoDirector
				self.form_add = editBancoDirector
			self.can_edit = True
			self.can_delete = True
			self.can_add = True

	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Configuración de páginas de blog", "")

	def _getFilerTable(self, value):
		fil = Q()
		if value != "":
			fil = fil & (Q(titulo__icontains = value) | Q(nombre_enlace__icontains = value) | Q(pagina__icontains = value))
		return fil
