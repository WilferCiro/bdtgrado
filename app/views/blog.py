from app.models import *
from django.shortcuts import render
from django.views import View
from django.http import HttpResponse, HttpResponseRedirect
from app.views.main import MainBlog


from django.core.paginator import Paginator

class IndexBlog(MainBlog, View):
	page_title = "Blog"
	template = "blog/index.html"
	def get_data(self, request, kargs):
		data_return = {"is_main" : True}
		return data_return

class HelpView(MainBlog, View):
	page_title = "Help"
	template = "blog/help.html"
	def get_data(self, request, kargs):
		return {}


class PaginaBlogView(MainBlog, View):
	page_title = "Blog"
	template = "blog/pages.html"
	def get_data(self, request, kargs):
		pk = int(kargs['pk'])
		try:
			pagina = PaginaBlog.objects.get(pk = pk)
		except Exception as ex:
			print(ex)
			self.can_view_page = False
			return {"can_view" : False}

		self.page_title = pagina.titulo
		data_return = {}
		data_return["pagina"] = pagina.pagina
		data_return["titulo"] = pagina.titulo
		data_return["imagen"] = pagina.imagen
		data_return["fecha_edicion"] = pagina.fecha_edicion
		data_return["can_view"] = True
		return data_return

class CalendarioView(MainBlog, View):
	page_title = "Calendario"
	template = "blog/calendario.html"

	def get_data(self, request, kwargs):

		self._add_css("calendario/fullcalendar.css")
		self._add_css("calendario/fullcalendar.print.css")
		self._add_js("calendario/jquery-ui.custom.min.js")
		self._add_js("calendario/fullcalendar.js")
		self._add_js("calendario_handler_noAdmin.js")

		data_return = dict()
		return data_return


class SustentacionesView(MainBlog, View):
	page_title = "Programación de sustentaciones"
	template = "blog/sustentaciones.html"
	def get_data(self, request, kargs):
		tgrados = []
		lista = []
		import datetime
		x = datetime.datetime.now()
		hoy = x.strftime('%Y-%m-%d')
		for concepto in InformeConcepto.objects.filter(Q(concepto = InformeConcepto.CONCEPTO_LISTO_SUSTENTAR) & ~Q(informe__fecha_sustentacion = None) & Q(informe__fecha_sustentacion__gte = hoy)).order_by("informe__fecha_sustentacion"):
			tgrado = concepto.informe.tgrado
			if tgrado.estado == TGrado.E_EJECUCION_INFORME and not tgrado in tgrados:
				tgrados.append(tgrado)
				evaluadores = []
				estudiantes = []
				for est in TGradoEstudiantes.objects.filter(tgrado = tgrado):
					estudiantes.append(est.estudiante.get_full_name() + " <b>C.C.</b>" + est.estudiante.documento)
				for concepto_in in InformeConcepto.objects.filter(informe = concepto.informe):
					evaluadores.append(concepto_in.evaluador.get_full_name())
				lista.append({
					"fecha_sustentacion" : concepto.informe.fecha_sustentacion,
					"lugar_sustentacion" : concepto.informe.lugar_sustentacion,
					"estudiantes" : estudiantes,
					"director" : tgrado.director.get_full_name(),
					"modalidad" : tgrado.get_modalidad(),
					"evaluadores" : evaluadores,
					"titulo" : tgrado.titulo
				})
		data_return = {}
		data_return["lista"] = lista
		return data_return


class BancoView(MainBlog, View):
	page_title = "Banco de proyectos de grado"
	template = "blog/banco_proyectos.html"
	def get_data(self, request, kargs):

		banco_list = BancoProyectos.objects.all().order_by("estado", "fecha_modificacion")
		page = request.GET.get('page', 1)

		paginator = Paginator(banco_list, 15)
		try:
			banco = paginator.page(page)
		except PageNotAnInteger:
			banco = paginator.page(1)
		except EmptyPage:
			banco = paginator.page(paginator.num_pages)

		lista = []
		for ban in banco:
			es_activo = False
			if ban.estado == ESTADO_ACTIVO:
				es_activo = True
			if ban.proponente == None:
				proponente = ""
				proponente_correo = ""
			else:
				proponente = ban.proponente.get_full_name()
				proponente_correo = ban.proponente.email
			lista.append({
				"titulo" : ban.titulo,
				"descripcion" : ban.descripcion,
				"proponente" : proponente,
				"correo_proponente" : proponente_correo,
				"grupo" : ban.grupo,
				"modalidad" : ban.get_modalidad(),
				"es_activo" : es_activo,
				"fecha_modificacion" : ban.fecha_modificacion
			})

		return {"banco" : banco, "lista_banco" : lista}


class ProcedimientosView(MainBlog, View):
	page_title = "Procedimientos de trabajos de grado"
	template = "blog/procedimientos.html"
	def get_data(self, request, kargs):
		self._add_js("jquery.smartWizard.js")
		self._add_css("smart_wizard_theme_arrows.min.css")
		self._add_css("smart_wizard.min.css")
		self._add_js("procedimientos.js")
		config = ConfiguracionInformes.objects.all().last()
		procedimiento = config.procedimiento
		data_return = {
			"procedimiento" : procedimiento
		}
		return data_return
