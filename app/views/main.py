from django.shortcuts import render
from django.views import View
from app.models import *
from django.http import JsonResponse
from django.db.models import Q
from django.shortcuts import HttpResponseRedirect
import os.path
from django.conf import settings
from django.utils.formats import localize, date_format

from django.utils import translation
from django.utils.translation import ugettext_lazy as _

class Main(object):
	"""
		Main Class
	"""
	template = "structure.html"
	menu = []
	js_files = []
	css_files = []
	breadcrumb = []
	errors = []
	page_title = "bdtgrado"
	aditional_templates = []
	can_view_page = True
	nro_notificaciones = 0

	def __init__(self):
		"""
			Constructor of the main class
		"""
		self.__empty_all()

	def __get_menu(self, path):
		"""
			Set the menu of application
			@param user with the user data
		"""
		menu = []
		menu.append(dict(label = _("Inicio"), url = "/index", icon = "fa fa-dashboard", item_class = "", content = "", submenu = []))
		if not self.user.es_estudiante:
			menu.append(dict(label = _("Trabajos de grado"), url = "/tgrado", icon = "fa fa-list", item_class = "", content = "", submenu = []))
		else:
			menu.append(dict(label = _("Mis trabajos de grado"), url = "/tgrado", icon = "fa fa-list", item_class = "", content = "", submenu = []))
			menu.append(dict(label = _("Mis datos"), url = "/user/profile", icon = "fa fa-user", item_class = "", content = "", submenu = []))

		if not self.user.es_estudiante:
			menu.append(dict(label = _("Estudiantes"), url = "/estudiantes", icon = "fa fa-graduation-cap", item_class = "", content = "", submenu = []))

		if self.user.es_admin:
			menu.append(dict(label = _("Evaluadores"), url = "/evaluadores", icon = "fa fa-pencil", item_class = "", content = "", submenu = []))

		submenu = []
		submenu.append(dict(label = _("Calendario"), url = "/calendario", icon = "fa fa-calendar", item_class = "", content = ""))
		submenu.append(dict(label = _("Documentos"), url = "/documentos", icon = "fa fa-file-pdf-o", item_class = "", content = ""))
		submenu.append(dict(label = _("Estadísticas"), url = "/estadisticas", icon = "fa fa-pie-chart", item_class = "", content = ""))
		submenu.append(dict(label = _("Banco de proyectos"), url = "/proyectos/banco", icon = "fa fa-bank", item_class = "", content = ""))
		if self.user.es_admin:
			submenu.append(dict(label = _("Sustentaciones"), url = "/programacion/sustentaciones", icon = "fa fa-comment", item_class = "", content = ""))
		#submenu.append(dict(label = _("PQRS"), url = "/pqrs", icon = "fa fa-comment", item_class = "", content = ""))
		menu.append(dict(label = _("Más funciones"), url = "", icon = "fa fa-plus-circle", item_class = "", content = "", submenu = submenu))


		if self.user.es_admin:
			submenu = []
			submenu.append(dict(label = _("Redactar"), url = "/email/redactar", icon = "fa fa-pencil", item_class = "", content = ""))
			submenu.append(dict(label = _("Borradores"), url = "/email/borradores", icon = "fa fa-eraser", item_class = "", content = ""))
			submenu.append(dict(label = _("Plantillas"), url = "/email/plantillas", icon = "fa fa-object-ungroup", item_class = "", content = ""))
			#submenu.append(dict(label = _("Otros correos"), url = "/email/otros", icon = "fa fa-users", item_class = "", content = ""))
			menu.append(dict(label = _("Correos"), url = "", icon = "ti-email", item_class = "", content = "", submenu = submenu))
			submenu.append(dict(label = _("Configuración"), url = "/configuracion/email", icon = "fa fa-print", item_class = "", content = ""))

			submenu = []
			submenu.append(dict(label = _("Imprimir"), url = "/informes/print", icon = "fa fa-print", item_class = "", content = ""))
			menu.append(dict(label = _("Informes"), url = "", icon = "ti-file	", item_class = "", content = "", submenu = submenu))

			submenu = []
			submenu.append(dict(label = _("Informes"), url = "/informes/configurar", icon = "fa fa-gear", item_class = "", content = ""))
			submenu.append(dict(label = _("Crit. Informe"), url = "/configuracion/criterios/informe", icon = "fa fa-gear", item_class = "", content = ""))
			submenu.append(dict(label = _("Crit. Sustentación"), url = "/configuracion/criterios/sustentacion", icon = "fa fa-gear", item_class = "", content = ""))
			submenu.append(dict(label = _("Páginas del blog"), url = "/configuracion/paginas/blog", icon = "fa fa-flag", item_class = "", content = ""))
			menu.append(dict(label = _("Configuración"), url = "", icon = "fa fa-gear", item_class = "", content = "", submenu = submenu))

			menu.append(dict(label = _("Ver Blog"), url = "/", icon = "fa fa-gear", item_class = "", content = "", submenu = []))
			menu.append(dict(label = _("Ayuda"), url = "/help", icon = "fa fa-info-circle", item_class = "", content = "", submenu = []))

		item = next((item for item in menu if str(path)[0:-1] in item["url"].lower()), False)
		if item != False:
			item["item_class"] = "active"
		else:
			for men in menu:
				item = next((item for item in men["submenu"] if str(path)[0:-1] in item["url"].lower()), False)
				if item != False:
					item["item_class"] = "active"
					men["item_class"] = "active"
					break

		self.menu = menu

	def __empty_all(self):
		"""
			Prevent data repeat
		"""
		self.js_files = []
		self.css_files = []
		self.aditional_templates = []
		self.breadcrumb = []
		self.menu = []
		self.errors = []

	def _add_error(self, error):
		"""
			Adds a error for show
			@param error as str
		"""
		error_2 = error
		try:
			error_2 = error.as_text()
		except:
			pass
		self.errors.append(error_2)

	def _add_css(self, css_file):
		"""
			Adds a css file to document
			@param css_file as css file path inside static/
		"""
		if css_file not in self.css_files:
			self.css_files.append(css_file)

	def _add_js(self, js_file):
		"""
			Adds a js file to document
			@param js_file as js file path inside static/
		"""
		if js_file not in self.js_files:
			self.js_files.append(js_file)


	def _add_aditional_template(self, template):
		"""
			Adds a template for include to document
			@param template as html file path inside static/
		"""
		if template not in self.aditional_templates:
			self.aditional_templates.append(template)

	def _add_breadcrumb(self, label, url):
		"""
			Adds a item of the breadcrumb
			@param label as text to show, string
			@param url as url to redirect
		"""
		self.breadcrumb.append(dict(label = label, url = url))

	def _count_notificaciones(self, request):
		self.nro_notificaciones = 0
		import datetime
		x = datetime.datetime.now()
		fecha_desde = x.strftime('%Y-%m-%d')
		fecha_hasta = x.strftime('%Y-%m-%d')
		self.nro_notificaciones = Calendario.objects.filter(Q(fecha_inicio__range = (fecha_desde, fecha_hasta)) | Q(fecha_fin__range = (fecha_desde, fecha_hasta))).count()

	def initialProccess(self, request, kwargs):
		"""
			Organices the initial data
		"""
		self.user = request.user
		#self._count_notificaciones(request)

		data_page = dict()

		if self.user.is_anonymous or not self.user.is_authenticated:
			success = request.GET.get("success", None)
			error = request.GET.get("error", None)
			logged = request.GET.get("logged", None)
			self.template = "login.html"
			return dict(success = success, error = error, logged = logged)
		else:
			self.__get_menu(str(request.path))
			data_page = self.get_data(request, kwargs)

			data_page["menu"] = self.menu
			data_page["user"] = self.user
			data_page["can_view_page"] = self.can_view_page
			if self.can_view_page:
				data_page["breadcrumb"] = self.breadcrumb
				data_page["page_title"] = self.page_title
				data_page["aditional_templates"] = self.aditional_templates
				data_page["js_files"] = self.js_files
				data_page["css_files"] = self.css_files
				try:
					data_page["last_error"] = ", ".join(request.session['last_error'])
				except:
					data_page["last_error"] = ""
			else:
				data_page["page_title"] = _("Prohibido")
		try:
			translation.activate(request.session[translation.LANGUAGE_SESSION_KEY])
			data_page["lang"] = request.session[translation.LANGUAGE_SESSION_KEY]
		except Exception as e:
			data_page["lang"] = "es"
		data_page["nro_notificaciones"] = self.nro_notificaciones
		request.session['last_error'] = ""
		return data_page

	def _essentialData(self, request):
		"""
			Organices the pre data in view like yeae and user
		"""
		self.user = request.user
		if not self.user.is_anonymous:
			self._preProccess(request)

	def get(self, request, *args, **kwargs):
		"""
			Execute when the page makes a request
			@param request as request data
			@return render to response data
		"""
		self._essentialData(request)
		data_page = self.initialProccess(request, kwargs)
		return render(request, self.template, data_page)


	def _preProccess(self, request):
		pass

	def post(self, request, *args, **kwargs):
		"""
			Proccess the post data
			@param request as request data
		"""
		self._essentialData(request)
		success = False
		if request.POST.get("draw", None) != None:
			dataRet = self._dataTable(request)
			return dataRet

		elif request.POST.get("ajaxRequest", None) != None:
			ajaxR = self._ajaxRequest(request)
			return ajaxR

		elif request.POST.get("getObjectData", None) != None:
			object_id = request.POST.get("object_id", None)
			if object_id != None:
				ajaxR = self._getObjectData(object_id)
				return ajaxR
			else:
				success = False

		elif request.POST.get("tipoForm", None) != None:
			tipo = request.POST.get("tipoForm", None)

			if tipo == "editData":
				success = self._editData(request)
				prev_dir = request.POST.get("prev_dir", None)
				request.session['last_error'] = self.errors
				if prev_dir != None:
					if "?" in prev_dir:
						return HttpResponseRedirect(str(prev_dir) + "&success="+str(success))
					else:
						return HttpResponseRedirect(str(prev_dir) + "?success="+str(success))
				else:
					return HttpResponseRedirect(str(request.path)+"?success="+str(success))

			elif tipo == "addData":
				success = self._addData(request)
				request.session['last_error'] = self.errors
				return HttpResponseRedirect(str(request.path)+"?success="+str(success))

			elif tipo == "removeData":
				success = self._removeData(request)
				request.session['last_error'] = self.errors
				return HttpResponseRedirect(str(request.path)+"?success="+str(success))

			elif tipo == "informeForm":
				response = self._proccessInforme(request)
				request.session['last_error'] = self.errors
				return response

			elif tipo == "sendData":
				success = self._sendData(request)
				request.session['last_error'] = self.errors
				return HttpResponseRedirect(str(request.path)+"?success="+str(success))

			else:
				self._nothingPost()

		else:
			self._nothingPost()

		data_page = self.initialProccess(request, kwargs)
		data_page["success"] = success
		data_page["last_error"] = self.errors
		return render(request, self.template, data_page)

	def redirectIndex(self):
		return HttpResponseRedirect("/")

	def _nothingPost(self):
		print("Nothing")

	def get_data(self, request, *args):
		"""
			Returns the data to show
			@param request as request data
			is abstract
		"""
		pass


class MainTable(Main):
	"""
		Main table class
	"""
	#### Tables definition elements
	template = "tablas.html"
	# Dict with Column user and Column back
	table_columns = dict(titulo = _("Título"))
	# Columns to return in _GetData
	return_edit_columns = []

	model_object = None
	register_name = ""
	form_action = "/index"
	mostrar_control_activos = False

	can_add = False
	can_edit = False
	can_delete = False
	form_edit = None
	form_add = None
	delete_register = False


	can_view = False
	view_aparte = False

	# Muestra el formulario en una página aparte, úsese sólamente cuando el registro tenga archivos
	edit_aparte = False

	aditional_buttons = ""

	def _checkPermission(self, object_id, user):
		return True

	def get_data(self, request, kwargs):
		"""
			Obtiene los datos predeterminados de la página
		"""
		editarGET = request.GET.get("EditRegister", None)
		addGET = request.GET.get("AddRegister", None)

		if (((editarGET != None and self.can_edit) or (addGET != None and self.can_add)) and self.edit_aparte):
			return_val = self._get_data_tables(request)
			self.template = "form.html"
			object_id = ""
			forms = []
			object_inst = None
			if editarGET != None and self.can_edit:
				object_id = request.GET.get("object_id", None)
				#if self._checkPermission(objeto_id, user):
				#	continue
				try:
					object_inst = self.model_object.objects.get(pk = object_id)
					if not self.can_edit_row(object_inst):
						self.can_view_page = False
						return {}
					tipo_form = "editData"
					forms.append(dict(href="first", completa="True", tab=_("Editar registro"), form = self.form_edit(instance = object_inst, obj = object_inst)))
				except Exception as e:
					tipo_form = "addData"
					forms.append(dict(href="first", completa="True", tab=_("Añadir registro"), form = self.form_add()))
			elif addGET != None and self.can_add:
				tipo_form = "addData"
				forms.append(dict(href="first", completa="True", tab=_("Añadir registro"), form = self.form_add()))

			if return_val == None:
				return_val = dict()
			return_val["register_name"] = self.register_name
			return_val["form_action"] = self.form_action
			return_val["forms"] = forms
			return_val["target"] = ""
			return_val["tipo_form"] = tipo_form
			return_val["object_id"] = object_id
			return_val["title_form"] = self._get_title_form(object_inst)
			return return_val

		# Table Views
		if self.form_edit != None and self.edit_aparte == False and self.can_edit:
			self._add_aditional_template("varios/dialogo_editar.html")

		if self.form_add != None and self.edit_aparte == False and self.can_add:
			self._add_aditional_template("varios/dialogo_agregar.html")

		if self.delete_register and self.can_delete:
			self._add_aditional_template("varios/dialogo_eliminar.html")

		# Add css and javascript files
		self._add_js("jquery.dataTables.min.js")
		self._add_js("dataTables.bootstrap4.min.js")
		self._add_js("tablas.js")
		self._add_css("dataTables.bootstrap4.min.css")
		self._add_css("dropdown_extension.css")

		return_val = self._get_data_tables(request)

		self.columns = [""]
		for key in self.table_columns.keys():
			self.columns.append(self.table_columns[key])
		self.columns.append(_("Operaciones"))

		# Valores
		if return_val == None:
			return_val = dict()
		return_val["register_name"] = self.register_name
		return_val["form_action"] = self.form_action
		return_val["columns"] = self.columns
		if self.form_edit != None:
			return_val["form_edit"] = self.form_edit()
		if self.form_add != None:
			return_val["form_add"] = self.form_add()
		return_val["mostrar_control_activos"] = self.mostrar_control_activos
		return_val["delete_register"] = self.delete_register
		#return_val["docente_add_delete"] = self.docente_add_delete
		return_val["can_add"] = self.can_add
		return_val["edit_aparte"] = self.edit_aparte
		return_val["success"] = request.GET.get("success", None)

		# return data
		return return_val

	def _editData(self, request):
		"""
			Edit the object data depending of the form
		"""
		object_id = request.POST.get("object_id", None)
		if object_id != None and self.can_edit:
			if self._checkPermission(object_id, request.user):
				sed = self.model_object.objects.get(pk = object_id)
				form_edit = self.form_edit(data = request.POST, files = request.FILES, instance=sed)
				if form_edit.is_valid():
					form_edit.save()
					return True
			self._add_error(form_edit.errors)
		else:
			self._add_error(_("Usted no tiene permisos para editar este objeto"))
		return False

	def _addData(self, request):
		"""
			Add a table register
		"""
		if self.can_add:
			form_add = self.form_add(data = request.POST, files = request.FILES)
			if form_add.is_valid():
				form_add.save()
				return True
			self._add_error(form_add.errors)
		else:
			self._add_error(_("Usted no tiene permisos para añadir este objeto"))
		return False

	def _deleteData(self, request):
		"""
			Deletes a table register
		"""
		object_id = request.POST.get("object_id", None)
		if object_id != None and self.can_delete:
			#if self._checkPermission(object_id, user):
			sed = self.model_object.objects.get(pk = object_id)
			sed.delete()
			return True
		else:
			self._add_error(_("Usted no tiene permisos para eliminar este objeto"))
		return False


	def _ajaxRequest(self, request):
		"""
			Proccess the ajax data
		"""
		action = request.POST.get("ajax_action", None)
		error = False
		obj_id = 0
		if action == "editar_modal":
			data = self._editData(request)
		if action == "agregar_modal":
			data = self._addData(request)
		if action == "eliminar_modal":
			data = self._deleteData(request)
		if self.errors == []:
			self.errors = [""]
		if type(data) is list or type(data) is tuple:
			error = not(data[0])
			obj_id = data[1]
		else:
			error = not(data)
		data = {
			"error" : error,
			"errores" : ", ".join(self.errors),
			"obj_id" : obj_id,
		}
		return JsonResponse(data)

	def _getObjectData(self, object_id):
		"""
			Returns the object data for complete the edit fields
			@param object_id as int
		"""
		object_data = self.model_object.objects.get(pk = object_id)
		data = {
			'object_id' : object_data.pk,
			'get_json' : True,
			'json_register_name' : str(object_data)
		}
		for key in self.return_edit_columns:
			if len(key) == 2 and type(key) in (tuple, list):
				if key[1] == "date_format":
					data[key[0]] = localize(str(getattr(object_data, key[0])).replace("None", ""))
				else:
					value = str(getattr(getattr(object_data, key[0]), key[1]))
					key_item = key[0]
					data[key_item] = value
			else:
				data[key] = str(getattr(object_data, key))
		return JsonResponse(data)


	def _dataTable(self, request):
		"""
			Return data of dataTable
			@param request as request data
		"""
		value = request.POST.get("search[value]", None)
		start = int(request.POST.get("start", 0))
		length = int(request.POST.get("length", 10))


		filters = ["pk"]
		for key in self.table_columns.keys():
			if not "__format" in key:
				filters.append(key.replace("__date_format", ""))
			else:
				filters.append(self.order_rows(key.replace("__format", "")))
		filters.append("pk")

		order = int(request.POST.get("order[0][column]", len(filters)))
		add = ""
		direction = request.POST.get("order[0][dir]", 'desc')

		if direction == "desc":
			add = "-"

		fil = self._getFilerTable(value)

		solo_activos = request.POST.get("solo_activos", "true")
		if solo_activos == "true":
			fil = fil & Q(estado = ESTADO_ACTIVO)

		if fil == Q():
			data = self.model_object.objects.all().order_by(add + filters[order])[start:start+length]
		else:
			data = self.model_object.objects.filter(fil).order_by(add + filters[order])[start:start+length]

		data_ret = []
		for e in data:
			datos = [
				"<input type='radio' class='form-control input_check' name='col' value='" + str(e.pk) + "'>",
			]

			for key in self.table_columns.keys():
				try:
					first, second = key.split("__")
					if second == "format":
						datos.append(self._getFormatRow(first, e))
					elif second == "date_format":
						value = str(date_format(getattr(e, first), format='SHORT_DATETIME_FORMAT', use_l10n=False)).replace("None", "")
						datos.append(value)
					else:
						value = str(getattr(getattr(e, first), second))
						datos.append(value)
				except:
					value = str(getattr(e, key))
					if os.path.isfile(settings.MEDIA_ROOT + "/" + value):
						direc = settings.MEDIA_URL + value
						datos.append("<a href='"+direc+"' download class='btn btn-sm btn-success'><i class='fa fa-download'></i>" + str(_("Descargar")) + "</a>")
					else:
						datos.append(value)

			buttons = ""
			if self.form_edit != None and self.can_edit and self.can_edit_row(e):
				buttons = buttons + "<a title='"+ str( _("Editar datos completos")) + "' class='btn btn-xs btn-warning clean_noti'"
				if self.edit_aparte == True:
					buttons = buttons + "href='" +self.form_action+ "?EditRegister&object_id="+str(e.pk)+"'"
				else:
					buttons = buttons + "onClick='editar_fila("+str(e.pk)+")'"
				buttons = buttons + "><i class='fa fa-edit'></i></a>"
			if self.can_view:
				if self.view_aparte:
					buttons = buttons + "<a title='"+ str(_("Ver registro")) + "' class='btn btn-xs btn-primary' href='"+str(self.form_action)+"view/"+str(e.pk)+"'><i class='fa fa-eye'></i></a>"
				else:
					buttons = buttons + "<a title='"+ str(_("Ver registro")) + "' class='btn btn-xs btn-primary' onClick='ver_fila("+str(e.pk)+")'><i class='fa fa-eye'></i></a>"
			if self.delete_register and self.can_delete and self.can_delete_row(e):
				buttons = buttons + "<button title='"+ str(_("Eliminar registro")) + "' class='btn btn-xs btn-danger clean_noti' onClick='eliminar_fila("+str(e.pk)+")'><i class='fa fa-trash'></i></button>"

			buttons += self.aditional_buttons.replace("__PK", str(e.pk))

			datos.append(buttons)

			data_ret.append(datos)
		return JsonResponse({'recordsTotal' : self.model_object.objects.filter(fil).count(), 'recordsFiltered' : self.model_object.objects.filter(fil).count(), 'data' : data_ret})

	def _get_title_form(self, obj):
		return ""

	def can_edit_row(self, obj):
		return True

	def can_delete_row(self, obj):
		return True

	def order_rows(self, key):
		return key



class MainBlog(Main):
	"""
		Main blog class
	"""
	template = "structure_blog.html"
	menu = []
	js_files = []
	css_files = []
	page_title = "blog"
	can_view_page = True

	def _essentialData(self, request):
		pass

	def __get_menu(self, path):
		menu = []
		all_pages = PaginaBlog.objects.all()
		for page in all_pages:
			menu.append(dict(label = _(page.titulo), url = "/blog/page/" + str(page.pk), item_class = ""))
		self.menu = menu

	def initialProccess(self, request, kwargs):
		"""
			Organices the initial data
		"""
		self.__get_menu(str(request.path))
		data_page = {}
		data_page = self.get_data(request, kwargs)
		data_page["menu"] = self.menu
		data_page["can_view_page"] = self.can_view_page
		data_page["page_title"] = self.page_title
		data_page["js_files"] = self.js_files
		data_page["css_files"] = self.css_files
		return data_page
