from app.views.main import Main, MainTable
from django.views import View
from django.db.models import Q

from app.models import *
from app.forms.informes import *
from django.conf import settings
from app.utils.pdf import show_pdf

class ConfigurarInformesView(Main, View):
	template = "form.html"
	page_title = "Editar informes"
	form_action = "/informes/configurar"

	def get_data(self, request, kwargs):

		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Configuración informes", "")

		try:
			sed = ConfiguracionInformes.objects.last()
		except:
			sed = ConfiguracionInformes()
			sed.save()
		form_envio = editInformes(instance = sed)

		data_return = dict()
		forms = [dict(href="first", completa="True", tab="Editar datos", form = form_envio)]
		data_return["forms"] = forms
		data_return["form_action"] = self.form_action
		data_return["tipo_form"] = "editData"
		data_return["target"] = ""
		return data_return

	def _preProccess(self, request):
		if not self.user.es_admin:
			self.can_view_page = False

	def _editData(self, request):
		if self.user.es_admin:
			try:
				sed = ConfiguracionInformes.objects.last()
			except:
				sed = ConfiguracionInformes()
				sed.save()
			form_edit = editInformes(data = request.POST, files = request.FILES, instance=sed)
			if form_edit.is_valid():
				form_edit.save()
				return True
			self._add_error(form_edit.errors)
		return False

class PrintInformeView(Main, View):
	template = "form.html"
	page_title = "Imprimir informe"
	form_action = "/informes/print"

	def get_data(self, request, kwargs):

		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Imprimir informes", "")

		form_print = imprimirInformeForm()

		data_return = dict()
		forms = [dict(href="first", completa="True", tab="Datos básicos", form = form_print)]
		data_return["forms"] = forms
		data_return["form_action"] = self.form_action
		data_return["tipo_form"] = "informeForm"
		data_return["target"] = "target='_blank'"
		return data_return

	def _preProccess(self, request):
		if not self.user.es_admin:
			self.can_view_page = False

	def _proccessInforme(self, request):
		# TODO: añadir fecha retiro y eso
		if self.user.es_admin:
			form_basic = imprimirInformeForm(request.POST)
			if form_basic.is_valid():
				fecha_desde = form_basic.cleaned_data["fecha_desde"]
				fecha_hasta = form_basic.cleaned_data["fecha_hasta"]
				rango_fecha = [fecha_desde, fecha_hasta]

				# Cabecera y demás
				try:
					Datos = ConfiguracionInformes.objects.last()
					cabecera = Datos.cabecera
					escudo_derecha = settings.MEDIA_URL + str(Datos.escudo_derecha)
					escudo_izquierda = settings.MEDIA_URL + str(Datos.escudo_izquierda)
				except:
					cabecera = None
					escudo_derecha = None
					escudo_izquierda = None

				anteproyectos_recibidos	= Anteproyecto.objects.filter(Q(fecha_ingreso__range = rango_fecha))
				evaluadores_asignados = AnteproyectoConcepto.objects.filter(Q(fecha_asignacion__range = rango_fecha))

				estado_anteproyectos = []
				anteproyectos_aprobados = []
				anteproyectos_aplazados = []
				"""for tgrado in TGrado.objects.filter(Q(estado = TGrado.E_EJECUCION_ANTEPROYECTO) | Q(estado = TGrado.E_ANTEPROYECTO)):
					if not Informe.objects.filter(tgrado = tgrado).exists():
						anteproyecto = Anteproyecto.objects.filter(tgrado = tgrado).last()
						if anteproyecto != None:
							concepto = AnteproyectoConcepto.objects.filter(anteproyecto = anteproyecto).last()
							if concepto != None:
								data = {
									"tgrado_titulo" : tgrado.titulo,
									"concepto" : concepto.get_concepto(),
									"evaluador" : concepto.evaluador,
									"modalidad" : tgrado.get_modalidad()
								}
								estado_anteproyectos.append(data)
							else:
								estado_anteproyectos.append({
									"tgrado_titulo" : tgrado.titulo,
									"concepto" : "sin asignar",
									"evaluador" : "",
									"modalidad" : tgrado.get_modalidad()
								})
				"""
				for con in AnteproyectoConcepto.objects.filter(Q(fecha_modificacion__range = rango_fecha)):
					if con.concepto == AnteproyectoConcepto.CONCEPTO_APROBADO:
						anteproyectos_aprobados.append({
							"tgrado_titulo" : con.anteproyecto.tgrado.titulo,
							"concepto" : con.get_concepto(),
							"evaluador" : con.evaluador,
							"modalidad" : con.anteproyecto.tgrado.get_modalidad()
						})
					elif con.concepto == AnteproyectoConcepto.CONCEPTO_APLAZADO:
						anteproyectos_aplazados.append({
							"tgrado_titulo" : con.anteproyecto.tgrado.titulo,
							"concepto" : con.get_concepto(),
							"evaluador" : con.evaluador,
							"modalidad" : con.anteproyecto.tgrado.get_modalidad()
						})

				data_return = dict()
				data_return["cabecera"] = cabecera
				data_return["escudo_derecha"] = escudo_derecha
				data_return["escudo_izquierda"] = escudo_izquierda
				data_return["fecha_desde"] = fecha_desde
				data_return["fecha_hasta"] = fecha_hasta

				# Datos a mostrar
				data_return["anteproyectos_recibidos"] = anteproyectos_recibidos
				data_return["evaluadores_asignados"] = evaluadores_asignados

				data_return["estado_anteproyectos"] = estado_anteproyectos

				data_return["anteproyectos_aprobados"] = anteproyectos_aprobados
				data_return["anteproyectos_aplazados"] = anteproyectos_aplazados

				return show_pdf(data_return, "print/informe.html", request.build_absolute_uri())

		return show_error()
