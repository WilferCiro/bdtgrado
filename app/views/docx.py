# Creado por Wilfer Daniel Ciro Maya - wilcirom@gmail.com
# Desde 2018 - 2019
# Creado para el comité de trabajos de grado del programa de Ingeniería electrónica de la universidad del Quindío, Armenia - Quindío, Colombia

# Django 2.1.3
# Bootstrap 4
# Mysql
# Para tips de optimización visite https://docs.djangoproject.com/en/2.1/topics/db/optimization/
# Para tips de seguridad visite https://librosweb.es/libro/django_1_0/capitulo_19.html

from app.views.main import Main, MainTable
from django.views import View
from django.db.models import Q

from app.models import *
from app.forms.tgrado import *
from app.forms.ajax import *
from django.http import JsonResponse
from django.utils.formats import localize
from django.http import HttpResponse

from docx import Document

class EvaluacionFormato(Main, View):

	def _proccessInforme(self, request):
		if self.user.es_admin:
			tgrado_pk = int(request.POST.get('tgrado'))
			estudiante_pk = int(request.POST.get('estudiante'))
			try:
				tgrado = TGrado.objects.get(pk = tgrado_pk)
				informe = Informe.objects.filter(Q(tgrado = tgrado)).last()
				estudiante = User.objects.get(Q(pk = estudiante_pk) & Q(es_estudiante = True))
				tgrado_estudiante = TGradoEstudiantes.objects.get(Q(estudiante = estudiante) & Q(tgrado = tgrado))
			except Exception as ex:
				return self.redirectIndex()

			if not self.user.es_admin:
				return self.redirectIndex()

			try:
				sed = ConfiguracionInformes.objects.last()
			except:
				sed = ConfiguracionInformes()
				sed.save()
				return self.redirectIndex()

			if sed.formato_evaluacion != None and sed.formato_evaluacion != "" and sed.formato_evaluacion.url != None:
				document = Document(sed.formato_evaluacion)

				evaluador1 = ""
				evaluador2 = ""
				evaluadores_obj = InformeConcepto.objects.filter(Q(informe = informe))
				if len(evaluadores_obj) >= 1:
					evaluador1 = evaluadores_obj[0].evaluador.get_full_name()
				if len(evaluadores_obj) >= 2:
					evaluador2 = evaluadores_obj[1].evaluador.get_full_name()

				dic = {
					"{{evaluador1}}" : str(evaluador1),
					"{{evaluador2}}" : str(evaluador2),
					"{{evaluador3}}" : str(tgrado.director.get_full_name()),
					"{{evaluador4}}" : "",
					"{{estudiante}}" : estudiante.get_full_name(),
					"{{tgrado}}" : tgrado.titulo,
					"{{director}}" : str(tgrado.director.get_full_name()),
					"{{codigo}}" : estudiante.documento,
					"{{pasantia}}" : ["X" if tgrado.modalidad == MODALIDAD_PASANTIA else ""][0],
					"{{aplicacion}}" : ["APLICACIÓN" if tgrado.modalidad == MODALIDAD_APLICACION else ""][0],
					"{{investigacion}}" : ["X" if tgrado.modalidad == MODALIDAD_INVESTIGACION else ""][0],
					"{{monografia}}" : ""
				}
				for p in document.paragraphs:
					inline = p.runs
					for i in range(len(inline)):
						text = inline[i].text
						for key in dic.keys():
							if key in text:
								text = text.replace(key, dic[key])
								inline[i].text = text
				for table in document.tables:
					for row in table.rows:
						for cell in row.cells:
							for p in cell.paragraphs:
								inline = p.runs
								for i in range(len(inline)):
									text = inline[i].text
									for key in dic.keys():
										if key in text:
											text = text.replace(key, dic[key])
											inline[i].text = text
			else:
				return self.redirectIndex()

			response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.wordprocessingml.document')
			response['Content-Disposition'] = 'attachment; filename=formato_' + str(estudiante.get_full_name()) + '.docx'
			document.save(response)
			return response
