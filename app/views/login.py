from app.models import *
from django.shortcuts import render
from django.views import View
from django.http import HttpResponse, HttpResponseRedirect
from app.forms.login import *
from app.utils.mails import *

class SingUp(View):
	def get(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")
		
		form = SingUpForm()
		
		return render(request, "login/singup.html", {"form" : form})
		
	def post(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")
		
		form = SingUpForm(data = request.POST)
		error = ""
		if form.is_valid():
			apellido = form.cleaned_data["apellido"]
			nombre = form.cleaned_data["nombre"]
			documento = form.cleaned_data["documento"]
			email = form.cleaned_data["email"] + "@uqvirtual.edu.co"
			pass1 = form.cleaned_data["pass1"]
			pass2 = form.cleaned_data["pass2"]
			sexo = form.cleaned_data["sexo"]
			if pass1 != pass2:
				error = "Las contraseñas no coinciden"
			else:
				try:
					exists = User.objects.get(username = documento)
					error = "No se puede registrar este documento, revise si ya está registrado"
				except:
					new_user = User.objects.create_user(username=documento,
				                         email=email)
					new_user.set_password(pass1)
					new_user.es_estudiante = True
					new_user.es_administrativo = False
					new_user.last_name = apellido
					new_user.first_name = nombre
					new_user.documento = documento
					new_user.genero = sexo
					new_user.save()
					return HttpResponseRedirect("/index?logged")
		print(form.errors)
		return render(request, "login/singup.html", {"form" : form, "error" : error})

class RecoverPass(View):
	def get_code(self, nro):
		import string, random
		passkey = ''
		for x in range(nro):
			if random.choice([1,2]) == 1:
				passkey += passkey.join(random.choice(string.ascii_uppercase))
			else:
				passkey += passkey.join(random.choice(string.digits))
		return passkey
	
	def get(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")
				
		return render(request, "login/recover.html", {})
	
	def post(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")
		
		documento = request.POST.get("documento", None)
		error = True
		if documento != None:
			try:
				user = User.objects.get(documento = documento)
				email = user.email
				if user.es_admin:
					mensaje = "Esta cuenta no puede ser recuperada por este método, por favor contacte con los administradores"
				else:
					mensaje = "Aún no se ha implementado esta opción, (" + str(email) + ")"
				
				if email != "":
					codigo_rec = self.get_code(8)
					while CambioContrasena.objects.filter(Q(codigo = codigo_rec)):
						codigo_rec = self.get_code(5)
						
					cambio_pass = CambioContrasena.objects.filter(usuario = user).last()
					if cambio_pass == None:
						cambio_pass = CambioContrasena(usuario = user)
					cambio_pass.codigo = codigo_rec
					cambio_pass.save()
					
					asunto = "Recuperación de contraseña en BDTgrado"
					cuerpo = "Tu código de recuperación es <b>" + str(codigo_rec) + "</b>, o accede directamente al <a href='http://127.0.0.1:8000/recover/pass/code?codigo="+ str(codigo_rec) +"'>siguiente enlace</a>"
					send_an_email(asunto = asunto, cuerpo = cuerpo, correos = [email])
					
					mensaje = "Se ha enviado un código a tu correo, por favor introdúcelo a continuación"
					
					error = False
					
					return HttpResponseRedirect("/recover/pass/code")
			except User.DoesNotExist:
				mensaje = "No existe usuario con este documento"
			except Exception as e:
				print(e)
				mensaje = "No se pudo recuperar, por favor contacte con los administradores"
		return render(request, "login/recover.html", {"mensaje" : mensaje, "error" : error})


class RecoverPassCode(View):
	
	def get(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")
		codigo = request.GET.get("codigo", None)
		
		data_return = {}
		if codigo != None:
			try:
				codigo_bd = CambioContrasena.objects.get(codigo = codigo)
				from datetime import datetime
				now = datetime.now()
				fecha = codigo_bd.fecha_solicitud
				if (now - fecha).days < 1:
					data_return["code_valid"] = True
					data_return["codigo"] = codigo
					data_return["nombre_usuario"] = codigo_bd.usuario.get_full_name()
				else:
					codigo_bd.delete()
			except:
				data_return["error"] = True
				data_return["mensaje"] = "El código ingresado no existe"
		
		return render(request, "login/recover_code.html", data_return)
	
	def post(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")
		
		tipo = request.POST.get("change_pass", None)
		error = True
		mensaje = "Error"
		if tipo == "change_pass":
			try:
				pass1 = request.POST.get("pass1", None)
				pass2 = request.POST.get("pass2", None)
				codigo = request.POST.get("codigo", None)
				if pass1 == pass2 and pass1 != None and pass1 != "" and codigo != None and codigo != "":
					codigo_bd = CambioContrasena.objects.get(codigo = codigo)
					from datetime import datetime
					now = datetime.now()
					fecha = codigo_bd.fecha_solicitud
					usuario = codigo_bd.usuario
					if (now - fecha).days < 1:
						usuario.set_password(pass1)
						usuario.save()
						error = False
						return HttpResponseRedirect("/index?cambio_pass")
					codigo_bd.delete()
					mensaje = "Ya pasó más de 1 día para el cambio de contraseña, vuelve a solicitar un código"
			except Exception as e:
				print(e)
				mensaje = "No se pudo modificar la contraseña, por favor vuelve a hacer el proceso completo"
		return render(request, "login/recover_code.html", {"mensaje" : mensaje, "error" : error})

		
		
		
