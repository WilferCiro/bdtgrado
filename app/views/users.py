from app.views.main import Main, MainTable
from django.views import View
from django.db.models import Q

from app.models import *
from app.forms.users import *
from django.utils.translation import ugettext_lazy as _

class EstudiantesView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	page_title = _("Estudiantes")
	register_name = _("estudiante")
	form_action = "/estudiantes/"
	model_object = User
	table_columns =	dict(first_name = _("Nombres"), last_name = _("Apellidos"), documento = _("Documento"), email = _("Correo"), fecha_creacion__date_format = _("Fecha de Ingreso"))
	return_edit_columns = ["last_name", "first_name", "documento", "email", "telefono1", "red_social", "genero"]
	form_edit = editUser
	form_add = editUser
	delete_register = True

	view_aparte = True

	def _preProccess(self, request):
		if self.user.es_estudiante:
			self.can_view_page = False
		elif self.user.es_admin:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		elif self.user.es_evaluador:
			self.can_edit = False
			self.can_delete = False
			self.can_add = False
		self.can_view = True

	def _addData(self, request):
		if self.can_add:
			form_edit = self.form_edit(data = request.POST)
			if form_edit.is_valid():
				try:
					new_object = form_edit.save(commit=False)
					documento = form_edit.cleaned_data["documento"]
					new_object.password = documento
					new_object.username = documento
					new_object.es_estudiante = True
					new_object.profesion = "Estudiante"
					new_object.save()
					form_edit.save_m2m()
					return True
				except:
					self._add_error(_("Ya existe un usuario con estos datos"))
					return False
			self._add_error(form_edit.errors)
		else:
			self._add_error(_("Usted no tiene permisos para añadir este objeto"))
		return False

	def _get_data_tables(self, request):
		self._add_breadcrumb(_("Inicio"), "/")
		self._add_breadcrumb(_("Estudiantes"), "")

	def _getFilerTable(self, value):
		fil = Q(es_estudiante = True)
		if self.user.es_evaluador and not self.user.es_admin:
			fil = fil & self.user.getEstudiantes()
		if value != "":
			fil = fil & (Q(last_name__icontains = value) | Q(first_name__icontains = value) | Q(documento__icontains=value) | Q(email__icontains = value) | Q(fecha_creacion__icontains = value))
		return fil

	def can_delete_row(self, obj):
		if TGradoEstudiantes.objects.filter(Q(estudiante = obj)).exists():
			return False
		return True

class EvaluadoresView(MainTable, View):
	"""
		view of Asignación Docente
	"""
	page_title = _("Evaluadores")
	register_name = _("evaluador")
	form_action = "/evaluadores/"
	model_object = User
	table_columns =	dict(first_name = _("Nombres"), last_name = _("Apellidos"), documento = _("Documento"), email = _("Correo"), fecha_creacion__date_format = _("Fecha de Ingreso"))
	return_edit_columns = ["last_name", "first_name", "documento", "email", "telefono1", "red_social", "genero"]
	form_edit = editUserEvaluador
	form_add = editUserEvaluador
	delete_register = True

	view_aparte = True

	def _preProccess(self, request):
		if self.user.es_admin:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
			self.can_view = True
		else:
			self.can_view_page = False

	def _addData(self, request):
		if self.can_add:
			form_edit = self.form_edit(data = request.POST)
			if form_edit.is_valid():
				try:
					nombre = form_edit.cleaned_data["first_name"]
					apellido = form_edit.cleaned_data["last_name"]
					genero = form_edit.cleaned_data["genero"]
					documento = form_edit.cleaned_data["documento"]
					email = form_edit.cleaned_data["email"]
					telefono1 = form_edit.cleaned_data["telefono1"]
					red_social = form_edit.cleaned_data["red_social"]

					new_user = User.objects.create_user(username=documento,
						                 email=email)
					new_user.set_password(documento)
					new_user.es_evaluador = True
					new_user.first_name = nombre
					new_user.last_name = apellido
					new_user.documento = documento
					new_user.genero = genero
					new_user.telefono1 = telefono1
					new_user.red_social = red_social
					new_user.save()
					return True
				except Exception as ex:
					print(ex)
					self._add_error("Ya existe un usuario con estos datos")
					return False
			self._add_error(form_edit.errors)
		else:
			self._add_error("Usted no tiene permisos para añadir este objeto")
		return False

	def _get_data_tables(self, request):
		self._add_breadcrumb(_("Inicio"), "/")
		self._add_breadcrumb(_("Evaluadores"), "")

	def _getFilerTable(self, value):
		fil = Q(es_evaluador = True)
		if value != "":
			fil = fil & (Q(last_name__icontains = value) | Q(first_name__icontains = value) | Q(documento__icontains=value) | Q(email__icontains = value) | Q(fecha_creacion__icontains = value))
		return fil

	def can_delete_row(self, obj):
		if obj.es_admin or TGradoAsesores.objects.filter(Q(asesor = obj)).exists() or TGrado.objects.filter(director = obj).exists():
			return False
		return True


class MyProfileView(Main, View):
	template = "user/profile.html"
	page_title = _("Perfil de usuario")

	def get_data(self, request, kwargs):
		try:
			user_id = int(kwargs['pk'])
			user_profile = User.objects.get(pk = user_id)
		except:
			user_profile = self.user

		email = user_profile.email
		if user_profile.es_estudiante:
			email = email.replace("@uqvirtual.edu.co", "")
		user_data = {
			"username" : user_profile.username,
			"documento" : user_profile.documento,
			"email" : email,
			"genero" : user_profile.genero,
			"telefono" : user_profile.telefono1,
			"red_social" : user_profile.red_social,
			"full_name" : user_profile.get_full_name(),
			"es_admin" : user_profile.es_admin,
			"es_evaluador" : user_profile.es_evaluador,
			"es_estudiante" : user_profile.es_estudiante,
			"pk" : user_profile.pk,
			"last_login" : user_profile.last_login,
			"creation_date" : user_profile.date_joined,
			"avatar" : user_profile.avatar
		}

		self._add_breadcrumb(_("Inicio"), "/")
		self._add_breadcrumb(_("Perfil de usuario"), "")

		data_return = dict()
		ver_sensible = False
		if self.user.es_admin or self.user.es_evaluador or self.user == user_profile:
			ver_sensible = True
		data_return["ver_sensible"] = ver_sensible

		if user_data["es_evaluador"]:
			asesorados = TGradoAsesores.objects.filter(asesor = user_profile).distinct().count()
			evaluados_anteproyectos = AnteproyectoConcepto.objects.filter(evaluador = user_profile).distinct().count()
			evaluados_informes = InformeConcepto.objects.filter(evaluador = user_profile).distinct().count()
			dirigidos = TGrado.objects.filter(director = user_profile).distinct().count()
			codirigidos = TGrado.objects.filter(codirector = user_profile).distinct().count()
			data_return["asesorados"] = asesorados
			data_return["evaluados_anteproyectos"] = evaluados_anteproyectos
			data_return["evaluados_informes"] = evaluados_informes
			data_return["dirigidos"] = dirigidos
			data_return["codirigidos"] = codirigidos

		elif user_data["es_estudiante"]:
			try:
				estado_academico = EstadoAcademico.objects.get(estudiante = user_profile)
			except:
				estado_academico = None
			lista_tgrados = []
			for tgrado in TGradoEstudiantes.objects.filter(estudiante = user_profile):
				lista_tgrados.append({
					"titulo" : tgrado.tgrado.titulo,
					"pk" : tgrado.tgrado.pk
				})

			if lista_tgrados != []:
				data_return["lista_tgrados"] = lista_tgrados

			data_return["estado_academico"] = estado_academico

		can_edit = False
		if self.user.es_admin or self.user == user_profile:
			if user_data["es_estudiante"]:
				if estado_academico != None:
					data_return["form_edit_estado_academico"] = editEstadoAcademico(instance = estado_academico)
				else:
					data_return["form_edit_estado_academico"] = editEstadoAcademico()

				data_return["form_edit_user"] = editUser(instance = user_profile)
			elif self.user.es_evaluador:
				data_return["form_edit_user"] = editUser(instance = user_profile)
			elif self.user.es_admin:
				data_return["form_edit_user"] = editUserEvaluador(instance = user_profile)

			data_return["form_edit_avatar"] = editAvatar(instance = user_profile)

			data_return["form_change_pass"] = changePass()

		data_return["user_data"] = user_data

		return data_return

	def _editData(self, request):
		tipo_edit = request.POST.get("tipoEdit", None)
		object_id = request.POST.get("object_id", None)

		if tipo_edit == "user_data" and object_id != None:
			try:
				user_edit = User.objects.get(pk = object_id)
				if self.user.es_admin or self.user == user_edit:
					if user_edit.es_estudiante or self.user.es_evaluador:
						form = editUser(data = request.POST, instance = user_edit)
					else:
						form = editUserEvaluador(data = request.POST, instance = user_edit)
					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
			except Exception as e:
				print(e)
				pass
		if tipo_edit == "email" and object_id != None:
			try:
				user_edit = User.objects.get(pk = object_id)
				if self.user.es_admin or self.user == user_edit:
					email = request.POST.get("email", None)
					if email != "" and email != None:
						if user_edit.es_estudiante:
							email = email + "@uqvirtual.edu.co"
						user_edit.email = email
						user_edit.save()
						return True
					else:
						self._add_error("El correo no puede estar vacío")
			except Exception as e:
				print(e)
				pass
		elif tipo_edit == "user_avatar":
			try:
				user_edit = User.objects.get(pk = object_id)
				if self.user.es_admin or self.user == user_edit:
					form = editAvatar(data = request.POST, files = request.FILES, instance = user_edit)
					if form.is_valid():
						form.save()
						return True
					self._add_error(form.errors)
			except Exception as e:
				print(e)
				pass

		elif tipo_edit == "estado_academico":
			try:
				user_id = request.POST.get("usuario_id", None)
				user_edit = User.objects.get(pk = user_id)
				if object_id != None:
					EA = EstadoAcademico.objects.get(pk = object_id)
				else:
					EA = EstadoAcademico(estudiante = user_edit)
					EA.save()
				if EA.estudiante == user_edit:
					form = editEstadoAcademico(data = request.POST, files = request.FILES, instance = EA)
					if form.is_valid():
						form.save()
						return True
					else:
						self._add_error(form.errors)
			except Exception as ex:
				print(ex)
				pass

		elif tipo_edit == "password":
			try:
				user_edit = User.objects.get(pk = object_id)
				form_edit = changePass(data = request.POST)
				if form_edit.is_valid() and (self.user.es_admin or self.user == user_edit):
					pass1 = form_edit.cleaned_data["pass1"]
					pass2 = form_edit.cleaned_data["pass2"]
					if pass1 == pass2:
						user_edit.set_password(pass1)
						user_edit.save()
						return True
					else:
						self._add_error("Las contraseñas no coinciden")
				else:
					self._add_error("No se puede editar la contraseña")
			except Exception as ex:
				print(ex)


		return False
