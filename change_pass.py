from django.db.models import Q
from app.models import *

for user in User.objects.filter(Q(es_evaluador = True) & Q(es_admin = False)):
	user.set_password("evaluador")
	user.save()
