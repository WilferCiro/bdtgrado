from django.conf import settings
import bdtgrado.settings as app_settings
import datetime
from django.db.models import Q
from django.core.mail import send_mail

settings.configure(INSTALLED_APPS=app_settings.INSTALLED_APPS,DATABASES=app_settings.DATABASES, STATIC_URL=app_settings.STATIC_URL)

import django
django.setup()

from app.models import User, InformeConcepto, TGrado, TGradoEstudiantes, MailTemplates, ESTADO_ACTIVO, AnteproyectoConcepto, Informe, Anteproyecto
today_min = datetime.datetime.combine(datetime.date.today(), datetime.time.min)
today_max = datetime.datetime.combine(datetime.date.today(), datetime.time.max)

last_template = MailTemplates.objects.filter(Q(estado = ESTADO_ACTIVO)).last()

all_msg = []

for concepto in InformeConcepto.objects.filter(Q(fecha_modificacion__range=(today_min, today_max))):
	tgrado = concepto.informe.tgrado
	all_estudiantes = tgrado.returnEstudiantes()
	for est in TGradoEstudiantes.objects.filter(tgrado = tgrado):
		if est.estudiante.email != "" and est.estudiante.email != None:
			mensaje_send = ("El comité de trabajos de grado se permite informarle que se ha editado la información relacionada al concepto del informe final de su trabajo de grado, relacionado a continuación: <br /><br />" +\
				"<b>Fecha: </b> " + str(concepto.fecha_modificacion) + "<br /> " + \
				"<b>Estudiante(s): </b> " + str(all_estudiantes) + "<br /> " + \
				"<b>Trabajo de grado</b>: " + str(tgrado.titulo) + " <br />" +\
				"<b>Director del Trabajo de grado</b>: " + str(tgrado.returnDirector()) + " <br />" +\
				"<b>Co Director del Trabajo de grado</b>: " + str(tgrado.returnCoDirector()) + " <br />" +\
				"<b>Asesor(es) del Trabajo de grado</b>: " + str(tgrado.returnAsesores()) + " <br />" +\
				"<b>Docente evaluador</b>: " + concepto.evaluador.get_full_name() + " <br />" +\
				"<b>Concepto</b>: " + str(concepto.get_concepto()) + "<br />")

			if last_template != None:
				if "{{cuerpo}}" in last_template.mensaje:
					mensaje_send = last_template.mensaje.replace("{{cuerpo}}", mensaje_send)
			all_msg.append({
				"cuerpo" : mensaje_send,
				"email" : est.estudiante.email,
				"asunto" : "Modificación de concepto de informe final"
			})

for concepto in AnteproyectoConcepto.objects.filter(Q(fecha_modificacion__range=(today_min, today_max))):
	tgrado = concepto.anteproyecto.tgrado
	all_estudiantes = tgrado.returnEstudiantes()
	for est in TGradoEstudiantes.objects.filter(tgrado = tgrado):
		if est.estudiante.email != "" and est.estudiante.email != None:
			mensaje_send = ("El comité de trabajos de grado se permite informarle que se ha editado la información del concepto del anteproyecto de trabajo de grado, relacionado a continuación: <br /><br />" +\
				"<b>Fecha: </b> " + str(concepto.fecha_modificacion) + "<br /> " + \
				"<b>Estudiante(s): </b> " + str(all_estudiantes) + "<br /> " + \
				"<b>Trabajo de grado</b>: " + str(tgrado.titulo) + " <br />" +\
				"<b>Director del Trabajo de grado</b>: " + str(tgrado.returnDirector()) + " <br />" +\
				"<b>Co Director del Trabajo de grado</b>: " + str(tgrado.returnCoDirector()) + " <br />" +\
				"<b>Asesor(es) del Trabajo de grado</b>: " + str(tgrado.returnAsesores()) + " <br />" +\
				"<b>Docente evaluador</b>: " + concepto.evaluador.get_full_name() + " <br />" +\
				"<b>Concepto</b>: " + str(concepto.get_concepto()) + "<br />")

			if last_template != None:
				if "{{cuerpo}}" in last_template.mensaje:
					mensaje_send = last_template.mensaje.replace("{{cuerpo}}", mensaje_send)
			all_msg.append({
				"cuerpo" : mensaje_send,
				"email" : est.estudiante.email,
				"asunto" : "Modificación de concepto de informe final"
			})

for anteproyecto in Anteproyecto.objects.filter(Q(fecha_edicion__range=(today_min, today_max))):
	tgrado = anteproyecto.tgrado
	all_estudiantes = tgrado.returnEstudiantes()
	for est in TGradoEstudiantes.objects.filter(tgrado = tgrado):
		if est.estudiante.email != "" and est.estudiante.email != None:
			mensaje_send = ("El comité de trabajos de grado se permite informarle que se ha editado la información del anteproyecto de trabajo de grado, relacionado a continuación: <br /><br />" +\
				"<b>Fecha: </b> " + str(anteproyecto.fecha_edicion) + "<br /> " + \
				"<b>Estudiante(s): </b> " + str(all_estudiantes) + "<br /> " + \
				"<b>Trabajo de grado</b>: " + str(tgrado.titulo) + " <br />" +\
				"<b>Director del Trabajo de grado</b>: " + str(tgrado.returnDirector()) + " <br />" +\
				"<b>Co Director del Trabajo de grado</b>: " + str(tgrado.returnCoDirector()) + " <br />" +\
				"<b>Asesor(es) del Trabajo de grado</b>: " + str(tgrado.returnAsesores()) + " <br />")

			if last_template != None:
				if "{{cuerpo}}" in last_template.mensaje:
					mensaje_send = last_template.mensaje.replace("{{cuerpo}}", mensaje_send)
			all_msg.append({
				"cuerpo" : mensaje_send,
				"email" : est.estudiante.email,
				"asunto" : "Modificación de anteproyecto"
			})

for informe in Informe.objects.filter(Q(fecha_edicion__range=(today_min, today_max))):
	tgrado = informe.tgrado
	all_estudiantes = tgrado.returnEstudiantes()
	for est in TGradoEstudiantes.objects.filter(tgrado = tgrado):
		if est.estudiante.email != "" and est.estudiante.email != None:
			mensaje_send = ("El comité de trabajos de grado se permite informarle que se ha editado la información del informe de trabajo de grado, relacionado a continuación: <br /><br />" +\
				"<b>Fecha: </b> " + str(informe.fecha_edicion) + "<br /> " + \
				"<b>Estudiante(s): </b> " + str(all_estudiantes) + "<br /> " + \
				"<b>Trabajo de grado</b>: " + str(tgrado.titulo) + " <br />" +\
				"<b>Director del Trabajo de grado</b>: " + str(tgrado.returnDirector()) + " <br />" +\
				"<b>Co Director del Trabajo de grado</b>: " + str(tgrado.returnCoDirector()) + " <br />" +\
				"<b>Asesor(es) del Trabajo de grado</b>: " + str(tgrado.returnAsesores()) + " <br />")

			if last_template != None:
				if "{{cuerpo}}" in last_template.mensaje:
					mensaje_send = last_template.mensaje.replace("{{cuerpo}}", mensaje_send)
			all_msg.append({
				"cuerpo" : mensaje_send,
				"email" : est.estudiante.email,
				"asunto" : "Modificación de informe final"
			})

all_msg.append({
	"cuerpo" : """
	<html>
	  <head>
	    <style>
	      .colored {
	        color: blue;
	      }
	      #body {
	        font-size: 14px;
	      }
	      @media screen and (min-width: 500px) {
	        .colored {
	          color:red;
	        }
	      }
	    </style>
	  </head>
	  <body>
	    <div id='body'>
	      <p>Hi Pierce,</p>
	      <p class='colored'>
	        This text is blue if the window width is
	        below 500px and red otherwise.
	      </p>
	      <p>Jerry</p>
	    </div>
	  </body>
	</html>
	""",
	"email" : "wilcirom@gmail.com",
	"asunto" : "Modificación de informe final"
})


for msg in all_msg:
	send_mail(
		msg["asunto"],
		'',
		'infotgradouniquindio@gmail.com',
		[msg["email"]],
		fail_silently=False,
		html_message = msg["cuerpo"]
	)
